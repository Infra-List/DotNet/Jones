using Microsoft.OpenApi.Models;

namespace Jones.Swagger.Extensions;

public static class OpenApiDocumentExtensions
{
    public static void PathsRemove(this OpenApiDocument swaggerDoc, string? relativePat)
    {
        var key = "/" + relativePat;
        if (key.Contains('?'))
        {
            var idx = key.IndexOf('?');
            key = key[..idx];
        }
        swaggerDoc.Paths.Remove(key);
    }
}