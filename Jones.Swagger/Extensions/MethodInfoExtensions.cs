using System.Reflection;
using Microsoft.AspNetCore.Authorization;

namespace Jones.Swagger.Extensions;

public static class MethodInfoExtensions
{
    public static bool IsHasAttribute<T>(this MethodInfo method)
    {
        return method.ReflectedType?.GetCustomAttributes(typeof(T), true).Length != 0
               || method.GetCustomAttributes(typeof(T), true).Length != 0;
    }

    public static bool IsAuthorize(this MethodInfo method) =>
        !method.IsHasAttribute<AllowAnonymousAttribute>() && method.IsHasAttribute<AuthorizeAttribute>();
}