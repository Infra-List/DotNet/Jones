using System;
using System.Linq;
using System.Reflection;
using Jones.Helper;
using Jones.Swagger.Filter;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Extensions;

public static class ServiceCollectionExtensions
{
    private static SwaggerConfig? _config;
    
    public static IServiceCollection AddSwagger(this IServiceCollection services, SwaggerConfig config, string? title = null, string? description = null, 
        bool isAuthorizationEnable = true, Action<SwaggerGenOptions>? setupAction = null)
    {
        _config = config;
        if (_config is not { IsEnable: true })
        {
            return services;
        }
        if (_config.AccessToken != null)
        {
            SwaggerAuthorizationHeaderParameterOperationFilter.SetAccessToken(_config.AccessToken);
        }
        
        services.AddSwaggerExamplesFromAssemblies(Assembly.GetEntryAssembly());
        
        services.AddSwaggerGen(options =>
        {
            options.SupportNonNullableReferenceTypes();
            options.UseInlineDefinitionsForEnums();
            
            if (_config.IsUseHidden == true)
            {
                options.DocumentFilter<SwaggerHiddenFilter>();
            }

            if (isAuthorizationEnable)
            {
                options.DocumentFilter<AuthorizeFilter>();
                // options.DocumentFilter<SwaggerAuthorizationFilter>();
            }
            
            // options.DocumentFilter<SwaggerEnumFilter>();
            options.SchemaFilter<SwaggerSchemaFilter>();
            options.ExampleFilters();
            options.CustomSchemaIds(i => i.FullName?.Replace("+", "."));
            
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = title ?? EntryAssemblyHelper.GetAssemblyTitle(),
                Description = 
                    $"""
                    `Status code` 在非200-299范围内，`Response Body` 将使用如下标准：<a href="https://www.rfc-editor.org/rfc/rfc7807" target="_blank">Problem Details for HTTP APIs</a>
                    {description}
                    """
            });
            
            //使用annotation来描述接口  不依赖xml文件
            options.EnableAnnotations();
            // 下面两句，将swagger文档中controller名使用GroupName替换
            // 在Swagger中，一个Tag可以看作是一个API分组
            options.DocInclusionPredicate((_, apiDescription) => string.IsNullOrWhiteSpace(apiDescription.GroupName) == false);
            options.SwaggerGeneratorOptions.TagsSelector = apiDescription => new []
            {
                apiDescription.GroupName
            };
            
            options.OperationFilter<SwaggerOperationFilter>();
            // options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
            
            #region 启用swagger验证功能
            if (isAuthorizationEnable)
            {
                if (SwaggerAuthorizationHeaderParameterOperationFilter.IsHasAccessToken())
                {
                    options.OperationFilter<SwaggerAuthorizationHeaderParameterOperationFilter>();
                }
                
                var securitySchema = new OpenApiSecurityScheme
                {
                    Description = "Using the Authorization header with the Bearer scheme.",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                };

                options.AddSecurityDefinition("Bearer", securitySchema);

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    { securitySchema, new[] { "Bearer" } }
                });
            }
            #endregion
            
            setupAction?.Invoke(options);
        });
        
        return services;
    }

    public static IApplicationBuilder UseSwagger(this IApplicationBuilder app)
    {
        if (_config is not { IsEnable: true })
        {
            return app;
        }
        
        // swagger会自动忽略 Authorization 头，固出此下策
        if (SwaggerAuthorizationHeaderParameterOperationFilter.IsHasAccessToken())
        {
            app.Use((httpContext, next) => // For the oauth2-less!
            {
                if (httpContext.Request.Headers["authorize"].Any() && !httpContext.Request.Headers["Authorization"].Any())
                    httpContext.Request.Headers.Add("Authorization", $"Bearer {httpContext.Request.Headers["authorize"]}");

                return next();
            });
        }
        
        SwaggerBuilderExtensions.UseSwagger(app);
        app.UseSwaggerUI();
            
        return app;
    }
}