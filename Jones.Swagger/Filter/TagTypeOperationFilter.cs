using Jones.Helper;
using Jones.Swagger.Extensions;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class TagTypeOperationFilter<T> : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        if (context.ApiDescription.TryGetMethodInfo(out var method))
        {
            if (method.IsHasAttribute<T>())
            {
                operation.Summary += $"      ({AttributeHelper.GetDisplayName<T>()})";
            }
        }
    }
}