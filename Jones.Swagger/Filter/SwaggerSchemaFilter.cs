using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class SwaggerSchemaFilter : ISchemaFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        if (context.Type.IsEnum || (context.Type.GenericTypeArguments.Length == 1 && context.Type.GenericTypeArguments.First().IsEnum))
        {
            var enumType = context.Type.IsEnum ? context.Type : context.Type.GenericTypeArguments.First();
            var enumDescriptions = new List<string>();
            foreach (var enumValue in Enum.GetValues(enumType))
            {
                var enumName = Enum.GetName(enumType, enumValue);
                if (enumName == null)
                {
                    continue;
                }

                var enumField = enumType.GetField(enumName);
                var enumSubItemDescription = enumField?
                    .GetCustomAttributes(typeof(DisplayAttribute), false)
                    .Cast<DisplayAttribute>()
                    .FirstOrDefault()?.Name ?? enumName;
                
                enumDescriptions.Add($"{enumField?.GetRawConstantValue()} = {(enumValue.ToString() == enumSubItemDescription ? enumValue : $"{enumValue}（{enumSubItemDescription}）")}");
            }
                
            var enumDescription = enumType
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .Cast<DescriptionAttribute>()
                .FirstOrDefault()?.Description ?? enumType.Name;
                
            schema.Description = $"{enumDescription} : {string.Join(", ", enumDescriptions.ToArray())}";
        }
        else if(context.MemberInfo != null)
        {
            var schemaAttribute = context.MemberInfo.GetCustomAttributes(typeof(DisplayAttribute), false)
                .Cast<DisplayAttribute>()
                .FirstOrDefault();
            if (schemaAttribute != null)
            {
                schema.Description = schemaAttribute.Name;
            }
        }
        else
        {
            var schemaAttribute = context.Type.GetCustomAttributes(typeof(DisplayAttribute), false)
                .Cast<DisplayAttribute>()
                .FirstOrDefault();
            if (schemaAttribute != null)
            {
                schema.Description = schemaAttribute.Name;
            }
        }
    }
}