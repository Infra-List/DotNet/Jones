using Jones.Swagger.Attributes;
using Jones.Swagger.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class SwaggerHiddenFilter(IHttpContextAccessor httpContextAccessor) : IDocumentFilter
{
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        var query = httpContextAccessor.HttpContext?.Request.Query;
        
        foreach (var apiDescription in context.ApiDescriptions)
        {
            if (apiDescription.TryGetMethodInfo(out var method))
            {
                if (query?.ContainsKey(SwaggerHiddenAttribute.UrlQueryKey) == true)
                {
                    if (!method.IsHasAttribute<SwaggerHiddenAttribute>())
                    {
                        swaggerDoc.PathsRemove(apiDescription.RelativePath);
                    }
                }
                else
                {
                    if (method.IsHasAttribute<SwaggerHiddenAttribute>())
                    {
                        swaggerDoc.PathsRemove(apiDescription.RelativePath);
                    }
                }
            }
        }
    }
}