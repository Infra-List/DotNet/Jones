using System;
using Jones.Swagger.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class TagTypeDocumentFilter<T>(IHttpContextAccessor httpContextAccessor) : IDocumentFilter where T : Attribute
{
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        var query = httpContextAccessor.HttpContext?.Request.Query;
        var urlQueryKey = typeof(T).Name.Replace("Attribute", "");

        if (query?.ContainsKey(urlQueryKey) == true)
        {
            foreach (var apiDescription in context.ApiDescriptions)
            {
                if (apiDescription.TryGetMethodInfo(out var method))
                {
                    if (!method.IsHasAttribute<T>())
                    {
                        swaggerDoc.PathsRemove(apiDescription.RelativePath);
                    }
                }
            }
        }
    }
}