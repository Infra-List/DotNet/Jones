using System;
using System.Linq;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class HiddenApiFilter : IDocumentFilter
{
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        foreach (var apiDescription in context.ApiDescriptions)
        {
            if (apiDescription.TryGetMethodInfo(out var method))
            {
                if (method?.ReflectedType?.GetCustomAttributes(typeof(HiddenApiAttribute), true).Any() == true
                    || method?.GetCustomAttributes(typeof(HiddenApiAttribute), true).Any() == true)
                {
                    var key = "/" + apiDescription.RelativePath;
                    if (key.Contains("?"))
                    {
                        var idx = key.IndexOf("?", StringComparison.Ordinal);
                        key = key[..idx];
                    }
                    swaggerDoc.Paths.Remove(key);
                }
            }
        }
    }
}