using System.Collections.Generic;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class SwaggerAuthorizationHeaderParameterOperationFilter : IOperationFilter
{
    private static string? _accessToken;

    public static void SetAccessToken(string accessToken)
    {
        // if (System.IO.File.Exists(accessTokenFileName))
        // {
        //     _accessToken = System.IO.File.ReadAllText(accessTokenFileName);
        // }
        _accessToken = accessToken;
    }

    public static bool IsHasAccessToken() => !string.IsNullOrEmpty(_accessToken);
        
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        if (operation.Parameters == null)
            operation.Parameters = new List<OpenApiParameter>();

        operation.Parameters.Add(new OpenApiParameter 
        {
            Name = "authorize",
            In = ParameterLocation.Header,
            Description = "应为 Authorization，但swagger会自动忽略 Authorization header，固出此下策",
            Required = true,
            Schema = new OpenApiSchema
            {
                Type = "string",
                Default = new OpenApiString(_accessToken)
            }
        });
        
        // var filterDescriptors = context.ApiDescription.ActionDescriptor.FilterDescriptors;
        // var controllerActionDescriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;
        // if (controllerActionDescriptor == null)
        // {
        //     return;
        // }
        // var isControllerAuthorized = controllerActionDescriptor
        //     .ControllerTypeInfo.GetCustomAttributes<AuthorizeAttribute>(true)
        //     .Any();
        // var isAuthorized = controllerActionDescriptor
        //     .MethodInfo.GetCustomAttributes<AuthorizeAttribute>(true)
        //     .Any();
        // var isAllowAnonymous = controllerActionDescriptor
        //     .MethodInfo.GetCustomAttributes<AllowAnonymousAttribute>(true)
        //     .Any();
        //     
        // if ((isControllerAuthorized || isAuthorized) && !isAllowAnonymous)
        // {
        //     if (operation.Parameters == null)
        //         operation.Parameters = new List<OpenApiParameter>();
        //
        //     operation.Parameters.Add(new OpenApiParameter 
        //     {
        //         Name = "authorize",
        //         In = ParameterLocation.Header,
        //         Description = "应为 Authorization，但swagger会自动忽略 Authorization header，固出此下策",
        //         Required = true,
        //         Schema = new OpenApiSchema
        //         {
        //             Type = "string",
        //             Default = new OpenApiString(_accessToken)
        //         }
        //     });
        // }
    }
}