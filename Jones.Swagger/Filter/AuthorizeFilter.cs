using Jones.Swagger.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class AuthorizeFilter(IHttpContextAccessor httpContextAccessor) : IDocumentFilter
{
    private const string UrlQueryKey = "authorize";
    
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        var query = httpContextAccessor.HttpContext?.Request.Query;

        if (query?.ContainsKey(UrlQueryKey) == true)
        {
            foreach (var apiDescription in context.ApiDescriptions)
            {
                if (apiDescription.TryGetMethodInfo(out var method))
                {
                    if (!method.IsAuthorize())
                    {
                        swaggerDoc.PathsRemove(apiDescription.RelativePath);
                    }
                }
            }
        }
    }
}