using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Jones.Swagger.Filter;

public class SwaggerOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        foreach (var parameter in operation.Parameters)
        {
            if (parameter.Description == null && parameter.Schema.Description != null)
            {
                parameter.Description = parameter.Schema.Description;
            }
        }
    }
}