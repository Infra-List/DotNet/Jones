using System;

namespace Jones.Swagger.Attributes;

public class SwaggerHiddenAttribute : Attribute
{
    public const string UrlQueryKey = "hidden";
}