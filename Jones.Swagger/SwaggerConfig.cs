namespace Jones.Swagger;

public record SwaggerConfig
{
    public string? Title { get; set; }
    public bool? IsEnable { get; set; }
    public bool? IsUseHidden { get; set; }
    public string? AccessToken { get; set; }
}