using System.Reactive.Disposables;

namespace Jones.Rx.Extensions;

public static class DisposableExtensions
{
    public static T DisposeWith<T>(this T item, CompositeDisposable compositeDisposable) where T : IDisposable
    {
        compositeDisposable.Add(item);
        return item;
    }
}