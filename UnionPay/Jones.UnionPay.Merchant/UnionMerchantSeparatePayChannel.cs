﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jones.UnionPay.Merchant;

[Description("银联服务商分账支付渠道")]
public enum UnionMerchantSeparatePayChannel
{
    [Display(Name = "线下支付")]
    Offline = -1,
    
    [Display(Name = "线上支付")]
    Online = 1,
}