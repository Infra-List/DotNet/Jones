namespace Jones.UnionPay.Merchant.UmsExpress.Impl;

public interface IOfflineHttpClientManager
{
    HttpClient GetHttpClient();
}
    
internal class OfflineHttpClientManager : IOfflineHttpClientManager
{
    public static readonly string HttpClientName = typeof(OfflineHttpClientManager).FullName!;
    
    private readonly IHttpClientFactory _httpClientFactory;

    public OfflineHttpClientManager(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }
    
    public HttpClient GetHttpClient()
    {
        return _httpClientFactory.CreateClient(HttpClientName);
    }
}
