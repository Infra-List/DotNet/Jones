using Microsoft.Extensions.DependencyInjection;

namespace Jones.UnionPay.Merchant.UmsExpress.Impl.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddUnionMerchantSeparatePayService(this IServiceCollection services, 
        string offlineBaseAddress, string onlineBaseAddress, bool? isSandbox, string? sandboxOfflineBaseAddress, string? sandboxOnlineBaseAddress)
    {
        services.AddHttpClient(OfflineHttpClientManager.HttpClientName, 
            client => client.BaseAddress = new Uri(
                isSandbox == true && !string.IsNullOrEmpty(sandboxOfflineBaseAddress) ? sandboxOfflineBaseAddress : offlineBaseAddress));
        services.AddTransient<IOfflineHttpClientManager, OfflineHttpClientManager>();
        
        services.AddHttpClient(OnlineHttpClientManager.HttpClientName, 
            client => client.BaseAddress = new Uri(
                isSandbox == true && !string.IsNullOrEmpty(sandboxOnlineBaseAddress) ? sandboxOnlineBaseAddress : onlineBaseAddress));
        services.AddTransient<IOnlineHttpClientManager, OnlineHttpClientManager>();
        
        services.AddTransient<IUnionMerchantSeparateOfflinePayService, UnionMerchantSeparateOfflinePayServiceImpl>();
        services.AddTransient<IUnionMerchantSeparateOnlinePayService, UnionMerchantSeparateOnlinePayServiceImpl>();

        return services;
    }
}