using Jones.UnionPay.Merchant.UmsExpress.Entities;

namespace Jones.UnionPay.Merchant.UmsExpress.Impl;

// http://dhjt.chinaums.com/online-pay-doc/
public class UnionMerchantSeparateOnlinePayServiceImpl : IUnionMerchantSeparateOnlinePayService
{
    // https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_7&index=5
    public Task<UnionMerchantSeparateResponse<MiniProgramRequestPaymentParameter>> MiniProgramPayOrder(UnionMerchantInfo merchant, UnionMerchantSubMerchantInfo? subMerchant, string openId,
        string orderId, int totalAmount, string? description, string notifyUrl)
    {
        throw new NotImplementedException();
    }

    public Task<UnionMerchantSeparateResponse<OnlineOrderQueryResult>> Query(UnionMerchantInfo merchant, string? outTradeNo, string? tradeNo)
    {
        throw new NotImplementedException();
    }
}