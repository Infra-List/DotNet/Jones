using System.Text.Json.Serialization;

namespace Jones.UnionPay.Merchant.UmsExpress.Impl.Entities;

public record GoodsInfoUploadResult(
    [property: JsonPropertyName("errCode")] string ErrCode,
    [property: JsonPropertyName("errInfo")] string ErrInfo,
    [property: JsonPropertyName("transactionTime")] string TransactionTime,
    [property: JsonPropertyName("transactionDate")] string TransactionDate,
    [property: JsonPropertyName("settlementDate")] string SettlementDate,
    [property: JsonPropertyName("retrievalRefNum")] string RetrievalRefNum,
    [property: JsonPropertyName("sign")] string Sign);