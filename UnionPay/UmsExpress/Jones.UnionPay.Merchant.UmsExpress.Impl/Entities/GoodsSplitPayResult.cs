using System.Text.Json.Serialization;

namespace Jones.UnionPay.Merchant.UmsExpress.Impl.Entities;

public record GoodsSplitPayResult(
    [property: JsonPropertyName("errCode")] string ErrCode,
    [property: JsonPropertyName("errInfo")] string ErrInfo,
    [property: JsonPropertyName("transactionTime")] string TransactionTime,
    [property: JsonPropertyName("transactionDateWithYear")] string TransactionDateWithYear,
    [property: JsonPropertyName("settlementDateWithYear")] string SettlementDateWithYear,
    [property: JsonPropertyName("retrievalRefNum")] string RetrievalRefNum,
    [property: JsonPropertyName("transactionAmount")] int TransactionAmount,
    [property: JsonPropertyName("amount")] string Amount,
    [property: JsonPropertyName("orderId")] string OrderId,
    [property: JsonPropertyName("thirdPartyDiscountInstruction")] string ThirdPartyDiscountInstruction,
    [property: JsonPropertyName("thirdPartyName")] string ThirdPartyName,
    [property: JsonPropertyName("thirdPartyBuyerId")] string ThirdPartyBuyerId,
    [property: JsonPropertyName("thirdPartyBuyerUserName")] string ThirdPartyBuyerUserName,
    [property: JsonPropertyName("thirdPartyOrderId")] string ThirdPartyOrderId,
    [property: JsonPropertyName("thirdPartyPayInformation")] string ThirdPartyPayInformation,
    [property: JsonPropertyName("sign")] string Sign);