using System.Globalization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Jones.Extensions;
using Jones.UnionPay.Merchant.UmsExpress.Entities;
using Jones.UnionPay.Merchant.UmsExpress.Helper;
using Jones.UnionPay.Merchant.UmsExpress.Impl.Entities;
using Microsoft.Extensions.Logging;

namespace Jones.UnionPay.Merchant.UmsExpress.Impl;

public class UnionMerchantSeparateOfflinePayServiceImpl(
    ILogger<UnionMerchantSeparateOfflinePayServiceImpl> logger,
    IOfflineHttpClientManager offlineHttpClientManager)
    : IUnionMerchantSeparateOfflinePayService
{
    private const string Controller = "queryService/UmsWebPayPlugins";

    private static SortedDictionary<string, string> SetParams(SortedDictionary<string, string> paramsDictionary, UnionMerchantInfo merchant, SubAccountInfo? subMerchant, 
        int systemTraceNum, string merchantOrderId, string msgType, string version)
    {
        paramsDictionary.Add("merchantCode", merchant.MerchantId);
        paramsDictionary.Add("terminalCode", merchant.TerminalNumber);
        paramsDictionary.Add("systemTraceNum", systemTraceNum.ToString());
        paramsDictionary.Add("merchantOrderId", merchantOrderId);
        if (subMerchant != null)
        {
            var subMerchants = new [] {subMerchant};
            paramsDictionary.Add("subAccountInfo", JsonSerializer.Serialize(subMerchants));
        }
        
        paramsDictionary.Add("instMid", "B2CSPLITDEFAULT");
        paramsDictionary.Add("version", version);
        paramsDictionary.Add("msgType", msgType);
        paramsDictionary.Add("signType", "MD5");

        var paramStringBuilder = new StringBuilder();
        foreach(var param in paramsDictionary)
        {
            if (paramStringBuilder.Length > 0)
            {
                paramStringBuilder.Append('&');
            }
            paramStringBuilder.Append($"{param.Key}={param.Value}");
        }
        paramsDictionary.Add("sign", $"{paramStringBuilder}{merchant.MerchantSecret}".Md5());
        
        return paramsDictionary;
    }

    // https://project-l.oss-cn-shenzhen.aliyuncs.com/doc/大华捷通B扫C分账交易接口v20200908.pdf
    private async Task<UnionMerchantSeparateResponse<GoodsInfoUploadResult>> GoodsInfoUpload(UnionMerchantInfo merchant, UnionMerchantSubMerchantInfo? subMerchant,
        int systemTraceNum, string orderId, int totalAmount)
    {
        var paramsDictionary = new SortedDictionary<string, string>
        {
            { "transactionAmount", totalAmount.ToString() }
        };
        paramsDictionary = SetParams(paramsDictionary, merchant, 
            subMerchant == null ? null : new SubAccountInfo(subMerchant.MerchantId, totalAmount.GetDivision(subMerchant.Rate)), 
            systemTraceNum, orderId, "goodsInfo", "20191031");
        
        // 打印日志
        var paramStringBuilder = new StringBuilder();
        foreach(var param in paramsDictionary)
        {
            paramStringBuilder.Append(Environment.NewLine);
            paramStringBuilder.Append($"{param.Key}:{param.Value}");
        }
        logger.LogDebug("[银联大华捷通付款码支付前商品信息上传] - [参数: {ErrorMsg}]]", paramStringBuilder);
        
        var response = await offlineHttpClientManager.GetHttpClient().PostAsync(Controller, new FormUrlEncodedContent(paramsDictionary));
        response.EnsureSuccessStatusCode();
        var responseContent = await response.Content.ReadAsStringAsync();
        var goodsInfoUploadResult = JsonSerializer.Deserialize<GoodsInfoUploadResult>(responseContent);
        if (goodsInfoUploadResult == null)
        {
            throw new HttpRequestException();
        }
        if (!UnionPayMerchantUmsExpressHelper.IsSuccess(goodsInfoUploadResult.ErrCode))
        {
            return UnionMerchantSeparateResponse<GoodsInfoUploadResult>.Error(responseContent, goodsInfoUploadResult.ErrCode, goodsInfoUploadResult.ErrInfo);
        }
        return UnionMerchantSeparateResponse<GoodsInfoUploadResult>.Success(goodsInfoUploadResult);
    }
    public async Task<UnionMerchantSeparateResponse<PaymentCodePayOrderResult>> PaymentCodePayOrder(UnionMerchantInfo merchant, UnionMerchantSubMerchantInfo? subMerchant, 
        string paymentCode, int systemTraceNum, string orderId, int totalAmount)
    {
        var goodsInfoUploadResultResult = await GoodsInfoUpload(merchant, subMerchant, systemTraceNum, orderId, totalAmount);
        if (!goodsInfoUploadResultResult.CheckIsSuccess(out var goodsInfoUploadResult))
        {
            return UnionMerchantSeparateResponse<PaymentCodePayOrderResult>.Error(goodsInfoUploadResultResult.Content, goodsInfoUploadResultResult.ErrorCode, goodsInfoUploadResultResult.ErrorMessage);
        }
        
        var paramsDictionary = new SortedDictionary<string, string>
        {
            { "transactionAmount", totalAmount.ToString() },
            { "transactionCurrencyCode", "156" },
            { "retrievalRefNumOfAddGoodsInfo", goodsInfoUploadResult.RetrievalRefNum },
            { "payMode", "CODE_SCAN" },
            { "payCode", paymentCode },
        };
        paramsDictionary = SetParams(paramsDictionary, merchant, 
            subMerchant == null ? null : new SubAccountInfo(subMerchant.MerchantId, totalAmount.GetDivision(subMerchant.Rate)), 
            systemTraceNum, orderId, "goodsSplitPay", "20191031");
        
        // 打印日志
        var paramStringBuilder = new StringBuilder();
        foreach(var param in paramsDictionary)
        {
            paramStringBuilder.Append(Environment.NewLine);
            paramStringBuilder.Append($"{param.Key}:{param.Value}");
        }
        logger.LogDebug("[银联大华捷通付款码支付] - [参数: {ErrorMsg}]]", paramStringBuilder);
        
        var response = await offlineHttpClientManager.GetHttpClient().PostAsync(Controller, new FormUrlEncodedContent(paramsDictionary));
        response.EnsureSuccessStatusCode();
        var responseContent = await response.Content.ReadAsStringAsync();
        var goodsSplitPayResult = JsonSerializer.Deserialize<GoodsSplitPayResult>(responseContent);
        if (goodsSplitPayResult == null)
        {
            throw new HttpRequestException();
        }
        return UnionPayMerchantUmsExpressHelper.IsSuccess(goodsSplitPayResult.ErrCode) 
            ? UnionMerchantSeparateResponse<PaymentCodePayOrderResult>.Success(new PaymentCodePayOrderResult(int.Parse(goodsSplitPayResult.Amount), goodsSplitPayResult.ThirdPartyName,
                null, goodsSplitPayResult.ThirdPartyOrderId, goodsSplitPayResult.ThirdPartyBuyerId, goodsSplitPayResult.ThirdPartyBuyerUserName, goodsSplitPayResult.ThirdPartyDiscountInstruction, goodsSplitPayResult.ThirdPartyPayInformation, 
                DateTime.ParseExact($"{goodsSplitPayResult.TransactionDateWithYear} {goodsSplitPayResult.TransactionTime}", "yyyyMMdd HHmmss", CultureInfo.InvariantCulture).AddHours(-8),
                DateTime.ParseExact(goodsSplitPayResult.SettlementDateWithYear, "yyyyMMdd", CultureInfo.InvariantCulture).AddHours(-8)))
            : UnionMerchantSeparateResponse<PaymentCodePayOrderResult>.Error(responseContent, goodsSplitPayResult.ErrCode, goodsSplitPayResult.ErrInfo);
    }
    
    public Task<UnionMerchantSeparateResponse<OfflineOrderQueryResult>> Query(UnionMerchantInfo merchant, int systemTraceNum, string orderId, DateTime transactionDate)
    {
        throw new NotImplementedException();
    }

    private record SubAccountInfo(
        [property: JsonPropertyName("merchantCode")] string MerchantCode,
        [property: JsonPropertyName("amount")] int Amount);
}