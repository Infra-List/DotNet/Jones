namespace Jones.UnionPay.Merchant.UmsExpress.Impl;

public interface IOnlineHttpClientManager
{
    HttpClient GetHttpClient();
}

public class OnlineHttpClientManager : IOnlineHttpClientManager
{
    public static readonly string HttpClientName = typeof(OnlineHttpClientManager).FullName!;
    
    private readonly IHttpClientFactory _httpClientFactory;

    public OnlineHttpClientManager(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }
    
    public HttpClient GetHttpClient()
    {
        return _httpClientFactory.CreateClient(HttpClientName);
    }
}