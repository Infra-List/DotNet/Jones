namespace Jones.UnionPay.Merchant.UmsExpress.Helper;

public static class SubMerchantHelper
{
    public static int GetDivision(this int totalAmount, int? rate)
    {
        return rate == null ? totalAmount : (int)(totalAmount * rate.Value / 10000m);
    }
}