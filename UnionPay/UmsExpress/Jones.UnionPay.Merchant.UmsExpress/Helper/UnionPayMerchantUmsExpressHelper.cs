namespace Jones.UnionPay.Merchant.UmsExpress.Helper;

public static class UnionPayMerchantUmsExpressHelper
{
    public static bool IsSuccess(string errCode)
    {
        return errCode is "SUCCESS" or "00";
    }
}