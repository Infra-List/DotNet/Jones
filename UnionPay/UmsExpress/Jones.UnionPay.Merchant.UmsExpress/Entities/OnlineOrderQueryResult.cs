using System.ComponentModel.DataAnnotations;

namespace Jones.UnionPay.Merchant.UmsExpress.Entities;

public record OnlineOrderQueryResult(
    [Display(Name = "交易状态",Prompt = """
                                     NEW_ORDER: 新订单;
                                     TRADE_CLOSED: 在指定时间段内未支付时关闭的交易, 在交易完成全额退款成功时关闭的交易, 支付失败的交易, 次状态的交易不允许进行任何操作;
                                     WAIT_BUYER_PAY: 等待买家付款;
                                     TRADE_SUCCESS: 支付成功;
                                     TRADE_REFUND: 订单转入退货流程, 退货可能是部分也可能是全部;
                                     UNKNOWN 不明确的交易状态
                                     """)]
    string? Status,
    [Display(Name = "实收金额")] int? ReceiptAmount,
    [Display(Name = "第三方名称")] string? ThirdPartyName,
    [Display(Name = "第三方平台的状态")] string? ThirdPartyStatus,
    [Display(Name = "第三方订单号")] string? ThirdPartyOrderId,
    [Display(Name = "第三方买家Id")] string? ThirdPartyBuyerId,
    [Display(Name = "第三方买家用户名")] string? ThirdPartyBuyerUserName,
    [Display(Name = "交易时间")] DateTime? TransactionTime,
    [Display(Name = "结算日期")] DateTime? SettlementDate);