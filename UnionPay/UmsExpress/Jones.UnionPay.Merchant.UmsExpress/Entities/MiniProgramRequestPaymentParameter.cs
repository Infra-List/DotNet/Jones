namespace Jones.UnionPay.Merchant.UmsExpress.Entities;

public record MiniProgramRequestPaymentParameter(string TimeStamp, string NonceStr, string Package, string SignType, string PaySign);