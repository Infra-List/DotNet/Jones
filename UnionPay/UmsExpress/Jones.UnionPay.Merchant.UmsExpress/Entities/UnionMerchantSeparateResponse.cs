using System.Diagnostics.CodeAnalysis;

namespace Jones.UnionPay.Merchant.UmsExpress.Entities;

public record UnionMerchantSeparateResponse(bool IsSuccess, string? Content, string? ErrorCode, string? ErrorMessage)
{
    public static UnionMerchantSeparateResponse Success() => 
        new (true, null, null, null);
    
    public static UnionMerchantSeparateResponse Error(string? content, string? errorCode, string? errorMessage) => 
        new (false, content, errorCode, errorMessage);
}

public record UnionMerchantSeparateResponse<T>(bool IsSuccess, string? Content, string? ErrorCode, string? ErrorMessage, T? Data)
    : UnionMerchantSeparateResponse(IsSuccess, Content, ErrorCode, ErrorMessage)
{
    public static UnionMerchantSeparateResponse<T> Success(T data) => 
        new (true, null, null, null, data);
    
    public new static UnionMerchantSeparateResponse<T> Error(string? content, string? errorCode, string? errorMessage) => 
        new (false, content, errorCode, errorMessage, default);
    
    public bool CheckIsSuccess([NotNullWhen(true)] out T? data)
    {
        data = Data;
        return IsSuccess;
    }
}