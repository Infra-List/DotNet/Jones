using System.ComponentModel.DataAnnotations;

namespace Jones.UnionPay.Merchant.UmsExpress.Entities;

public record OfflineOrderQueryResult(
    [Display(Name = "查询结果", Prompt = "00: 成功, 01: 超时 其余均为失败")]
    string QueryResCode,
    [Display(Name = "查询结果描述", Prompt = "当queryResCode 不为00时存在")]
    string QueryResInfo,
    [Display(Name = "第三方名称")] string? ThirdPartyName,
    [Display(Name = "交易时间")] DateTime? TransactionTime,
    [Display(Name = "结算日期")] DateTime? SettlementDate);