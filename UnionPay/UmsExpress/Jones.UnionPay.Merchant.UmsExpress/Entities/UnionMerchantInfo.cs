using System.ComponentModel.DataAnnotations;

namespace Jones.UnionPay.Merchant.UmsExpress.Entities;

public record UnionMerchantInfo(
    [Display(Name = "终端号")] string TerminalNumber,
    [Display(Name = "商户号")] string MerchantId,
    string MerchantSecret);