using System.ComponentModel.DataAnnotations;

namespace Jones.UnionPay.Merchant.UmsExpress.Entities;

public record UnionMerchantSubMerchantInfo(
    [Display(Name = "商户号")] string MerchantId,
    [Display(Name = "费率", Prompt = "单位为万分之")] int? Rate);