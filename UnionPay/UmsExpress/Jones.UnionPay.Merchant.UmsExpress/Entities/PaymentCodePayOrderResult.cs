using System.ComponentModel.DataAnnotations;

namespace Jones.UnionPay.Merchant.UmsExpress.Entities;

public record PaymentCodePayOrderResult(
    [Display(Name = "实收金额")] int? ReceiptAmount,
    [Display(Name = "第三方名称")] string? ThirdPartyName,
    [Display(Name = "第三方平台的状态")] string? ThirdPartyStatus,
    [Display(Name = "第三方订单号")] string? ThirdPartyOrderId,
    [Display(Name = "第三方买家Id")] string? ThirdPartyBuyerId,
    [Display(Name = "第三方买家用户名")] string? ThirdPartyBuyerUserName,
    [Display(Name = "第三方优惠说明")] string? ThirdPartyDiscountInstruction,
    [Display(Name = "第三方支付信息")] string? ThirdPartyPayInformation,
    [Display(Name = "交易时间")] DateTime? TransactionTime,
    [Display(Name = "结算日期")] DateTime? SettlementDate);