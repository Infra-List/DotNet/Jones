using Jones.UnionPay.Merchant.UmsExpress.Entities;

namespace Jones.UnionPay.Merchant.UmsExpress;

public interface IUnionMerchantSeparateOnlinePayService
{
    // 微信小程序支付
    Task<UnionMerchantSeparateResponse<MiniProgramRequestPaymentParameter>> MiniProgramPayOrder(UnionMerchantInfo merchant, UnionMerchantSubMerchantInfo? subMerchant, 
        string openId, string orderId, int totalAmount, string? description, string notifyUrl);
    
    // 订单查询
    Task<UnionMerchantSeparateResponse<OnlineOrderQueryResult>> Query(UnionMerchantInfo merchant, string? outTradeNo, string? tradeNo);
}