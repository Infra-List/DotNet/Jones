using Jones.UnionPay.Merchant.UmsExpress.Entities;

namespace Jones.UnionPay.Merchant.UmsExpress;

public interface IUnionMerchantSeparateOfflinePayService
{
    // 付款码支付
    Task<UnionMerchantSeparateResponse<PaymentCodePayOrderResult>> PaymentCodePayOrder(UnionMerchantInfo merchant, UnionMerchantSubMerchantInfo? subMerchant, 
        string paymentCode, int systemTraceNum, string orderId, int totalAmount);
    
    // 订单查询
    Task<UnionMerchantSeparateResponse<OfflineOrderQueryResult>> Query(UnionMerchantInfo merchant, int systemTraceNum, string orderId, DateTime transactionDate);
}