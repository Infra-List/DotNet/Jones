using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jones.UnionPay.Types;

[Description("银联支付方式")]
public enum UnionPayType
{
    [Display(Name = "付款码支付")]
    PaymentCode = 11,
    // [Display(Name = "App支付")]
    // App = 31,
    [Display(Name = "微信小程序支付")]
    WeChatMiniProgram = 51,
}