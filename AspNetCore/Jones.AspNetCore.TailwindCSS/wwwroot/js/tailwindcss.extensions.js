window.tailwindcss = {
    version: "1.0.0"
};
window.tailwindcss.fileInput = {
    version: "1.0.0",
    init: function (option, dropZoneElement, inputFileElement) {

        const hoverClass = "input-file-container-focus";
        // Add a class when the user drags a file over the drop zone
        function onDragHover(e) {
            e.preventDefault();
            dropZoneElement.classList.add(hoverClass);
        }

        function onDragLeave(e) {
            e.preventDefault();
            dropZoneElement.classList.remove(hoverClass);
        }

        // Handle the paste and drop events
        function onDrop(e) {
            e.preventDefault();
            dropZoneElement.classList.remove(hoverClass);

            // Set the files property of the input element and raise the change event
            inputFileElement.files = e.dataTransfer.files;
            const event = new Event('change', { bubbles: true });
            inputFileElement.dispatchEvent(event);
        }

        function onPaste(e) {
            // Set the files property of the input element and raise the change event
            inputFileElement.files = e.clipboardData.files;
            const event = new Event('change', { bubbles: true });
            inputFileElement.dispatchEvent(event);
        }

        // Register all events
        if (option.isAllowDrop) {
            dropZoneElement.addEventListener("dragenter", onDragHover);
            dropZoneElement.addEventListener("dragover", onDragHover);
            dropZoneElement.addEventListener("dragleave", onDragLeave);
            dropZoneElement.addEventListener("drop", onDrop);
        }

        if (option.isAllowPaste) {
            dropZoneElement.addEventListener('paste', onPaste);
        }

        // The returned object allows to unregister the events when the Blazor component is destroyed
        return {
            dispose: () => {
                if (option.isAllowDrop) {
                    dropZoneElement.removeEventListener('dragenter', onDragHover);
                    dropZoneElement.removeEventListener('dragover', onDragHover);
                    dropZoneElement.removeEventListener('dragleave', onDragLeave);
                    dropZoneElement.removeEventListener("drop", onDrop);
                }

                if (option.isAllowPaste) {
                    dropZoneElement.removeEventListener('paste', onPaste);
                }
            }
        }
    }
};