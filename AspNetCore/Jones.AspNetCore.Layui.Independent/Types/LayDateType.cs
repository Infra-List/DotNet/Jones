using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Layui.Independent.Types;

public enum LayDateType
{
    [Display(Name = "year", Prompt = "请选择年份")]
    Year,
    
    [Display(Name = "month", Prompt = "请选择月份")]
    Month,
    
    [Display(Name = "date", Prompt = "请选择日期")]
    Date,
    
    [Display(Name = "time", Prompt = "请选择时间")]
    Time,
    
    [Display(Name = "datetime", Prompt = "请选择日期时间")]
    Datetime,
}