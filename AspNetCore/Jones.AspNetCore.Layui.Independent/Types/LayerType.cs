using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Layui.Independent.Types;

public enum LayerType
{
    [Display(Name = "信息框")]
    Info = 0,
        
    [Display(Name = "页面层")]
    Page = 1,
        
    [Display(Name = "iframe层")]
    Iframe = 2,
        
    [Display(Name = "加载层")]
    Loading = 3,
        
    [Display(Name = "tips层")]
    Tips = 4,
}