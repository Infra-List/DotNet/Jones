using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Layui.Independent.Types;

public enum LayDateTrigger
{
    [Display(Name = "focus")]
    Focus,
        
    [Display(Name = "click")]
    Click,
}