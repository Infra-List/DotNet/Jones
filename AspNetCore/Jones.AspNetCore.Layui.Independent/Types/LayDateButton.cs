using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Layui.Independent.Types;

public enum LayDateButton
{
    [Display(Name = "clear")]
    Clear,
        
    [Display(Name = "now")]
    Now,
        
    [Display(Name = "confirm")]
    Confirm
}