using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Layui.Independent.Types;

public enum LayDateLang
{
    [Display(Name = "cn")]
    Cn,
        
    [Display(Name = "en")]
    En
}