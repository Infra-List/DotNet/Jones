using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Layui.Independent.Types;

public enum LayerSize
{
    [Display(Name = "300px")]
    Small,
        
    [Display(Name = "500px")]
    Medium,
        
    [Display(Name = "800px")]
    Large,
        
    [Display(Name = "1140px")]
    ExtraLarge
}