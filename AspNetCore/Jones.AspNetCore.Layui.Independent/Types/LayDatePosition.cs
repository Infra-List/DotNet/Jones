using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Layui.Independent.Types;

public enum LayDatePosition
{
    [Display(Name = "abolute")]
    Abolute,
        
    [Display(Name = "fixed")]
    Fixed,
        
    [Display(Name = "static")]
    Static
}