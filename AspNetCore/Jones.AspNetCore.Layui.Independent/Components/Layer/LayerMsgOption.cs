namespace Jones.AspNetCore.Layui.Independent.Components.Layer;

public record LayerMsgOption
{
    /// <summary>
    /// x秒关闭（如果不配置，默认是3秒）
    /// </summary>
    public double? Time { get; set; }
        
    /// <summary>
    /// 图标
    /// 1-绿勾；2-黄叉；3-黄问号；4-灰锁；5-红伤心；6-绿笑
    /// </summary>
    public int? Icon { get; set; }
}