using System;
using System.Threading.Tasks;
using Jones.AspNetCore.Components;
using Jones.AspNetCore.Layui.Independent.Types;
using Jones.AspNetCore.Utils;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Layui.Independent.Components.Layer;

public abstract class LayerBase<TResult> : JonesComponentBase
{
    protected string? Class => CssBuilder.Default()
        .AddClassFromAttributes(AdditionalAttributes)
        .Build();
        
    protected ElementReference ContentElement { get; set; }
        
    [Parameter]
    public RenderFragment ChildContent { get; set; }
        
    [Inject]
    protected IJSRuntime JSRuntime { get; set; }
        
    private IJSObjectReference? _layer;

    private TaskCompletionSource<TResult?>? _returnTask;
        
    [Parameter]
    public LayerSize? Size { get; set; }

    #region 选择项

    /// <summary>
    /// 基本层类型
    /// 类型：Number，默认：0
    /// layer提供了5种层类型。可传入的值有：0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）。
    /// 若你采用layer.open({type: 1})方式调用，则type为必填项（信息框除外）
    /// </summary>
    [Parameter]
    public LayerType? Type { get; set; } = LayerType.Page;
        
    /// <summary>
    /// 标题
    /// 类型：String/Array/Boolean，默认：'信息'
    /// title支持三种类型的值，若你传入的是普通的字符串，如title :'我是标题'，那么只会改变标题文本；
    /// 若你还需要自定义标题区域样式，那么你可以title: ['文本', 'font-size:18px;']，
    /// 数组第二项可以写任意css样式；如果你不想显示标题栏，你可以title: false
    /// </summary>
    [Parameter]
    public dynamic? Title  { get; set; }
        
    /// <summary>
    /// 样式类名
    /// 类型：String，默认：''
    /// skin不仅允许你传入layer内置的样式class名，还可以传入您自定义的class名。
    /// 这是一个很好的切入点，意味着你可以借助skin轻松完成不同的风格定制。目前layer内置的skin有：layui-layer-lanlayui-layer-molv，
    /// 未来我们还会选择性地内置更多，但更推荐您自己来定义。以下是一个自定义风格的简单例子
    /// </summary>
    [Parameter]
    public string? Skin { get; set; }
        
    /// <summary>
    /// 宽高
    /// 类型：String/Array，默认：'auto'
    /// 在默认状态下，layer是宽高都自适应的，但当你只想定义宽度时，你可以area: '500px'，高度仍然是自适应的。
    /// 当你宽高都要定义时，你可以area: ['500px', '300px']
    /// <see cref="LayerSize"/>
    /// </summary>
    [Parameter]
    public dynamic? Area  { get; set; }
        
    /// <summary>
    /// 坐标
    /// 类型：String/Array，默认：垂直水平居中
    /// offset默认情况下不用设置。但如果你不想垂直水平居中，你还可以进行以下赋值：
    /// https://www.layui.com/doc/modules/layer.html#offset
    /// </summary>
    [Parameter]
    public dynamic? Offset  { get; set; }
        
    /// <summary>
    /// 遮罩
    /// 类型：String/Array/Boolean，默认：0.3
    /// 即弹层外区域。默认是0.3透明度的黑色背景（'#000'）。
    /// 如果你想定义别的颜色，可以shade: [0.8, '#393D49']；如果你不想显示遮罩，可以shade: 0
    /// </summary>
    [Parameter]
    public dynamic? Shade  { get; set; }
        
    /// <summary>
    /// 是否点击遮罩关闭
    /// 类型：Boolean，默认：false
    /// 如果你的shade是存在的，那么你可以设定shadeClose来控制点击弹层外区域关闭。
    /// </summary>
    [Parameter]
    public bool? IsShadeClose { get; set; }
        
    /// <summary>
    /// 自动关闭所需毫秒
    /// 类型：Number，默认：0
    /// 默认不会自动关闭。当你想自动关闭时，可以time: 5000，即代表5秒后自动关闭，注意单位是毫秒（1秒=1000毫秒）
    /// </summary>
    [Parameter]
    public int? Time { get; set; }
        
    /// <summary>
    /// 用于控制弹层唯一标识
    /// 类型：String，默认：空字符
    /// 设置该值后，不管是什么类型的层，都只允许同时弹出一个。一般用于页面层和iframe层模式
    /// </summary>
    public string? Id { get; set; }
        
    /// <summary>
    /// 弹出动画
    /// 类型：Number，默认：0
    /// 我们的出场动画全部采用CSS3。这意味着除了ie6-9，其它所有浏览器都是支持的。
    /// 目前anim可支持的动画类型有0-6 如果不想显示动画，设置 anim: -1 即可。另外需要注意的是，3.0之前的版本用的是 shift 参数
    /// https://www.layui.com/doc/modules/layer.html#anim
    /// </summary>
    [Parameter]
    public int? Anim { get; set; }
        
    /// <summary>
    /// 关闭动画 （layer 3.0.3新增）
    /// 类型：Boolean，默认：true
    /// 默认情况下，关闭层时会有一个过度动画。如果你不想开启，设置 isOutAnim: false 即可
    /// </summary>
    [Parameter]
    public bool? IsOutAnim { get; set; }
        
    // /// <summary>
    // /// 最大最小化
    // /// 类型：Boolean，默认：false
    // /// 该参数值对type:1和type:2有效。默认不显示最大小化按钮。需要显示配置maxmin: true即可
    // /// </summary>
    // public bool? Maxmin { get; set; }
        
    /// <summary>
    /// 固定
    /// 类型：Boolean，默认：true
    /// 即鼠标滚动时，层是否固定在可视区域。如果不想，设置fixed: false即可
    /// </summary>
    [Parameter]
    public bool? Fixed { get; set; }

    /// <summary>
    /// 是否允许拉伸
    /// 类型：Boolean，默认：true
    /// 默认情况下，你可以在弹层右下角拖动来拉伸尺寸。如果对指定的弹层屏蔽该功能，设置 false即可。该参数对loading、tips层无效
    /// </summary>
    [Parameter]
    public bool? Resize { get; set; }

    /// <summary>
    /// 是否允许浏览器出现滚动条
    /// 类型：Boolean，默认：true
    /// 默认允许浏览器滚动，如果设定scrollbar: false，则屏蔽
    /// </summary>
    [Parameter]
    public bool? Scrollbar { get; set; }
        
    /// <summary>
    /// 最大宽度
    /// 类型：Number，默认：360
    /// 请注意：只有当area: 'auto'时，maxWidth的设定才有效。
    /// </summary>
    [Parameter]
    public int? MaxWidth { get; set; }
        
    /// <summary>
    /// 最大高度
    /// 类型：Number，默认：无
    /// 请注意：只有当高度自适应时，maxHeight的设定才有效。
    /// </summary>
    [Parameter]
    public int? MaxHeight { get; set; }
        
    /// <summary>
    /// 层叠顺序
    /// 类型：，默认：19891014（贤心生日 0.0）
    /// 一般用于解决和其它组件的层叠冲突。
    /// </summary>
    [Parameter]
    public int? ZIndex { get; set; }
        
    /// <summary>
    /// 触发拖动的元素
    /// 类型：String/DOM/Boolean，默认：'.layui-layer-title'
    /// 默认是触发标题区域拖拽。如果你想单独定义，指向元素的选择器或者DOM即可。如move: '.mine-move'。
    /// 你还配置设定move: false来禁止拖拽
    /// </summary>
    [Parameter]
    public dynamic? Move  { get; set; }
        
    /// <summary>
    /// 是否允许拖拽到窗口外
    /// 类型：Boolean，默认：false
    /// 默认只能在窗口内拖拽，如果你想让拖到窗外，那么设定moveOut: true即可
    /// </summary>
    [Parameter]
    public bool? MoveOut { get; set; }
        
    /// <summary>
    /// tips方向和颜色
    /// 类型：Number/Array，默认：2
    /// tips层的私有参数。支持上右下左四个方向，通过1-4进行方向设定。
    /// 如tips: 3则表示在元素的下面出现。有时你还可能会定义一些颜色，可以设定tips: [1, '#c00']
    /// </summary>
    [Parameter]
    public dynamic? Tips  { get; set; }
        
    /// <summary>
    /// 是否允许多个tips
    /// 类型：Boolean，默认：false
    /// 允许多个意味着不会销毁之前的tips层。通过tipsMore: true开启
    /// </summary>
    [Parameter]
    public bool? TipsMore { get; set; }
        
        
        
    /// <summary>
    /// 层弹出后的成功回调方法
    /// 类型：Function，默认：null
    /// 当你需要在层创建完毕时即执行一些语句，可以通过该回调。success会携带两个参数，分别是当前层DOM当前层索引。
    /// </summary>
    public string? Success { get; set; }
        
    /// <summary>
    /// 确定按钮回调方法
    /// 类型：Function，默认：null
    /// 该回调携带两个参数，分别为当前层索引、当前层DOM对象。
    /// </summary>
    public string? Yes { get; set; }
        
    /// <summary>
    /// 右上角关闭按钮触发的回调
    /// 类型：Function，默认：null
    /// 该回调携带两个参数，分别为：当前层索引参数（index）、当前层的DOM对象（layero），默认会自动触发关闭。如果不想关闭，return false即可
    /// </summary>
    public string? Cancel { get; set; }
        
    /// <summary>
    /// 层销毁后触发的回调
    /// 类型：Function，默认：null
    /// 无论是确认还是取消，只要层被销毁了，end都会执行，不携带任何参数。
    /// </summary>
    public string? End { get; set; }
        
    /// <summary>
    /// 最大化后触发的回调
    /// 类型：Function，默认：null
    /// 携带两个参数，即当前层 DOM、当前层索引
    /// </summary>
    public string? Full { get; set; }
        
    /// <summary>
    /// 最小化后触发的回调
    /// 类型：Function，默认：null
    /// 携带两个参数，即当前层 DOM、当前层索引
    /// </summary>
    public string? Min { get; set; }
        
    /// <summary>
    /// 还原后触发的回调
    /// 类型：Function，默认：null
    /// 携带两个参数，即当前层 DOM、当前层索引
    /// </summary>
    public string? Restore { get; set; }

    #endregion
    
    [Parameter]
    public Action<TResult>? OnCompleted { get; set; }
        
    [Parameter]
    public Action<IJSObjectReference, ElementReference>? OnSuccess { get; set; }
        
    [Parameter]
    public Action<IJSObjectReference, ElementReference>? OnYes { get; set; }
        
    [Parameter]
    public Action<IJSObjectReference, ElementReference>? OnCancel { get; set; }
        
    [Parameter]
    public Action<IJSObjectReference, ElementReference>? OnFull { get; set; }
        
    [Parameter]
    public Action<IJSObjectReference, ElementReference>? OnMin { get; set; }
        
    [Parameter]
    public Action<IJSObjectReference, ElementReference>? OnRestore { get; set; }
        
    [Parameter]
    public Action? OnEnd { get; set; }

    public async Task<TResult?> Show(Action? beforeShow = null)
    {
        beforeShow?.Invoke();
        _returnTask = new TaskCompletionSource<TResult?>();
        await DoShow();
        return await _returnTask.Task;
    }

    private async Task DoShow()
    {
        var option = new LayerOption
        {
            Type = Type == null ? null : (int)Type.Value,
            Title = Title,
            Skin = Skin,
            Area = Size?.GetDisplayName(),
            Offset = Offset,
            Shade = Shade,
            ShadeClose = IsShadeClose,
            Time = Time,
            Id = Id,
            Anim = Anim,
            IsOutAnim = IsOutAnim,
            Fixed = Fixed,
            Resize = Resize,
            Scrollbar = Scrollbar,
            MaxWidth = MaxWidth,
            MaxHeight = MaxHeight,
            ZIndex = ZIndex,
            Move = Move,
            MoveOut = MoveOut,
            Tips = Tips,
            TipsMore = TipsMore,
            Success = OnSuccess == null ? null : nameof(OnSuccessCallback),
            Yes = OnYes == null ? null : nameof(OnYesCallback),
            Cancel = OnCancel == null ? null : nameof(OnCancelCallback),
            End = nameof(OnEndCallback),
            Full = OnFull == null ? null : nameof(OnFullCallback),
            Min = OnMin == null ? null : nameof(OnMinCallback),
            Restore = OnRestore == null ? null : nameof(OnRestoreCallback),
        };
        _layer = await JSRuntime.InvokeAsync<IJSObjectReference>("layuiX.layer.open", ContentElement, option, DotNetObjectReference.Create(this));
    }
    
    [JSInvokable]
    public Task OnSuccessCallback(IJSObjectReference layer, ElementReference element)
    {
        OnSuccess?.Invoke(layer, element);
        return Task.CompletedTask;
    }
    
    [JSInvokable]
    public Task OnYesCallback(IJSObjectReference layer, ElementReference element)
    {
        OnYes?.Invoke(layer, element);
        return Task.CompletedTask;
    }
    
    [JSInvokable]
    public Task OnCancelCallback(IJSObjectReference layer, ElementReference element)
    {
        OnCancel?.Invoke(layer, element);
        return Task.CompletedTask;
    }
    
    [JSInvokable]
    public Task OnEndCallback()
    {
        OnEnd?.Invoke();
        return Task.CompletedTask;
    }
    
    [JSInvokable]
    public Task OnFullCallback(IJSObjectReference layer, ElementReference element)
    {
        OnFull?.Invoke(layer, element);
        return Task.CompletedTask;
    }
    
    [JSInvokable]
    public Task OnMinCallback(IJSObjectReference layer, ElementReference element)
    {
        OnMin?.Invoke(layer, element);
        return Task.CompletedTask;
    }
    
    [JSInvokable]
    public Task OnRestoreCallback(IJSObjectReference layer, ElementReference element)
    {
        OnRestore?.Invoke(layer, element);
        return Task.CompletedTask;
    }

    private async ValueTask DoClose()
    {
        if (_layer != null)
        {
            await JSRuntime.InvokeVoidAsync("layuiX.layer.close", _layer);
            _layer = null;
        }
    }
    
    public async Task Complete(TResult result)
    {
        await DoClose();
        SetResult(result);
        OnCompleted?.Invoke(result);
    }

    private void SetResult(TResult? result)
    {
        _returnTask?.SetResult(result);
        _returnTask = null;
    }

    public async Task Hide() => await DoClose();
}