using System;

namespace Jones.AspNetCore.Layui.Independent.Components.LayDate;

public record LayDateDateTime
{
    public int Year { get; }
    public int Month { get; }
    public int Date { get; }
    public int Hours { get; }
    public int Minutes { get; }
    public int Seconds { get; }

    public LayDateDateTime(int year, int month, int date, int hours, int minutes, int seconds)
    {
        Year = year;
        Month = month;
        Date = date;
        Hours = hours;
        Minutes = minutes;
        Seconds = seconds;
    }

    public DateTime ToDateTime() => new DateTime(Year, Month, Date, Hours, Minutes, Seconds);
    
    public DateTime ToUniversalDate() => new DateTime(Year, Month, Date).ToUniversalTime();

    public DateTime ToUniversalTime() => new DateTime(Year, Month, Date, Hours, Minutes, Seconds).ToUniversalTime();

    public long ToUnixTimeMilliseconds() => 
        new DateTimeOffset(new DateTime(Year, Month, Date, Hours, Minutes, Seconds)).ToUnixTimeMilliseconds();
}