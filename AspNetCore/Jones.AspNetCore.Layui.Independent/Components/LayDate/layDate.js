window.layuiX.layDate = {
    version: "1.0.0",
    sdkVersion: function () {
        return laydate.v;
    },
    render: function(sender, option, dotNetObject){
        // console.log(option);
        let layDateOption = {
          elem: sender  
        };
        if (option.type) {
            layDateOption.type = option.type;
        }
        if (option.range) {
            layDateOption.range = option.range;
        }
        if (option.format) {
            layDateOption.format = option.format;
        }
        if (option.valueTimeStamp) {
            layDateOption.value = new Date(option.valueTimeStamp);
        } else if (option.value) {
            layDateOption.value = option.value;
        }
        if (option.isInitValue) {
            layDateOption.isInitValue = option.isInitValue;
        }
        if (option.isPreview) {
            layDateOption.isPreview = option.isPreview;
        }
        if (option.min) {
            layDateOption.min = option.min;
        }
        if (option.max) {
            layDateOption.max = option.max;
        }
        if (option.trigger) {
            layDateOption.trigger = option.trigger;
        }
        if (option.show) {
            layDateOption.show = option.show;
        }
        if (option.position) {
            layDateOption.position = option.position;
        }
        if (option.zIndex) {
            layDateOption.zIndex = option.zIndex;
        }
        if (option.showBottom != null) {
            layDateOption.showBottom = option.showBottom;
        }
        if (option.btns) {
            layDateOption.btns = option.btns;
        }
        if (option.lang) {
            layDateOption.lang = option.lang;
        }
        if (option.theme) {
            layDateOption.theme = option.theme;
        }
        if (option.calendar) {
            layDateOption.calendar = option.calendar;
        }
        if (option.mark) {
            layDateOption.mark = option.mark;
        }
        
        if (option.ready) {
            layDateOption.ready = function (dateTime) {
                dotNetObject.invokeMethodAsync(option.ready, dateTime);
            };
        }
        if (option.change) {
            layDateOption.change = function (value, dateTime, endDateTime) {
                dotNetObject.invokeMethodAsync(option.change, value, dateTime, endDateTime);
            };
        }
        if (option.done) {
            layDateOption.done = function (value, dateTime, endDateTime) {
                if (value) {
                    dotNetObject.invokeMethodAsync(option.done, value, dateTime, endDateTime);
                } else {
                    dotNetObject.invokeMethodAsync(option.done, null, null, null);
                }
            };
        }

        $(sender).bind("input propertychange",function(event){
            const value = $(sender).val();
            if(!value) {
                if (option.change) {
                    dotNetObject.invokeMethodAsync(option.change, null, null, null);
                }
                if (option.done) {
                    dotNetObject.invokeMethodAsync(option.done, null, null, null);
                }
            }
        });
        
        // console.log(layDateOption);
        return laydate.render(layDateOption);
    },
    hint: (layDateSender, value) => {
        layDateSender.hint(value);
    }
};