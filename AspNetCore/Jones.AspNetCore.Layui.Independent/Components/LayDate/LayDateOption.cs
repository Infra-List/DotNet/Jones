using System.Collections.Generic;
using Jones.AspNetCore.Layui.Independent.Types;

namespace Jones.AspNetCore.Layui.Independent.Components.LayDate;

/// <summary>
/// LayDate 配置项
///  https://www.layui.com/doc/modules/laydate.html
/// </summary>
public record LayDateOption
{
    /// <summary>
    /// 控件选择类型，用于单独提供不同的选择器类型
    /// 类型：String，默认值：date
    /// <see cref="LayDateType"/>
    /// </summary>
    public string? Type { get; set; }
        
    /// <summary>
    /// 开启左右面板范围选择
    /// 类型：Boolean/String/Array，默认值：false
    /// 如果设置 true，将默认采用 “ - ” 分割。 你也可以直接设置 分割字符。五种选择器类型均支持左右面板的范围选择。
    /// https://www.layui.com/doc/modules/laydate.html#range
    /// </summary>
    public dynamic? Range { get; set; }
        
    /// <summary>
    /// 自定义格式
    /// 类型：String，默认值：yyyy-MM-dd
    /// 通过日期时间各自的格式符和长度，来设定一个你所需要的日期格式。如：yyyy-MM-dd HH:mm:ss
    /// https://www.layui.com/doc/modules/laydate.html#format
    /// </summary>
    public string? Format { get; set; }
        
    /// <summary>
    /// 初始值
    /// 类型：String，默认值：new Date()
    /// 支持传入符合format参数设定的日期格式字符，或者 new Date()
    /// </summary>
    public string? Value { get; set; }
    public long? ValueTimeStamp { get; set; }
        
    /// <summary>
    /// 初始值填充
    /// 类型：Boolean，默认值：true
    /// 用于控制是否自动向元素填充初始值（需配合 value 参数使用）
    /// </summary>
    public bool? IsInitValue { get; set; }
        
    /// <summary>
    /// 是否开启选择值预览
    /// 类型：Boolean，默认值：true
    /// 用于控制是否显示当前结果的预览（type 为 datetime 时不开启）
    /// </summary>
    public bool? IsPreview { get; set; }
        
    /// <summary>
    /// 最小/大范围内的日期时间值
    /// 类型：string，默认值：min: '1900-1-1'、max: '2099-12-31'
    /// 设定有限范围内的日期或时间值，不在范围内的将不可选中
    /// https://www.layui.com/doc/modules/laydate.html#minmax
    /// </summary>
    public dynamic? Min { get; set; }
    public dynamic? Max { get; set; }
        
    /// <summary>
    /// 自定义弹出控件的事件
    /// 类型：String，默认值：focus，如果绑定的元素非输入框，则默认事件为：click
    /// <see cref="LayDateTrigger"/>
    /// </summary>
    public string? Trigger { get; set; }
        
    /// <summary>
    /// 默认显示
    /// 类型：Boolean，默认值：false
    /// 如果设置: true，则控件默认显示在绑定元素的区域。通常用于外部事件调用控件
    /// </summary>
    public bool? Show { get; set; }
        
    /// <summary>
    /// 定位方式
    /// 类型：String，默认值：absolute
    /// <see cref="LayDatePosition"/>
    /// https://www.layui.com/doc/modules/laydate.html#position
    /// </summary>
    public string? Position { get; set; }
        
    /// <summary>
    /// 层叠顺序
    /// 类型：Number，默认值：66666666
    /// </summary>
    public int? ZIndex { get; set; }
        
    /// <summary>
    /// 是否显示底部栏
    /// 类型：Boolean，默认值：true
    /// 如果设置 false，将不会显示控件的底部栏区域
    /// </summary>
    public bool? ShowBottom { get; set; }
        
    /// <summary>
    /// 工具按钮
    /// 类型：Array，默认值：['clear', 'now', 'confirm']
    /// 右下角显示的按钮，会按照数组顺序排列，内置可识别的值有：clear、now、confirm
    /// <see cref="LayDateButton"/>
    /// </summary>
    public string[]? Btns { get; set; }
        
    /// <summary>
    /// 语言
    /// 类型：String，默认值：cn
    /// 我们内置了两种语言版本：cn（中文版）、en（国际版，即英文版）。这里并没有开放自定义文字，是为了避免繁琐的配置。
    /// <see cref="LayDateLang"/>
    /// </summary>
    public string? Lang { get; set; }
        
    /// <summary>
    /// 主题
    /// default（默认简约）、molv（墨绿背景）、#颜色值（自定义颜色背景）、grid（格子主题）
    /// </summary>
    public string? Theme { get; set; }
        
    /// <summary>
    /// 是否显示公历节日
    /// 类型：Boolean，默认值：false
    /// </summary>
    public bool? Calendar { get; set; }
        
    /// <summary>
    /// 标注重要日子
    /// 类型：Object，默认值：无
    /// </summary>
    public Dictionary<string, string>? Mark { get; set; }
        
        
        
    /// <summary>
    /// 控件初始打开的回调
    /// 控件在打开时触发，回调返回一个参数：初始的日期时间对象
    /// </summary>
    public string? Ready { get; set; }
        
    /// <summary>
    /// 日期时间被切换后的回调
    /// 年月日时间被切换时都会触发。回调返回三个参数，分别代表：生成的值、日期时间对象、结束的日期时间对象
    /// </summary>
    public string? Change { get; set; }
        
    /// <summary>
    /// 控件选择完毕后的回调
    /// 点击日期、清空、现在、确定均会触发。回调返回三个参数，分别代表：生成的值、日期时间对象、结束的日期时间对象
    /// </summary>
    public string? Done { get; set; }
}