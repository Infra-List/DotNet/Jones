using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jones.AspNetCore.Layui.Independent.Components.Layer;
using Jones.AspNetCore.Services;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Layui.Independent.Services;

public class DialogServiceImpl : IDialogService
{
    private readonly IJSRuntime _jsRuntime;

    private TaskCompletionSource<DialogResult?>? _returnTask;
    private Func<DialogResult?>? _onConfirm;
    private Func<DialogResult?>? _onCancel;

    public DialogServiceImpl(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }

    public async Task SystemAlert(string content)
    {
        await _jsRuntime.InvokeVoidAsync("alert", content);
    }

    public async Task<bool> SystemConfirm(string content)
    {
        var confirmed = await _jsRuntime.InvokeAsync<bool>("confirm", content);
        return confirmed;
    }

    public async Task<string> SystemPrompt(string content)
    {
        var inputContent = await _jsRuntime.InvokeAsync<string>("prompt", content);
        return inputContent;
    }

    public async Task<DialogResult?> Confirm(string content, string? title = null, string? confirmText = null, Func<DialogResult?>? onConfirm = null,
        string? cancelText = null, Func<DialogResult?>? onCancel = null)
    {
        _returnTask = new TaskCompletionSource<DialogResult?>();
        _onConfirm = onConfirm;
        _onCancel = onCancel;

        var btn = new List<string>();
        if (onConfirm != null)
        {
            btn.Add(confirmText ?? "确定");
        }
        if (onCancel != null)
        {
            btn.Add(cancelText ?? "取消");
        }
            
        var options = new LayerOption
        {
            Title = title,
            Btn = btn.ToArray(),
            Yes = onConfirm == null ? null : nameof(OnConfirm),
            Cancel = onCancel == null ? null : nameof(OnCancel),
        };
        await _jsRuntime.InvokeVoidAsync("layuiX.layer.confirm", content, options, DotNetObjectReference.Create(this));
            
        return await _returnTask.Task;
    }

    public Task<DialogResult?> SimpleConfirm(string content, string? title = null, string? confirmText = null, string? cancelText = null, bool isHasCancel = true) =>
        Confirm(content, title, confirmText, () => DialogResult.Confirm, cancelText,  isHasCancel ? () => DialogResult.Cancel : null);
        
    [JSInvokable] 
    public Task OnConfirm()
    {
        _returnTask?.SetResult(_onConfirm?.Invoke());
        return Task.CompletedTask;
    }
            
    [JSInvokable]
    public Task OnCancel()
    {
        _returnTask?.SetResult(_onCancel?.Invoke());
        return Task.CompletedTask;
    }
}