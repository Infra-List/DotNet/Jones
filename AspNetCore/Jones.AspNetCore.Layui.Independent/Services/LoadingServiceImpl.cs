using System.Threading.Tasks;
using Jones.AspNetCore.Layui.Independent.Components.Layer;
using Jones.AspNetCore.Services;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Layui.Independent.Services;

public class LoadingServiceImpl : ILoadingService
{
    private readonly IJSRuntime _jsRuntime;

    public LoadingServiceImpl(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }
    
    public async Task<IJSObjectReference> ShowLoadingModal(bool isProgress = false)
    {
        var options = new LayerOption
        {
            Shade = new object[] { 0.2, "#000" }
        };
        return await _jsRuntime.InvokeAsync<IJSObjectReference>("layuiX.layer.load", (int)LayerLoadIcon.General, options);
    }

    public Task SetLoadingModalProgress(IJSObjectReference loadingModal, float percent, string? message)
    {
        return Task.CompletedTask;
    }

    public async Task CloseLoadingModal(IJSObjectReference loadingModal)
    {
        await _jsRuntime.InvokeVoidAsync("layuiX.layer.close", loadingModal);
    }
}