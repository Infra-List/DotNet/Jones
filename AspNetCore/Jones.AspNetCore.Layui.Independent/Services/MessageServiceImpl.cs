using System.Threading.Tasks;
using Jones.AspNetCore.Layui.Independent.Components.Layer;
using Jones.AspNetCore.Services;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Layui.Independent.Services;

public class MessageServiceImpl : IMessageService
{
    private readonly IJSRuntime _jsRuntime;

    public MessageServiceImpl(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }

    public Task Success(string content, double? duration = null) =>
        DoShow(content, duration, 6).AsTask();

    public Task Error(string content, double? duration = null) =>
        DoShow(content, duration, 5).AsTask();

    public Task Info(string content, double? duration = null) =>
        DoShow(content, duration).AsTask();

    public Task Warning(string content, double? duration = null) =>
        DoShow(content, duration, 3).AsTask();

    private ValueTask DoShow(string content, double? time = null, int? icon = null)
    {
        var option = new LayerMsgOption
        {
            Time = time,
            Icon = icon
        };
        return _jsRuntime.InvokeVoidAsync("layuiX.layer.msg", content, option);
    }
}