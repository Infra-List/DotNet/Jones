using System.Threading.Tasks;
using Jones.AspNetCore.Layui.Independent.Components.Layer;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Layui.Independent.Services;

public enum LayerLoadIcon
{
    General = 0,
    First = 1,
    Second = 2,
}

public interface ILayerService
{
    Task<TValue> Confirm<TValue, TDotNetObjectReference>(string content, LayerOption? options, DotNetObjectReference<TDotNetObjectReference> dotNetObjectReference) where TDotNetObjectReference : class;
    Task<TValue> Load<TValue>(LayerLoadIcon icon = LayerLoadIcon.General, LayerOption? options = null);
    Task Close<TValue>(TValue layer);
}

public class LayerServiceImpl : ILayerService
{
    private readonly IJSRuntime _jsRuntime;

    public LayerServiceImpl(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }

    public async Task<TValue> Confirm<TValue, TDotNetObjectReference>(string content, LayerOption? options, DotNetObjectReference<TDotNetObjectReference> dotNetObjectReference) where TDotNetObjectReference : class
    {
        return await _jsRuntime.InvokeAsync<TValue>("layuiX.layer.confirm", content, options, dotNetObjectReference);
    }

    public async Task<TValue> Load<TValue>(LayerLoadIcon icon = LayerLoadIcon.General, LayerOption? options = null)
    {
        options ??= new LayerOption();
        if (options.Shade == null)
        {
            options.Shade = new object[] { 0.2, "#000" };
        }
        return await _jsRuntime.InvokeAsync<TValue>("layuiX.layer.load", (int)icon, options);
    }

    public async Task Close<TValue>(TValue layer)
    {
        await _jsRuntime.InvokeVoidAsync("layuiX.layer.close", layer);
    }
}