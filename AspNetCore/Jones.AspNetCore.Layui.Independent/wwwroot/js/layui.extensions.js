window.layuiX = {
    version: "1.0.0"
};
window.layuiX.layDate = {
    version: "1.0.0",
    sdkVersion: function () {
        return laydate.v;
    },
    render: function(sender, option, dotNetObject){
        // console.log(option);
        let layDateOption = {
          elem: sender  
        };
        if (option.type) {
            layDateOption.type = option.type;
        }
        if (option.range) {
            layDateOption.range = option.range;
        }
        if (option.format) {
            layDateOption.format = option.format;
        }
        if (option.valueTimeStamp) {
            layDateOption.value = new Date(option.valueTimeStamp);
        } else if (option.value) {
            layDateOption.value = option.value;
        }
        if (option.isInitValue) {
            layDateOption.isInitValue = option.isInitValue;
        }
        if (option.isPreview) {
            layDateOption.isPreview = option.isPreview;
        }
        if (option.min) {
            layDateOption.min = option.min;
        }
        if (option.max) {
            layDateOption.max = option.max;
        }
        if (option.trigger) {
            layDateOption.trigger = option.trigger;
        }
        if (option.show) {
            layDateOption.show = option.show;
        }
        if (option.position) {
            layDateOption.position = option.position;
        }
        if (option.zIndex) {
            layDateOption.zIndex = option.zIndex;
        }
        if (option.showBottom != null) {
            layDateOption.showBottom = option.showBottom;
        }
        if (option.btns) {
            layDateOption.btns = option.btns;
        }
        if (option.lang) {
            layDateOption.lang = option.lang;
        }
        if (option.theme) {
            layDateOption.theme = option.theme;
        }
        if (option.calendar) {
            layDateOption.calendar = option.calendar;
        }
        if (option.mark) {
            layDateOption.mark = option.mark;
        }
        
        if (option.ready) {
            layDateOption.ready = function (dateTime) {
                dotNetObject.invokeMethodAsync(option.ready, dateTime);
            };
        }
        if (option.change) {
            layDateOption.change = function (value, dateTime, endDateTime) {
                dotNetObject.invokeMethodAsync(option.change, value, dateTime, endDateTime);
            };
        }
        if (option.done) {
            layDateOption.done = function (value, dateTime, endDateTime) {
                if (value) {
                    dotNetObject.invokeMethodAsync(option.done, value, dateTime, endDateTime);
                } else {
                    dotNetObject.invokeMethodAsync(option.done, null, null, null);
                }
            };
        }

        $(sender).bind("input propertychange",function(event){
            const value = $(sender).val();
            if(!value) {
                if (option.change) {
                    dotNetObject.invokeMethodAsync(option.change, null, null, null);
                }
                if (option.done) {
                    dotNetObject.invokeMethodAsync(option.done, null, null, null);
                }
            }
        });
        
        // console.log(layDateOption);
        return laydate.render(layDateOption);
    },
    hint: (layDateSender, value) => {
        layDateSender.hint(value);
    }
};
function getLayerOption(option, dotNetObject) {
    let layerOption = {};
    if (option.type) {
        layerOption.type = option.type;
    }
    if (option.title) {
        layerOption.title = option.title;
    }
    if (option.skin) {
        layerOption.skin = option.skin;
    }
    if (option.area) {
        layerOption.area = option.area;
    }
    if (option.offset) {
        layerOption.offset = option.offset;
    }
    if (option.btn) {
        layerOption.btn = option.btn;
    }
    if (option.closeBtn) {
        layerOption.closeBtn = option.closeBtn;
    }
    if (option.shade) {
        layerOption.shade = option.shade;
    }
    if (option.shadeClose) {
        layerOption.shadeClose = option.shadeClose;
    }
    if (option.time) {
        layerOption.time = option.time;
    }
    if (option.id) {
        layerOption.id = option.id;
    }
    if (option.anim) {
        layerOption.anim = option.anim;
    }
    if (option.isOutAnim) {
        layerOption.isOutAnim = option.isOutAnim;
    }
    if (option.maxmin) {
        layerOption.maxmin = option.maxmin;
    }
    if (option.fixed) {
        layerOption.fixed = option.fixed;
    }
    if (option.resize) {
        layerOption.resize = option.resize;
    }
    if (option.scrollbar) {
        layerOption.scrollbar = option.scrollbar;
    }
    if (option.maxWidth) {
        layerOption.maxWidth = option.maxWidth;
    }
    if (option.maxHeight) {
        layerOption.maxHeight = option.maxHeight;
    }
    if (option.zIndex) {
        layerOption.zIndex = option.zIndex;
    }
    if (option.move) {
        layerOption.move = option.move;
    }
    if (option.moveOut) {
        layerOption.moveOut = option.moveOut;
    }
    if (option.tips) {
        layerOption.tips = option.tips;
    }
    if (option.tipsMore) {
        layerOption.tipsMore = option.tipsMore;
    }
    if (option.yes) {
        layerOption.yes = function (index, layer) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.yes, index, layer);
        };
    }
    if (option.cancel) {
        layerOption.cancel = function (index, layer) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.cancel, index, layer);
        };
    }
    if (option.full) {
        layerOption.full = function (layer, index) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.full, index, layer);
        };
    }
    if (option.min) {
        layerOption.min = function (layer, index) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.min, index, layer);
        };
    }
    if (option.restore) {
        layerOption.restore = function (layer, index) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.restore, index, layer);
        };
    }
    layerOption.success = function (layer, index) {
        documentKeydownListening();
        if (option.success) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.success, index, layer);
        }
    };
    layerOption.end = function () {
        documentKeydownUnListening();
        if (option.end) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.end);
        }
    };
    return layerOption;
}

function documentKeydownListening() {
    document.addEventListener('keydown', documentKeydown, true);
}

function documentKeydownUnListening() {
    document.removeEventListener('keydown', documentKeydown, true);
}

function documentKeydown(event) {
    if (event.defaultPrevented) {
        return; // Should do nothing if the default action has been cancelled
    }

    let handled = false;
    if (event.key !== undefined) {
        if (event.key === "Enter") {
            document.getElementsByClassName("layui-layer-btn0")[0].click();
            handled = true;

        }else if(event.key === "Esc" || event.key === "Escape"){
            document.getElementsByClassName("layui-layer-btn1")[0].click();
            handled = true;
        }
    } else if (event.keyCode !== undefined) {
        if (event.keyCode === 13) {
            document.getElementsByClassName("layui-layer-btn0")[0].click();
            handled = true;

        }else if(event.keyCode == 27){
            document.getElementsByClassName("layui-layer-btn1")[0].click();
            handled = true;
        }
    }

    if (handled) {
        // Suppress "double action" if event handled
        event.preventDefault();
    }
}

function getLayerConfirmOption(option, dotNetObject) {
    let layerOption = {};
    if (option.title) {
        layerOption.title = option.title;
    }
    if (option.btn) {
        layerOption.btn = option.btn;
    }
    if (option.maxWidth) {
        layerOption.maxWidth = option.maxWidth;
    }
    if (option.maxHeight) {
        layerOption.maxHeight = option.maxHeight;
    }
    if (option.zIndex) {
        layerOption.zIndex = option.zIndex;
    }
    if (option.move) {
        layerOption.move = option.move;
    }
    if (option.moveOut) {
        layerOption.moveOut = option.moveOut;
    }
    layerOption.success = function (layer, index) {
        documentKeydownListening();
        if (option.success) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.success, index, layer);
        }
    };
    layerOption.end = function () {
        documentKeydownUnListening();
        if (option.end) {
            dotNetObject && dotNetObject.invokeMethodAsync(option.end);
        }
    };
    return layerOption;
}

window.layuiX.layer = {
    version: "1.0.0",
    sdkVersion: function () {
        return layer.v;
    },
    msg: function (content, option) {
        let layerMsgOption = {};
        if (option.time) {
            layerMsgOption.time = option.time;
        }
        if (option.icon) {
            layerMsgOption.icon = option.icon;
        }
        let index =  layer.msg(content, layerMsgOption);
        return { index: index };
    },
    open: function(content, option, dotNetObject){
        // console.log(content);
        let layerOption = getLayerOption(option, dotNetObject);
        layerOption.content = $(content);
        let index = layer.open(layerOption);
        return { index: index };
    },
    load: function (icon, options, dotNetObject) {
        let layerOption = options ? getLayerOption(options, dotNetObject) : {};
        let index = layer.load(icon, layerOption);
        return { index: index };
    },
    close: function (target) {
        layer.close(target.index);
    },
    confirm: function (content, option, dotNetObject) {
        let layerOption = getLayerConfirmOption(option, dotNetObject);
        let index = layer.confirm(content, layerOption, 
            function (index) {
                layer.close(index);
                if (option.yes) {
                    dotNetObject && dotNetObject.invokeMethodAsync(option.yes);
                }
            },
            function (index) {
                if (option.cancel) {
                    dotNetObject && dotNetObject.invokeMethodAsync(option.cancel);
                }
            });
        return { index: index };
    }
};