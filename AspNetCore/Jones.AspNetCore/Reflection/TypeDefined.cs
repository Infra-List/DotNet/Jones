﻿using System;

namespace Jones.AspNetCore.Reflection;

public static class TypeDefined<T>
{
    public static bool IsNullable { get; }

    public static bool IsGenericType { get; }

    public static Type NullableType { get; }

    static TypeDefined()
    {
        IsNullable = IsNullableType(typeof(T));
        NullableType = GetNullableGenericType(typeof(T));
        IsGenericType = typeof(T).IsGenericType;
    }

    public static bool IsNullableType(Type type)
    {
        return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
    }

    public static Type GetNullableGenericType(Type type)
    {
        return IsNullableType(type) ? type.GetGenericArguments()[0] : null;
    }
}