using System.Threading;

namespace Jones.AspNetCore.Helper;

public static class IdHelper
{
    private static int _id;
    public static int GetId()
    {
        Interlocked.Increment(ref _id);
        return _id;
    }
}