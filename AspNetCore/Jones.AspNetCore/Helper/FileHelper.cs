using System;
using System.IO;

namespace Jones.AspNetCore.Helper;

public static class FileHelper
{
    public static bool IsImage(string name, string? contentType) => 
        contentType?.Contains("image", StringComparison.OrdinalIgnoreCase) == true
        || Path.GetExtension(name).ToLowerInvariant() switch
        {
            ".jpg" or ".jpeg" or ".png" or ".bmp" or ".gif" => true,
            _ => false
        };

    public static bool IsGif(string name) => Path.GetExtension(name).ToLowerInvariant() == ".gif";
}