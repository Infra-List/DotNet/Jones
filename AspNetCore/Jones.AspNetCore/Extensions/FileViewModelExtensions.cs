using System;
using System.IO;
using Jones.AspNetCore.Helper;
using Jones.AspNetCore.Models;
using Jones.AspNetCore.Utils;

namespace Jones.AspNetCore.Extensions;

public static class FileViewModelExtensions
{
    public static bool IsImage(this FileViewModel file) => FileHelper.IsImage(file.Name, file.ContentType);
        
    public static string GetImageSrc(this FileViewModel file)
    {
        return "data:" + file.ContentType + ";base64," + Convert.ToBase64String(file.Content);
    }
        
    public static string? PreviewFromFontAwesome(this FileViewModel file)
    {
        var builder = CssBuilder.Default("fa");
        var fileExtension = Path.GetExtension(file.Name).ToLowerInvariant();
        var icon = fileExtension switch
        {
            ".csv" or ".xls" or ".xlsx" => "fa-file-excel-o",
            ".doc" or ".docx" or ".dot" or ".dotx" => "fa-file-word-o",
            ".ppt" or ".pptx" => "fa-file-powerpoint-o",
            ".wav" or ".mp3" => "fa-file-audio-o",
            ".mp4" or ".mov" or ".mkv" => "fa-file-video-o",
            ".cs" or ".html" or ".vb" => "fa-file-code-o",
            ".pdf" => "fa-file-pdf-o",
            ".zip" or ".rar" or ".iso" => "fa-file-archive-o",
            ".txt" or ".log" or ".iso" => "fa-file-text-o",
            ".jpg" or ".jpeg" or ".png" or ".bmp" or ".gif" => "fa-file-image-o",
            _ => "fa-file-o"
        };
        builder.AddClass(icon);
        builder.AddClass("fa-2x");
        return builder.Build();
    }
}