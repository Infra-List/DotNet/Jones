using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Extensions;

// https://khalidabuhakmeh.com/blazor-javascript-isolation-modules-and-dynamic-csharp
public static class JsRuntimeExtensions
{
    public static Task<IJSObjectReference> Import<T>(this IJSRuntime jsRuntime, string pathFromWwwRoot) =>
        jsRuntime.Import(typeof(T), pathFromWwwRoot);
    
    public static async Task<IJSObjectReference> Import(this IJSRuntime jsRuntime, Type type, string pathFromWwwRoot)
    {
        var libraryName = type.Assembly.GetName().Name;
        return await jsRuntime.InvokeAsync<IJSObjectReference>(
            "import", 
            Path.Combine($"./_content/{libraryName}/", pathFromWwwRoot)
        );
    }
}