using System;
using System.Threading.Tasks;

namespace Jones.AspNetCore.Services;

public interface ILocalStorageService
{
    ValueTask<bool> Set<T>(string key, T value, Func<T, string>? serializer = null);
    ValueTask<T?> Get<T>(string key, T? defaultValue = default, Func<string, T>? serializer = null);
    ValueTask<bool> Contains(string key);
    ValueTask<bool> Remove(string key);
    ValueTask<bool> Clear();
}