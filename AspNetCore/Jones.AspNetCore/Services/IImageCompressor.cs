using System.Threading.Tasks;

namespace Jones.AspNetCore.Services;

public interface IImageCompressor
{
    Task<ImageCompressorResult> Compress(byte[] image, string contentType, ImageCompressorOption option);
}

public record ImageCompressorOption(float? Quality = null, float? MaxWidth = null, float? MaxHeight = null, 
    float? MinWidth = null, float? MinHeight = null, float? Width = null, float? Height = null);

public record ImageCompressorResult(bool IsSuccess, ImageAfterCompression? Image = null, string? Message = null);

public record ImageAfterCompression(byte[] Content, string ContentType, long Size);