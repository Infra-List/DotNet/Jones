using System.Threading.Tasks;

namespace Jones.AspNetCore.Services;

public interface IMessageService
{
    Task Success(string content, double? duration = null);
    Task Error(string content, double? duration = null);
    Task Info(string content, double? duration = null);
    Task Warning(string content, double? duration = null);
}