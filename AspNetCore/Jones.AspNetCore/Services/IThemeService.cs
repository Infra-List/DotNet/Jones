using System.Threading.Tasks;
using Jones.AspNetCore.Types;

namespace Jones.AspNetCore.Services;

public delegate Task NotifyDarkModeStateChange(bool state);

public interface IThemeService
{
    Task Initialize();
    
    ThemeMode ThemeMode { get; }
    void SetThemeMode(ThemeMode themeMode);
    
    Task<bool> IsPrefersColorSchemeDarkAsync(); 
    
    event NotifyDarkModeStateChange OnDarkModeStateChanged;
}