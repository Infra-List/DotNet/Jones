using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Services;

public interface ILoadingService
{
    Task<IJSObjectReference> ShowLoadingModal(bool isProgress = false);
    Task SetLoadingModalProgress(IJSObjectReference loadingModal, float percent, string? message);
    Task CloseLoadingModal(IJSObjectReference loadingModal);
}