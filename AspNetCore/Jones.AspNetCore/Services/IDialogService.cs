using System;
using System.Threading.Tasks;

namespace Jones.AspNetCore.Services;

public interface IDialogService
{
    Task SystemAlert(string content);
    Task<bool> SystemConfirm(string content);
    Task<string> SystemPrompt(string content);
    Task<DialogResult?> Confirm(string content, string? title = null, string? confirmText = null, Func<DialogResult?>? onConfirm = null, string? cancelText = null, Func<DialogResult?>? onCancel = null);
    Task<DialogResult?> SimpleConfirm(string content, string? title = null, string? confirmText = null, string? cancelText = null, bool isHasCancel = true);
}
    
public enum DialogResult {
    Cancel, Confirm
}