namespace Jones.AspNetCore.Types;

public enum ThemeMode
{
    System,
    Light,
    Dark,
}