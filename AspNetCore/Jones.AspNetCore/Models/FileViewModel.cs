namespace Jones.AspNetCore.Models;

public record FileViewModel
{
    public string Name { get; set; }
        
    public long Size { get; set; }
        
    public string ContentType { get; set; }
        
    public byte[] Content { get; set; }
}