namespace Jones.AspNetCore.Models;

public record PaginationViewModel
{
    public int Page { get; set; }
    public int PageSize { get; set; }

    public PaginationViewModel(int page, int pageSize)
    {
        Page = page;
        PageSize = pageSize;
    }

    public static PaginationViewModel Create(int page, int pageSize) => new(page, pageSize);

    public static PaginationViewModel Create(Paging paging) => Create(paging.Page, paging.PageSize);
}