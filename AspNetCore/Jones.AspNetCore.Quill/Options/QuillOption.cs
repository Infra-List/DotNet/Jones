namespace Jones.AspNetCore.Quill.Options;

public record QuillOption
{
    /// <summary>
    /// Quill.debug(level: String | Boolean)
    /// 启用记录信息的静态方法，给定的级别有：'error', 'warn', 'log'或'info'。传入true等同于传入'log'。传入false表示停用所有级别的消息记录。
    /// 默认值： warn
    /// </summary>
    public dynamic? Debug { get; }
        
    /// <summary>
    /// 编辑器允许的格式列表白名单。完整的列表，请见
    /// https://quilljs.com/docs/formats/
    /// 默认值： All formats
    /// </summary>
    public string[]? Formats { get; }
        
    /// <summary>
    /// 当编辑器为空时显示的占位符文字。
    /// 默认值： None
    /// </summary>
    public string? Placeholder { get; }
        
    /// <summary>
    /// 是否将编辑器实例设置为只读模式。
    /// 默认值： false
    /// </summary>
    public bool? ReadOnly { get; }

    /// <summary>
    /// 使用主题的名称。这个内置的选项是"bubble"或"snow"。一个无效或错误的值将加载默认的最小化主题。注意，主题的样式表任需要手动包含。
    /// https://quilljs.com/docs/themes/
    /// </summary>
    public string? Theme { get; } = "snow";
        
    public string[]? FontSizes { get; }

    public bool? IsImageEnable { get; }
        
    // https://github.com/kensnyder/quill-image-drop-module
    public bool? IsUseImageDropModule { get; }
        
    // https://github.com/mudoo/quill-resize-module/
    public QuillResizeModuleOption? ResizeModuleOption { get; }
        
    // 图片处理
    public string? OnFileHandler { get; }
        
    public string? OnTextChange { get; set; }
        
    // https://github.com/NoelOConnell/quill-image-uploader
    // https://stackoverflow.com/questions/60976208/how-do-i-upload-files-with-blazor

    public QuillOption(dynamic? debug, string[]? formats, string? placeholder, bool? readOnly, string? theme, string[]? fontSizes, 
        bool? isImageEnable, bool? isUseImageDropModule, QuillResizeModuleOption? resizeModuleOption, string? onFileHandler, string? onTextChange)
    {
        Debug = debug;
        Formats = formats;
        Placeholder = placeholder;
        ReadOnly = readOnly;
        Theme = theme;
        FontSizes = fontSizes;
        IsImageEnable = isImageEnable;
        IsUseImageDropModule = isUseImageDropModule;
        ResizeModuleOption = resizeModuleOption;
        OnFileHandler = onFileHandler;
        OnTextChange = onTextChange;
    }
}