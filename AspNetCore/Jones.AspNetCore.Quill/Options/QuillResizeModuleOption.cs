namespace Jones.AspNetCore.Quill.Options;

// https://github.com/mudoo/quill-resize-module/
public record QuillResizeModuleOption;