function getQuillOption(option) {
    let quillOption = {};

    if (option.debug) {
        quillOption.debug = option.debug;
    }

    if (option.formats) {
        quillOption.formats = option.formats;
    }

    if (option.placeholder) {
        quillOption.placeholder = option.placeholder;
    }

    if (option.readOnly) {
        quillOption.readOnly = option.readOnly;
    }

    if (option.theme) {
        quillOption.theme = option.theme;
    }

    return quillOption;
}

window.quillX = {
    version: "1.0.0",
    init: function (quillElement, toolbar, option, dotNetObject) {

        if (option.fontSizes) {
            let Size = Quill.import('attributors/style/size');
            Size.whitelist = option.fontSizes;
            Quill.register(Size, true);
        }

        // 设置option
        var quillOption = getQuillOption(option);
        quillOption.modules = {};
        if (toolbar) {
            quillOption.modules.toolbar = toolbar;
        }

        if (option.onFileHandler) {
            Quill.register("modules/imageUploader", ImageUploader);
            quillOption.modules.imageUploader = {
                upload: function (file) {
                    return new Promise(async (resolve, reject) => {
                        if (!file.type.match(/^image\/(gif|jpe?g|a?png|svg|webp|bmp|vnd\.microsoft\.icon)/i)) {
                            reject("file is not an image");
                            return;
                        }

                        dotNetObject.invokeMethodAsync(option.onFileHandler, {
                            Name: file.name,
                            Size: file.size,
                            ContentType: file.type,
                            Content: new Uint8Array(await readFile(file))
                        })
                            .then(result => {
                                resolve(result);
                            })
                            .catch(error => {
                                reject(error);
                                console.error("Error:", error);
                            });
                    });
                }
            };
        } else {
            if (option.isImageEnable && option.isImageEnable === true) {
                if (option.isUseImageDropModule) {
                    quillOption.modules.imageDrop = true;
                }
            }
        }

        if (option.resizeModuleOption) {
            // Quill.register("modules/resize", QuillResize);
            // quillOption.modules.resize = {
            //     modules: [ 'Resize', 'DisplaySize' ]
            // };

            // Quill.register('modules/imageResize', ImageResize);
            quillOption.modules.imageResize = {
                modules: [ 'Resize', 'DisplaySize' ]
            };
        }

        const quill = new Quill(quillElement, quillOption);

        if (option.onTextChange) {
            quill.on('text-change', function(delta, oldDelta, source) {
                dotNetObject.invokeMethodAsync(option.onTextChange, JSON.stringify(delta), JSON.stringify(oldDelta), source);
            });
        }

        return quill;
    },
    deleteText: function (quill, index, length) {
        quill.deleteText(index, length);
    },
    getContents: function (quill) {
        const isEmpty = quill.getLength() === 1;
        return isEmpty ? null : JSON.stringify(quill.getContents());
    },
    getLength: function (quill) {
        return quill.getLength();
    },
    getText: function (quill) {
        return quill.getText();
    },
    setContents: function (quill, content) {
        let delta = JSON.parse(content);
        let newDelta = quill.setContents(delta);
        return JSON.stringify(newDelta);
    },
    clearContents: function (quill) {
        quill.deleteText(0,quill.getLength());
    }
};

function readFile(file) {
    return new Promise((resolve, reject) => {
        // Create file reader
        let reader = new FileReader()
        // Register event listeners
        reader.addEventListener("loadend", e => resolve(e.target.result))
        reader.addEventListener("error", reject)
        // Read file
        reader.readAsArrayBuffer(file)
    })
}