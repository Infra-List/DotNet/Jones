using System.Threading.Tasks;

namespace Jones.AspNetCore.Quill;

public interface IQuillJsCallbacks
{
    Task TextChange(string delta, string oldDelta, string source);
}