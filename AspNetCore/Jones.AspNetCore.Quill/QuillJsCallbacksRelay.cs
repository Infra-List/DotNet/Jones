using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Quill;

public class QuillJsCallbacksRelay : IDisposable
{
    private readonly IQuillJsCallbacks _callbacks;

    public IDisposable DotNetReference { get; }

    public QuillJsCallbacksRelay(IQuillJsCallbacks callbacks)
    {
        _callbacks = callbacks;

        DotNetReference = DotNetObjectReference.Create(this);
    }

    [JSInvokable]
    public Task TextChange(string delta, string oldDelta, string source) => 
        _callbacks.TextChange(delta, oldDelta, source);

    public void Dispose()
    {
        DotNetReference.Dispose();
    }
}