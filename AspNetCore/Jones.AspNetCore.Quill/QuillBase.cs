using System;
using System.Threading.Tasks;
using Jones.AspNetCore.Components;
using Jones.AspNetCore.Models;
using Jones.AspNetCore.Quill.Options;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Quill;

// https://www.py4u.net/discuss/310435
public abstract class QuillBase : JonesComponentBase, IDisposable
{
    [Parameter] public RenderFragment? ToolbarContent { get; set; }
    [Parameter] public RenderFragment? EditorContent { get; set; }
        
    [Parameter] public dynamic? Debug { get; set; }
    [Parameter] public string[]? Formats { get; set; }
    [Parameter] public string? Placeholder { get; set; }
    [Parameter] public bool? ReadOnly { get; set; }
    [Parameter] public string? Theme { get; set; } = "snow";
        
    [Parameter] public string[]? FontSizes { get; set; }
    [Parameter] public bool? IsImageEnable { get; set; }
    [Parameter] public bool? IsUseImageDropModule { get; set; } = true;
    
    [Parameter] public long MaxFileSize { get; set; } = 10 * 1024 * 1024;
    [Parameter] public Func<FileViewModel, Task<(bool IsSuccess, string? Url, string? Message)>>? OnUploadFile { get; set; }
    
    [Parameter] public QuillResizeModuleOption? ResizeModuleOption { get; set; } = new();
    
    [Parameter] public Func<string?, string?, string?, Task>? TextChange { get; set; }
    
    [Parameter] public Action<IJSObjectReference?>? OnSetQuillObjectReference { get; set; }
        
    // [Parameter] public string? Value { get; set; }
    // [Parameter] public EventCallback<string?> ValueChanged { get; set; }

    protected ElementReference ToolBarElement;
    protected ElementReference QuillElement;
    
    [Inject]
    protected IJSRuntime JsRuntime { get; set; } = null!;
    private IJSObjectReference? _quillObjectReference;

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await Init();
        }
    }

    private async Task Init()
    {
        var option = new QuillOption(Debug, Formats, Placeholder, ReadOnly, Theme, FontSizes, 
            IsImageEnable, IsUseImageDropModule, ResizeModuleOption, OnUploadFile != null ? nameof(FileHandler) : null, 
            TextChange == null ? null : nameof(OnTextChange));
        SetQuillObjectReference(await JsRuntime.InvokeAsync<IJSObjectReference>("window.quillX.init",
            QuillElement, ToolBarElement, option, DotNetObjectReference.Create(this)));

    }

    private void SetQuillObjectReference(IJSObjectReference? quillObjectReference)
    {
        _quillObjectReference = quillObjectReference;
        OnSetQuillObjectReference?.Invoke(quillObjectReference);
    }
    
    [JSInvokable]
    public async Task<string> FileHandler(FileViewModel file)
    {
        if (OnUploadFile == null)
        {
            throw new Exception("没有配置上传方法");
        }
        var uploadResult = await OnUploadFile(file);
        if (!uploadResult.IsSuccess)
        {
            throw new Exception(uploadResult.Message);
        }

        return uploadResult.Url ?? throw new Exception($"上传文件后返回的地址为空");
    }

    [JSInvokable]
    public async Task OnTextChange(string? content, string? oldContent, string? source)
    {
        if (TextChange == null)
        {
            return;
        }

        await TextChange.Invoke(content, oldContent, source);
    }

    public async Task DeleteText(int index, int length)
    {
        if (_quillObjectReference == null)
        {
            return;
        }
        await JsRuntime.InvokeVoidAsync("window.quillX.deleteText", _quillObjectReference, index, length);
    }
        
    public async Task<string?> GetContents()
    {
        if (_quillObjectReference == null)
        {
            return null;
        }
        return await JsRuntime.InvokeAsync<string>("window.quillX.getContents", _quillObjectReference);
    }
        
    public async Task<long?> GetLength()
    {
        if (_quillObjectReference == null)
        {
            return null;
        }
        return await JsRuntime.InvokeAsync<long>("window.quillX.getLength", _quillObjectReference);
    }

    public async Task<string?> GetText()
    {
        if (_quillObjectReference == null)
        {
            return null;
        }
        return await JsRuntime.InvokeAsync<string>("window.quillX.getText", _quillObjectReference);
    }

    public async Task<string?> SetContents(string? content)
    {
        if (_quillObjectReference == null)
        {
            return null;
        }
        return await JsRuntime.InvokeAsync<string>("window.quillX.setContents", _quillObjectReference, content);
    }

    public async Task ClearContents()
    {
        if (_quillObjectReference == null)
        {
            return;
        }
        await JsRuntime.InvokeAsync<string>("window.quillX.clearContents", _quillObjectReference);
    }

    public void Dispose()
    {
        _quillObjectReference?.DisposeAsync();
        SetQuillObjectReference(null);
    }

    // // https://quilljs.com/docs/modules/toolbar/
    // private static readonly dynamic[] ToolbarOptions = {
    //     new []
    //     {
    //         "bold", "italic", "underline", "strike"
    //     },
    //     new []
    //     {
    //         new
    //         {
    //             align = ""
    //         },
    //         new
    //         {
    //             align = "center"
    //         },
    //         new
    //         {
    //             align = "right"
    //         },
    //     },
    //     new []
    //     {
    //         new
    //         {
    //             list = "ordered"
    //         },
    //         new
    //         {
    //             list = "bullet"
    //         }
    //     },
    //     new []
    //     {
    //         new
    //         {
    //             script = "sub"
    //         },
    //         new
    //         {
    //             script = "super"
    //         }
    //     },
    //     new []
    //     {
    //         new
    //         {
    //             // size = new [] { "Small", "", "Large", "Huge" }
    //             size = new [] { "14px", "16px", "18px", "20px" }
    //         }
    //     },
    //     new []
    //     {
    //         new
    //         {
    //             header = new [] { "1", "2", "3", "4", "5", "6", "" }
    //         }
    //     },
    //     new []
    //     {
    //         new Dictionary<string, dynamic[]>
    //         {
    //             {
    //                 "color", Array.Empty<dynamic>()
    //             }
    //         },
    //         new Dictionary<string, dynamic[]>
    //         {
    //             {
    //                 "background", Array.Empty<dynamic>()
    //             }
    //         }
    //     },
    //     new []
    //     {
    //         "link", "image"
    //     },
    //     // new []
    //     // {
    //     //     "blockquote", "clean"
    //     // },
    // };
}