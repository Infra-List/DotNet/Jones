namespace Jones.AspNetCore.TailwindCSS.FlowBite.Types;

public enum DrawerPlacement
{
    Start, End, Top, Bottom
}