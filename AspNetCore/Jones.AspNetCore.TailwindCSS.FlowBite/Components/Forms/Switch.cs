using Jones.AspNetCore.Utils;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.TailwindCSS.FlowBite.Components.Forms
{
    public class Switch : InputCheckbox
    {
        [Parameter] 
        public string? OnText { get; set; } = "是";
        
        [Parameter] 
        public string? OffText { get; set; } = "否";

        [Parameter] 
        public bool IsShowText { get; set; } = true;

// <div class="form-check form-switch">
//   <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
//   <label class="form-check-label" for="flexSwitchCheckDefault">Default switch checkbox input</label>
// </div>

// <label class="relative inline-flex items-center cursor-pointer">
    // <input type="checkbox" value="" class="sr-only peer">
    // <div class="w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
    // <span class="ml-3 text-sm font-medium text-gray-900 dark:text-gray-300">Toggle me</span>
// </label>
    
        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            builder.OpenElement(0, "label");
            builder.AddMultipleAttributes(1, AdditionalAttributes);
            builder.AddAttribute(2, "class", 
                CssBuilder.Default("relative inline-flex items-center cursor-pointer").AddClassFromAttributes(AdditionalAttributes).Build());
            
            builder.OpenElement(3, "input");
            builder.AddAttribute(5, "type", "checkbox");
            builder.AddAttribute(6, "class", "sr-only peer");
            builder.AddAttribute(7, "checked", BindConverter.FormatValue(CurrentValue));
            builder.AddAttribute(8, "onchange", EventCallback.Factory.CreateBinder<bool>(this, value => CurrentValue = value, CurrentValue));
            builder.CloseElement();
            
            builder.OpenElement(9, "div");
            builder.AddAttribute(10, "class", "w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600");
            builder.CloseElement();

            var currentValueText = CurrentValue ? OnText : OffText;
            if (IsShowText && currentValueText != null)
            {
                builder.OpenElement(11, "span");
                builder.AddAttribute(12, "class", "ms-3 text-gray-900 dark:text-gray-300");
                builder.AddContent(13, currentValueText);
                builder.CloseElement();
            }
            
            builder.CloseElement();
        }
    }
}