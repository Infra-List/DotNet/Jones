using Jones.AspNetCore.Helper;
using Jones.AspNetCore.Utils;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.TailwindCSS.FlowBite.Components.Forms;

public class Checkbox : InputCheckbox
{
    private string? _id;
    [Parameter]
    public string? Id 
    {
        get
        {
            _id ??=
                (ValueExpression == null 
                    ? null 
                    : $"{FieldIdentifier.Create(ValueExpression).FieldName}_{IdHelper.GetId()}");
            return _id;
        }
        set => _id = value;
    }
        
    [Parameter] 
    public string? Text { get; set; }

    [Parameter]
    public Action? OnSelectedChanged { get; set; }

    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (ValueExpression != null)
        {
            var fieldName = FieldIdentifier.Create(ValueExpression).FieldName;
            EditContext.OnFieldChanged += (_, args) =>
            {
                if (args.FieldIdentifier.FieldName == fieldName)
                {
                    SelectedItemChanged();
                }
            };
        }
    }
        
// <div class="form-check form-check-inline">
//   <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
//   <label class="form-check-label" for="inlineCheckbox1">1</label>
// </div>
        
    private string? Class => CssBuilder.Default("flex items-center")
        .AddClassFromAttributes(AdditionalAttributes)
        .Build();
        
    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(0, "div");
        builder.AddMultipleAttributes(1, AdditionalAttributes);
        builder.AddAttribute(2, "class", Class);
            
        builder.OpenElement(3, "input");
        builder.AddAttribute(4, "id", Id);
        builder.AddAttribute(5, "type", "checkbox");
        builder.AddAttribute(6, "class", "form-check-input");
        builder.AddAttribute(7, "checked", BindConverter.FormatValue(CurrentValue));
        builder.AddAttribute(8, "onchange", EventCallback.Factory.CreateBinder<bool>(this, value => CurrentValue = value, CurrentValue));
        builder.CloseElement();

        var text = Text ?? ValueExpression?.GetDisplayName();
        if (text != null)
        {
            builder.OpenElement(9, "label");
            builder.AddAttribute(10, "for", Id);
            builder.AddAttribute(11, "class", "ms-2 text-gray-900 dark:text-gray-300");
            builder.AddContent(12, text);
            builder.CloseElement();
        }
            
        builder.CloseElement();
    }

    protected virtual void SelectedItemChanged()
    {
        OnSelectedChanged?.Invoke();
    }
}