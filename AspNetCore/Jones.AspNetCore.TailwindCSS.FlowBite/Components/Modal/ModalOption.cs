namespace Jones.AspNetCore.TailwindCSS.FlowBite.Components.Modal;

// https://flowbite.com/docs/components/modal/
public record ModalOption
{
    /// <summary>
    /// Choose between static or dynamic to prevent closing the modal when clicking outside.
    /// </summary>
    public string? Backdrop { get; set; }
    
    /// <summary>
    /// Set to false to disable closing the modal on hitting ESC or clicking on the backdrop.
    /// </summary>
    public bool? Closable { get; set; }
    
    /// <summary>
    /// Set a callback function when the modal has been hidden.
    /// </summary>
    public string? OnHide { get; set; }
    
    /// <summary>
    /// Set a callback function when the modal has been shown.
    /// </summary>
    public string? OnShow { get; set; }
    
    /// <summary>
    /// Set a callback function when the modal visibility has been toggled.
    /// </summary>
    public string? OnToggle { get; set; }
}