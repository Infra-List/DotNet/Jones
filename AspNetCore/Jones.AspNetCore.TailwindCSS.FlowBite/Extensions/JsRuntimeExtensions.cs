using Jones.AspNetCore.TailwindCSS.FlowBite.Components.Modal;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.TailwindCSS.FlowBite.Extensions;

// https://github.com/Timmoth/npm-webpack-blazor
public static class JsRuntimeExtensions
{
    private const string JsNamespace = "flowbiteX";

    public static ValueTask FlowBiteInit(this IJSRuntime jsRuntime) => jsRuntime.InvokeVoidAsync($"{JsNamespace}.init");

    public static ValueTask FlowBiteInitSidebar(this IJSRuntime jsRuntime) => jsRuntime.InvokeVoidAsync($"{JsNamespace}.initSidebar");

    public static ValueTask FlowBiteInitAccordions(this IJSRuntime jsRuntime) => jsRuntime.InvokeVoidAsync($"{JsNamespace}.initAccordions");
    
    public static ValueTask<IJSObjectReference> CreateTooltips(this IJSRuntime jsRuntime, string id) =>
        jsRuntime.InvokeAsync<IJSObjectReference>($"{JsNamespace}.createTooltips", id);

    public static ValueTask<IJSObjectReference> CreateModal<TResult>(this IJSRuntime jsRuntime, ElementReference modalElement, ModalOption option, DotNetObjectReference<Modal<TResult>> dotNetObjectReference) =>
        jsRuntime.InvokeAsync<IJSObjectReference>($"{JsNamespace}.createModal", modalElement, option, dotNetObjectReference);

    public static ValueTask ShowModal(this IJSRuntime jsRuntime, IJSObjectReference modal) => 
        jsRuntime.InvokeVoidAsync($"{JsNamespace}.showModal", modal);
    
    public static ValueTask CloseModal(this IJSRuntime jsRuntime, IJSObjectReference modal) => 
        jsRuntime.InvokeVoidAsync($"{JsNamespace}.closeModal", modal);
}