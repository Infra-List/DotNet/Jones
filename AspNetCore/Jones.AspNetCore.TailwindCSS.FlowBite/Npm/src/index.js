import {
    initAccordions,
    initCarousels,
    initCollapses,
    initDials,
    initDismisses,
    initDrawers,
    initDropdowns,
    initModals,
    initPopovers,
    initTabs,
    initTooltips,
    Modal,
    Tooltip
} from 'flowbite'

window.flowbiteX = {
    version: "1.0.0",
    init: function () {
        initAccordions();
        initCarousels();
        initCollapses();
        initDials();
        initDismisses();
        initDrawers();
        initDropdowns();
        initModals();
        initPopovers();
        initTabs();
        initTooltips();
    },
    initSidebar: function () {
        initSidebar();
    },
    createTooltips: function (id) {
        const $targetEl = document.getElementById(id);
        const $triggerEl = document.querySelectorAll(`[data-tooltip-target="${id}"]`)[0];
        return new Tooltip($targetEl, $triggerEl);
    },
    initAccordions: function () {
        initAccordions();
    },
    // initTooltips: function () {
    //     initTooltips();
    // },
    // createModal: function (sender, option, dotNetObject) {
    //     return createModal(sender, option, dotNetObject);
    // },
    // showModal: function (modal) {
    //     showModal(modal);
    // },
    // closeModal: function (modal) {
    //     closeModal(modal);
    // }
}

function initSidebar() {
    const sidebar = document.getElementById('sidebar');

    if (sidebar) {
        const toggleSidebarMobile = (sidebar, sidebarBackdrop, toggleSidebarMobileHamburger, toggleSidebarMobileClose) => {
            sidebar.classList.toggle('hidden');
            sidebarBackdrop.classList.toggle('hidden');
            toggleSidebarMobileHamburger.classList.toggle('hidden');
            toggleSidebarMobileClose.classList.toggle('hidden');
        }

        const toggleSidebarMobileEl = document.getElementById('toggleSidebarMobile');
        const sidebarBackdrop = document.getElementById('sidebarBackdrop');
        const toggleSidebarMobileHamburger = document.getElementById('toggleSidebarMobileHamburger');
        const toggleSidebarMobileClose = document.getElementById('toggleSidebarMobileClose');
        const toggleSidebarMobileSearch = document.getElementById('toggleSidebarMobileSearch');

        if (toggleSidebarMobileSearch) {
            toggleSidebarMobileSearch.addEventListener('click', () => {
                toggleSidebarMobile(sidebar, sidebarBackdrop, toggleSidebarMobileHamburger, toggleSidebarMobileClose);
            });
        }
        
        if (toggleSidebarMobileEl) {
            toggleSidebarMobileEl.addEventListener('click', () => {
                toggleSidebarMobile(sidebar, sidebarBackdrop, toggleSidebarMobileHamburger, toggleSidebarMobileClose);
            });
        }
        
        if (sidebarBackdrop) {
            sidebarBackdrop.addEventListener('click', () => {
                toggleSidebarMobile(sidebar, sidebarBackdrop, toggleSidebarMobileHamburger, toggleSidebarMobileClose);
            });
        }
    }
}

function getModalOption(option, dotNetObject) {
    let modalOption = {};
    if (modalOption.backdrop) {
        modalOption.backdrop = option.backdrop;
    }
    if (modalOption.closable) {
        modalOption.closable = option.closable;
    }
    if (option.onHide) {
        modalOption.onHide = function () {
            dotNetObject && dotNetObject.invokeMethodAsync(option.onHide);
        };
    }
    if (option.onShow) {
        modalOption.onShow = function () {
            dotNetObject && dotNetObject.invokeMethodAsync(option.onShow);
        };
    }
    if (option.onToggle) {
        modalOption.onToggle = function () {
            dotNetObject && dotNetObject.invokeMethodAsync(option.onToggle);
        };
    }
    
    return modalOption;
}

function createModal(sender, option, dotNetObject) {
    const modalOption = getModalOption(option, dotNetObject);
    return new Modal(sender, modalOption);
}

function showModal(modal) {
    modal.show();
}

function closeModal(modal) {
    modal.hide();
}

window.flowbiteX.modal = {
    version: "1.0.0",
    getModal : (sender) => {
        const modal = bootstrap.Modal.getInstance(sender);
        if (modal == null) {
            return new bootstrap.Modal(sender);
        } else {
            return modal;
        }
    },
    toggle: function(sender){
        this.getModal(sender).toggle();
    },
    show: function(sender){
        this.getModal(sender).show();
    },
    hide: function(sender){
        this.getModal(sender).hide();
    },
    handleUpdate: function(sender){
        this.getModal(sender).handleUpdate();
    },
    dispose: function(sender){
        $(sender).remove();
        // this.getModal(sender).dispose();
    },
    onShow: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('show.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onShown: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('shown.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHide: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hide.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHidden: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hidden.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHidePrevented: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hidePrevented.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    showLoadingModal: (isProgress) => {
        let modalLoading = $('<div class="modal fade mt-5" data-bs-backdrop="static" tabindex="-1" aria-hidden="true">' +
            '<div class="modal-dialog">' +
            '<div class="progress active">' +
            '<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:100%"></div></div></div></div>');
        if (isProgress) {
            $(modalLoading).find(".progress-bar").css("width", "0");
        } else {
            $(modalLoading).find(".progress-bar").css("width", "100%");
        }
        new bootstrap.Modal(modalLoading).show();
        return modalLoading;
    },
    setLoadingModalProgress: (loadingModal, percent, message) => {
        $(loadingModal).find(".progress-bar").html(message);
        $(loadingModal).find(".progress-bar").css("width", percent + "%");
        // bootstrap.Modal.getInstance(loadingModal).handleUpdate();
    },
    closeLoadingModal: (loadingModal) => {
        // bootstrap.Modal.getInstance(loadingModal).hide();
        // https://github.com/twbs/bootstrap/issues/25008
        const int = setInterval(function () {
            if ($(loadingModal).is(".show"))
                bootstrap.Modal.getInstance(loadingModal).hide();
            else
                clearInterval(int);
        }.bind(this), 100);
    },
};

window.flowbiteX.offcanvas = {
    version: "1.0.0",
    getOffcanvas : (sender) => {
        const offcanvas = bootstrap.Offcanvas.getInstance(sender);
        if (offcanvas == null) {
            return new bootstrap.Offcanvas(sender);
        } else {
            return offcanvas;
        }
    },
    toggle: function(sender){
        this.getOffcanvas(sender).toggle();
    },
    show: function(sender){
        this.getOffcanvas(sender).show();
    },
    hide: function(sender){
        this.getOffcanvas(sender).hide();
    },
    dispose: function(sender){
        $(sender).remove();
        // this.getOffcanvas(sender).dispose();
    },
    onShow: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('show.bs.offcanvas', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onShown: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('shown.bs.offcanvas', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHide: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hide.bs.offcanvas', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHidden: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hidden.bs.offcanvas', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    }
};