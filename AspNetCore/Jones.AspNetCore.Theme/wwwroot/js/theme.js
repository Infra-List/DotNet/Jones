﻿export function isDarkTheme() {
    return  window.matchMedia("(prefers-color-scheme: dark)").matches;
}

export function createThemeListener(dotNetRef) {
    window.matchMedia("(prefers-color-scheme: dark)").addEventListener('change', (mediaQuery) => {
        dotNetRef.invokeMethodAsync("DarkModeStateChanged", mediaQuery.matches)
    });
}