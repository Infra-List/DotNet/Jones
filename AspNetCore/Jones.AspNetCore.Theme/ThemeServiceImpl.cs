using Jones.AspNetCore.Extensions;
using Jones.AspNetCore.Services;
using Jones.AspNetCore.Types;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Theme;

public class ThemeServiceImpl : IThemeService, IAsyncDisposable
{
    private const string  ScriptFile = "js/theme.js";
    
    private const string JsIsDarkTheme = "isDarkTheme";
    private const string JsCreateThemeListener = "createThemeListener";

    private static readonly string ThemeModeLocalStorageKey = $"{typeof(ThemeServiceImpl).FullName ?? nameof(ThemeServiceImpl)}.ThemeMode";
    
    private readonly ILocalStorageService _localStorage;
    private readonly Lazy<Task<IJSObjectReference>> _moduleTask;
    
    private bool _isInitialized;
    
    public ThemeServiceImpl(ILocalStorageService localStorage, IJSRuntime jsRuntime)
    {
        _localStorage = localStorage;
        _moduleTask = new Lazy<Task<IJSObjectReference>>(() => jsRuntime.Import<ThemeServiceImpl>(ScriptFile));
    }

    public async Task Initialize()
    {
        if (!_isInitialized)
        {
            _isInitialized = true;
            
            ThemeMode = await _localStorage.Get<ThemeMode?>(ThemeModeLocalStorageKey) ?? ThemeMode.System;

            await (await _moduleTask.Value).InvokeVoidAsync(JsCreateThemeListener, DotNetObjectReference.Create(this)).ConfigureAwait(false);
        }
    }

    public ThemeMode ThemeMode { get; private set; }

    public async void SetThemeMode(ThemeMode themeMode)
    {
        ThemeMode = themeMode;
        await _localStorage.Set(ThemeModeLocalStorageKey, ThemeMode); 
    }

    [JSInvokable]
    public async Task DarkModeStateChanged(bool state)
    {
        if (OnDarkModeStateChanged == null)
        {
            return;
        }
        await OnDarkModeStateChanged.Invoke(state);
    }

    public async Task<bool> IsPrefersColorSchemeDarkAsync() =>
        await (await _moduleTask.Value).InvokeAsync<bool>(JsIsDarkTheme, null).ConfigureAwait(false);

    public event NotifyDarkModeStateChange? OnDarkModeStateChanged;

    public async ValueTask DisposeAsync()
    {
        if (_moduleTask.IsValueCreated)
        {
            await (await _moduleTask.Value).DisposeAsync();
        }
    }
}