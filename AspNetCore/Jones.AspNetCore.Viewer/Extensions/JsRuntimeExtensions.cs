using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Viewer.Extensions;

public static class JsRuntimeExtensions
{
    private const string JsNamespace = "viewer";
    
    
    public static ValueTask<IJSObjectReference> ViewerInit(this IJSRuntime jsRuntime, ElementReference element, ViewerOptions? options) =>
        jsRuntime.InvokeAsync<IJSObjectReference>($"{JsNamespace}.init", element, options);
    
    public static ValueTask<IJSObjectReference> ViewerDestroy(this IJSRuntime jsRuntime, IJSObjectReference viewer) =>
        jsRuntime.InvokeAsync<IJSObjectReference>($"{JsNamespace}.destroy", viewer);
}