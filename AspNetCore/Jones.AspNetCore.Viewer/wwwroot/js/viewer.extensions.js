window.viewer = {
    version: "1.0.0",
    init: function (element, options) {
        if (options.toolBarLite) {
            options.toolbar = {
                zoomIn: true,
                zoomOut: true,
                //rotateLeft: true,
                rotateRight: true,
                //prev: true,
                //next: true,
            };
        }
        return new Viewer(element, options);
    },
    destroy: function (viewer) {
        viewer.destroy();
    },
}