﻿namespace Jones.AspNetCore.Viewer;

/// <summary>
/// 图片浏览器选项类
/// </summary>
public record ViewerOptions
{
    public bool? ToolBarLite { get; set; }
}