namespace Jones.AspNetCore.Components;

public interface IRefreshableComponent
{
    void Refresh();
}