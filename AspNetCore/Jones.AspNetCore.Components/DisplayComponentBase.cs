using System;
using System.Globalization;
using System.Linq.Expressions;
using Jones.AspNetCore.Utils;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Components;

public abstract class DisplayComponentBase<TField> : JonesComponentBase
{
    [Parameter]
    public string? Title { get; set; }

    [Parameter]
    public RenderFragment? TitleTemplate { get; set; }
        
    /// <summary>
    /// 获得/设置 数据绑定字段值
    /// </summary>
    [Parameter]
    public TField Field { get; set; }
        
    [Parameter]
    public EventCallback<TField> FieldChanged { get; set; }

    /// <summary>
    /// 获得/设置 ValueExpression 表达式
    /// </summary>
    [Parameter]
    public Expression<Func<TField>>? FieldExpression { get; set; }

    [Parameter]
    public string? Format { get; set; }
        
    /// <summary>
    /// 获得/设置 列格式化回调委托
    /// </summary>
    [Parameter]
    public Func<TField, string?>? Formatter { get; set; }
        
    [Parameter]
    public bool? IsTimeStamp { get; set; }
        
    [Parameter]
    public bool? IsLocalDateTime { get; set; }
        
    public virtual string? GetTitle() => Title ?? FieldExpression?.GetDisplayName();

    public virtual string? GetText()
    {
        if (Formatter != null)
        {
            return Formatter.Invoke(Field);
        }
        if (IsTimeStamp == true)
        {
            var dateTime = TimeStampToDateTime();
            if (dateTime != null)
            {
                if (string.IsNullOrWhiteSpace(Format))
                {
                    return dateTime.ToString();
                }
                return Formatter<DateTime>.Format(dateTime.Value, Format);
            }
        }

        if (typeof(TField) == typeof(DateTime) || typeof(TField) == typeof(DateTime?))
        {
            if (Field != null)
            {
                // Console.WriteLine($"时间：{Field}");
                var dateTime = Convert.ToDateTime(Field);
                if (IsLocalDateTime != true)
                {
                    dateTime = dateTime.ToLocalTime();
                }
                if (string.IsNullOrWhiteSpace(Format))
                {
                    return dateTime.ToString(CultureInfo.InvariantCulture);
                }
                return Formatter<DateTime>.Format(dateTime, Format);
            }
        }

        if (!string.IsNullOrWhiteSpace(Format))
        {
            return Formatter<TField>.Format(Field, Format);
        }
            
        if(IsEnum())
        {
            return (Field as Enum)?.GetDisplayName() ?? Field?.ToString();
        }
            
        return Field?.ToString();
    }

    protected virtual DateTime? TimeStampToDateTime() => Field == null ? null : Convert.ToInt64(Field).ToLocalDateTime();

    protected virtual bool IsEnum()
    {
        var type = Nullable.GetUnderlyingType(typeof(TField)) ?? typeof(TField);
        return type.IsEnum;
        // return typeof(TField).IsEnum;
    }
}