using System.Collections.Generic;

namespace Jones.AspNetCore.Components.Description;

public interface IDescription
{
    IList<IDescriptionItem> Items { get; }
}