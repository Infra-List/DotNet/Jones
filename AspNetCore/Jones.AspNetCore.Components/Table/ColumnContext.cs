using System.Collections.Generic;

namespace Jones.AspNetCore.Components.Table;

public class ColumnContext
{
    private readonly ITable _table;

    public IList<ITableColumn> Columns { get; } = new List<ITableColumn>();

    public IList<ITableColumn> HeaderColumns { get; } = new List<ITableColumn>();

    public ColumnContext(ITable table)
    {
        _table = table;
    }
        
    public void AddColumn(ITableColumn column)
    {
        Columns.Add(column);
    }

    public void AddHeaderColumn(ITableColumn column)
    {
        HeaderColumns.Add(column);
    }
}