using System;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Components.Table;

public class TableColumn<TField> : DisplayComponentBase<TField>, ITableColumn
{
    [CascadingParameter]
    public ITable? Table { get; set; }
        
    [CascadingParameter]
    public ColumnContext? Context { get; set; }
        
    [CascadingParameter(Name = "IsInitialize")]
    public bool IsInitialize { get; set; }
        
    [CascadingParameter(Name = "IsHeader")]
    public bool IsHeader { get; set; }

    [CascadingParameter(Name = "IsFooter")]
    public bool IsFooter { get; set; }

    [Parameter]
    public RenderFragment? FooterTemplate { get; set; }
        
    [Parameter]
    public Func<string?>? Footer { get; set; }
    
    [Parameter]
    public string? Style { get; set; }

    /// <summary>
    /// 获得/设置 子组件
    /// </summary>
    [Parameter]
    public RenderFragment? ChildContent { get; set; }

    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (IsInitialize)
        {
            Context?.AddColumn(this);
        }
        else if (IsHeader)
        {
            Context?.AddHeaderColumn(this);
        }
    }
}