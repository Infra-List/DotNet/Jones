using System;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Components.Table;

public interface ITableColumn
{
    /// <summary>
    /// 获得/设置 列 td 自定义样式 默认为 null 未设置
    /// </summary>
    ITable? Table { get; }
        
    bool IsHeader { get; }
        
    string? Title { get; }
        
    /// <summary>
    /// 是否汇总 Column
    /// </summary>
    bool IsFooter { get; }
        
    RenderFragment? FooterTemplate { get; }
        
    Func<string?>? Footer { get; }
}