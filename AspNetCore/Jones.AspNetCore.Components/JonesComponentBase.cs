using System.Collections.Generic;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Components;

public class JonesComponentBase : ComponentBase
{
    [Parameter(CaptureUnmatchedValues = true)]
    public IReadOnlyDictionary<string, object>? AdditionalAttributes { get; set; }
}