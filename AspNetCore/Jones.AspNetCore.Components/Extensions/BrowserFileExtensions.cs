using System.IO;
using System.Threading.Tasks;
using Jones.AspNetCore.Models;
using Microsoft.AspNetCore.Components.Forms;

namespace Jones.AspNetCore.Components.Extensions;

public static class BrowserFileExtensions
{
    public static async Task<byte[]> GetContent(this IBrowserFile file, long maxFileSize)
    {
        await using var stream = file.OpenReadStream(maxFileSize);
        await using var ms = new MemoryStream();
        await stream.CopyToAsync(ms);
        return ms.ToArray();
    }

    public static async Task<FileViewModel> GetFileViewModel(this IBrowserFile file, long maxFileSize) =>
        new FileViewModel
        {
            Name = file.Name,
            Size = file.Size,
            ContentType = file.ContentType,
            Content = await file.GetContent(maxFileSize)
        };
}