using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Components.Extensions;

/// <summary>
/// string 扩展方法
/// </summary>
public static class StringExtensions
{
    /// <summary>
    /// 字符串类型转换为其他数据类型
    /// </summary>
    /// <returns></returns>
    public static bool TryConvertTo(this string? source, Type type, [MaybeNullWhen(false)] out object? val)
    {
        bool ret;
        if (type == typeof(string))
        {
            val = source;
            ret = true;
        }
        else
        {
            var methodInfo = typeof(StringExtensions).GetMethods().FirstOrDefault(m => m.IsGenericMethod)!.MakeGenericMethod(type);
            var v = type == typeof(string) ? null : Activator.CreateInstance(type);
            var args = new[] { source, v };
            ret = (bool)methodInfo.Invoke(null, args)!;
            val = ret ? args[1] : null;
        }
        return ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="source"></param>
    /// <param name="val"></param>
    /// <returns></returns>
    public static bool TryConvertTo<TValue>(this string? source, [MaybeNullWhen(false)] out TValue val)
    {
        var ret = false;
        var type = Nullable.GetUnderlyingType(typeof(TValue)) ?? typeof(TValue);
        if (type == typeof(string))
        {
            val = (TValue)(object)source!;
            ret = true;
        }
        else
        {
            try
            {
                if (source == null)
                {
                    val = default;
                    ret = true;
                }
                else if (source == string.Empty)
                {
                    ret = BindConverter.TryConvertTo(source, CultureInfo.InvariantCulture, out val);
                }
                else
                {
                    var isBoolean = type == typeof(bool);
                    var v = isBoolean ? (object)source.Equals("true", StringComparison.CurrentCultureIgnoreCase) : source;
                    ret = BindConverter.TryConvertTo(v, CultureInfo.InvariantCulture, out val);
                }
            }
            catch
            {
                val = default;
            }
        }
        return ret;
    }
}