namespace Jones.AspNetCore.Components.Types;

public enum InputUpdateTrigger
{
    OnChange,
    OnInput
}