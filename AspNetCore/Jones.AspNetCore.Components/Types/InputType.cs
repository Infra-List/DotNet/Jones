using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Components.Types;

public enum InputType
{
    [Display(Name = "button")]
    Button,
        
    [Display(Name = "checkbox")]
    Checkbox,
        
    [Display(Name = "color")]
    Color,
        
    [Display(Name = "date")]
    Date,
        
    [Display(Name = "datetime-local")]
    DatetimeLocal,
        
    [Display(Name = "email")]
    Email,
        
    [Display(Name = "file")]
    File,
        
    [Display(Name = "hidden")]
    Hidden,
        
    [Display(Name = "image")]
    Image,
        
    [Display(Name = "month")]
    Month,
        
    [Display(Name = "number")]
    Number,
        
    [Display(Name = "password")]
    Password,
        
    [Display(Name = "radio")]
    Radio,
        
    [Display(Name = "range")]
    Range,
        
    [Display(Name = "reset")]
    Reset,
        
    [Display(Name = "search")]
    Search,
        
    [Display(Name = "submit")]
    Submit,
        
    [Display(Name = "tel")]
    Tel,
        
    [Display(Name = "text")]
    Text,
        
    [Display(Name = "time")]
    Time,
        
    [Display(Name = "url")]
    Url,
        
    [Display(Name = "week")]
    Week
}