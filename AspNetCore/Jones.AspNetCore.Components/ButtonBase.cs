using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Jones.AspNetCore.Components;

public abstract class ButtonBase : JonesComponentBase
{
    private bool _isDisabled;
    [Parameter]
    public bool IsDisabled
    {
        get => _isDisabled || IsLoading == true;
        set => _isDisabled = value; 
    }

    [Parameter]
    public bool? IsLoading { get; set; }
    
    [Parameter]
    public string? Icon { get; set; }
        
    [Parameter]
    public string? LoadingIcon { get; set; }
    
    [Parameter]
    public RenderFragment? ChildContent { get; set; }
    
    protected string? Disabled => IsDisabled ? "true" : null;

    [Parameter]
    public EventCallback<MouseEventArgs> OnClick { get; set; }
}

public record ButtonConfig
{
    public string? LoadingIcon { get; set; }
}