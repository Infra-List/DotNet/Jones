using System;
using System.Linq;
using System.Threading.Tasks;
using Jones.AspNetCore.Components.Extensions;
using Jones.AspNetCore.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace Jones.AspNetCore.Components.Forms;

public abstract class FilesInputBase : JonesComponentBase
{
    protected string MaximumFileCountLimitExceededTips => $"上传文件数不能{MaximumFileCount}个";
        
    [Parameter] public string? Placeholder { get; set; }
        
    [Parameter] public bool IsAllowDrop { get; set; } = true;
        
    [Parameter] public bool IsAllowPaste { get; set; } = true;
        
    [Parameter] public string? Accept { get; set; }
        
    private bool? _isMultiple;
    [Parameter]
    public bool? IsMultiple {
        get
        {
            if (_isMultiple != null)
                return _isMultiple;
            return MaximumFileCount <= 1 ? null : true;
        }
        set => _isMultiple = value;
    }
        
    [Parameter] public long MaxFileSize { get; set; } = 10 * 1024 * 1024;

    private int? _maximumFileCount;
    [Parameter] 
    public int MaximumFileCount {
        get
        {
            if (_isMultiple == false)
            {
                return 1;
            }

            return _maximumFileCount ?? (_isMultiple == true ? 10 : 1);
        }
        set => _maximumFileCount = value;
    }
        
    [Parameter] public Func<FileViewModel[]?, Task>? OnChange { get; set; }
        
    [Parameter] public Func<string, Task>? OnError { get; set; }

    [Parameter] public int ImageMaxWith { get; set; } = 1024;
        
    [Parameter] public int ImageMHeight { get; set; } = 1024;
        
    [Parameter] public RenderFragment? LabelContent { get; set; }
        
    [Parameter] public bool? IsRequired { get; set; }
        
    [Parameter] public Func<IBrowserFile, long, Task<(bool IsSuccess, FileViewModel? File, string? Message)>>? OnTransform { get; set; }
    
    [Parameter]
    public string[]? InitFiles { get; set; }

    private FileViewModel[] _files = Array.Empty<FileViewModel>();
    public FileViewModel[] Files
    {
        get => _files;
        private set
        {
            _files = value;
            OnChange?.Invoke(_files);
        }
    }

    // https://fengyuanchen.github.io/compressorjs/
    protected async Task OnFilesChange(InputFileChangeEventArgs e)
    {
        if (Files.Length + e.FileCount > MaximumFileCount)
        {
            await OnFilesError(MaximumFileCountLimitExceededTips);
            return;
        }
        var files = Files.ToList();
        foreach (var file in e.GetMultipleFiles(MaximumFileCount))
        {
            if (files.Count >= MaximumFileCount)
            {
                break;
            }
            var (isSuccess, fileViewModel, message) = await Get(file);
            if (!isSuccess)
            {
                await OnFilesError(message ?? $"文件（{file.Name}）转换失败");
                return;
            }
            if (fileViewModel == null)
            {
                await OnFilesError($"文件（{file.Name}）转换后为空");
                return;
            }
            files.Add(fileViewModel);
        }
        Files = files.ToArray();
    }

    private async Task<(bool IsSuccess, FileViewModel? File, string? Message)> Get(IBrowserFile file) =>
        OnTransform == null ? (true, await file.GetFileViewModel(MaxFileSize), null) : await OnTransform.Invoke(file, MaxFileSize);

    protected async Task OnFilesError(string message)
    {
        if (OnError != null)
        {
            await OnError.Invoke(message);
        }
    }

    public void Delete(FileViewModel file)
    {
        var files = Files.ToList();
        files.Remove(file);
        Files = files.ToArray();
        StateHasChanged();
    }

    public void DeleteInitFiles(string file)
    {
        if (InitFiles == null)
        {
            return;
        }
        var initFiles = InitFiles.ToList();
        initFiles.Remove(file);
        InitFiles = initFiles.ToArray();
        StateHasChanged();
    }

    public void Clear()
    {
        Files = Array.Empty<FileViewModel>();
        InitFiles = null;
        StateHasChanged();
    }
}