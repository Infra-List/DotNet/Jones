using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.Components.Forms;

public class InputOptGroupSelectData<TItem, TValue> : InputOptGroupSelectData<OptGroup<TItem>, TItem, TValue> { }

public class InputOptGroupSelectData<TOptGroup, TItem, TValue> : InputBase<TValue> where TOptGroup : OptGroup<TItem>
{
    private IEnumerable<TOptGroup>? _optGroups;

    [Parameter]
    public IEnumerable<TOptGroup>? OptGroups
    {
        get => _optGroups;
        set
        {
            if (value == null && _optGroups == null || value != null && _optGroups != null && value.SequenceEqual(_optGroups))
            {
                return;
            }

            IsDisabled = value?.Any() != true;
            _optGroups = value;
            // Console.WriteLine(GetType().Name + "，Items Count：" + _items?.Count());
            AdjustmentValue();
        }
    }
        
    [Parameter]
    public bool? IsRequired { get; set; }
        
    [Parameter]
    public Func<Task<IEnumerable<TOptGroup>?>>? OnGetData { get; set; }

    [Parameter]
    public Func<TItem, string?>? OnGetValue { get; set; }
        
    [Parameter]
    public Func<TItem, string?>? OnGetText { get; set; }

    [Parameter]
    public Action? OnSelectedChanged { get; set; }

    [Parameter]
    public Action<TItem?>? OnSelectedItemChanged { get; set; }
        
    [Parameter]
    public string? NullValueText { get; set; }
        
    [Parameter]
    public bool? IsDisabled { get; set; }

    private int _sequence;

    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (ValueExpression != null)
        {
            var fieldName = FieldIdentifier.Create(ValueExpression).FieldName;
            EditContext.OnFieldChanged += (_, args) =>
            {
                if (args.FieldIdentifier.FieldName == fieldName)
                {
                    SelectedChanged();
                    SelectedItemChanged(Value);
                }
            };
        }
    }

    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(++_sequence, "select");
        builder.AddMultipleAttributes(++_sequence, AdditionalAttributes);
        builder.AddAttribute(++_sequence, "class", CssClass);
        builder.AddAttribute(++_sequence, "value", BindConverter.FormatValue(CurrentValueAsString));
        builder.AddAttribute(++_sequence, "onchange", EventCallback.Factory.CreateBinder<string?>(this, value => CurrentValueAsString = value, CurrentValueAsString));

        if (IsDisabled == true)
        {
            builder.AddAttribute(++_sequence, "disabled", "true");
        }

        var isHasAll = IsRequired != true && ValueExpression?.IsHasRequiredAttribute() != true;
        if (isHasAll)
        {
            builder.OpenElement(++_sequence, "option");
            builder.AddAttribute(++_sequence, "value", "");
            builder.AddContent(++_sequence, NullValueText ?? $"全部{ValueExpression?.GetDisplayName()}");
            builder.CloseElement();
        }

        if (OptGroups != null)
        {
            if (OptGroups.Count() == 1)
            {
                foreach (var option in OptGroups.First().Options)
                {
                    CreateOption(builder, option);
                }
            }
            else
            {
                foreach (var optGroup in OptGroups)
                {
                    CreateOptGroup(builder, optGroup);
                }
            }
        }
            
        builder.CloseElement();
    }

    private void CreateOptGroup(RenderTreeBuilder builder, TOptGroup optGroup)
    {
        builder.OpenElement(++_sequence, "optgroup");
        builder.AddAttribute(++_sequence, "label", optGroup.Label);
        foreach (var option in optGroup.Options)
        {
            CreateOption(builder, option);
        }
        builder.CloseElement();
    }

    private void CreateOption(RenderTreeBuilder builder, TItem option)
    {
        builder.OpenElement(++_sequence, "option");
        builder.AddAttribute(++_sequence, "value", GetValue(option));
        builder.AddContent(++_sequence, GetText(option));
        builder.CloseElement();
    }
        
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            _ = InitData();
        }
    }

    protected void SelectedChanged()
    {
        OnSelectedChanged?.Invoke();
    }

    protected void SelectedItemChanged(TValue? value)
    {
        OnSelectedItemChanged?.Invoke(OptGroups == null || value == null ? default : OptGroups.SelectMany(p => p.Options).FirstOrDefault(p => GetValue(p) == FormatValueAsString(value)));
    }

    protected virtual async Task InitData()
    {
        await Get();
    }

    protected async Task Get()
    {
        OptGroups = await GetData();
    }

    protected async Task<IEnumerable<TOptGroup>?> GetData()
    {
        if (OnGetData == null)
        {
            return null;
        }
            
        return await OnGetData.Invoke();
    }

    private async void AdjustmentValue()
    {
        if (OptGroups?.Any() != true)
        {
            await ResetValue(default);
        }
        else
        {
            if (OptGroups.All(p => p.Options.All(o => GetValue(o) != CurrentValueAsString)))
            {
                if (IsRequired != true && ValueExpression?.IsHasRequiredAttribute() != true)
                {
                    await ResetValue(default);
                }
                else
                {
                    var valueString = GetValue(OptGroups.First().Options.First());
                    if (TryParseValueFromString(valueString, out var value, out _))
                    {
                        await ResetValue(value);
                    }
                }
            }
        }
    }

    private async Task ResetValue(TValue? value)
    {
        if (Value == null && value == null || value != null && value.Equals(Value))
        {
            return;
        }
        await ValueChanged.InvokeAsync(value);
        SelectedChanged();
        SelectedItemChanged(value);
    }

    protected string? GetValue(TItem item) => OnGetValue?.Invoke(item);
        
    protected string? GetText(TItem item) => OnGetText?.Invoke(item);

    /// <inheritdoc />
    protected override bool TryParseValueFromString(string? value, [MaybeNullWhen(false)] out TValue result,
        [NotNullWhen(false)] out string? validationErrorMessage)
    {
        try
        {
            if (BindConverter.TryConvertTo<TValue>(value, CultureInfo.CurrentCulture, out var parsedValue))
            {
                result = parsedValue;
                validationErrorMessage = null;
                return true;
            }
            else
            {
                result = default;
                validationErrorMessage = $"The {DisplayName ?? FieldIdentifier.FieldName} field is not valid.";
                return false;
            }
        }
        catch (InvalidOperationException ex)
        {
            throw new InvalidOperationException($"{GetType()} does not support the type '{typeof(TValue)}'.", ex);
        }
    }
}

public record OptGroup<TItem>(string Label, IEnumerable<TItem> Options);