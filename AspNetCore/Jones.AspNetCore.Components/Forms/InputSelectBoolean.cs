using System;
using System.Diagnostics.CodeAnalysis;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.Components.Forms;

public class InputSelectBoolean : InputSelect<bool?>
{
    [CascadingParameter] 
    protected EditContext? EditContext { get; set; }
        
    [Parameter]
    public bool? IsRequired { get; set; }
        
    [Parameter]
    public string? NullValueText { get; set; }
        
    [Parameter]
    public string? TrueText { get; set; }
        
    [Parameter]
    public string? FalseText { get; set; }
        
    [Parameter]
    public Action? OnSelectedChanged { get; set; }

    private int _sequence;

    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (OnSelectedChanged != null && EditContext != null && ValueExpression != null)
        {
            var fieldName = FieldIdentifier.Create(ValueExpression).FieldName;
            EditContext.OnFieldChanged += (_, args) =>
            {
                if (args.FieldIdentifier.FieldName == fieldName)
                {
                    OnSelectedChanged.Invoke();
                }
            };
        }
    }

    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(++_sequence, "select");
        builder.AddMultipleAttributes(++_sequence, AdditionalAttributes);
        builder.AddAttribute(++_sequence, "class", CssClass);
        builder.AddAttribute(++_sequence, "value", BindConverter.FormatValue(CurrentValueAsString));
        builder.AddAttribute(++_sequence, "onchange", EventCallback.Factory.CreateBinder<string?>(this, value => CurrentValueAsString = value, CurrentValueAsString));

        var isHasAll = IsRequired != true && ValueExpression?.IsHasRequiredAttribute() != true;
        if (isHasAll)
        {
            builder.OpenElement(++_sequence, "option");
            builder.AddAttribute(++_sequence, "value", "");
            builder.AddContent(++_sequence, NullValueText ?? ValueExpression?.GetDisplayName());
            builder.CloseElement();
        }

        CreateOption(builder, true.ToString().ToLower(), TrueText ?? "是");
        CreateOption(builder, false.ToString().ToLower(), FalseText ?? "否");
            
        builder.CloseElement();
    }

    private void CreateOption(RenderTreeBuilder builder, string value, string text)
    {
        builder.OpenElement(++_sequence, "option");
        builder.AddAttribute(++_sequence, "value", value);
        builder.AddContent(++_sequence, text);
        builder.CloseElement();
    }
        
    protected override bool TryParseValueFromString(string? value, out bool? result, [NotNullWhen(false)] out string? validationErrorMessage)
    {
        if (string.IsNullOrEmpty(value))
        {
            result = null;
        }
        else
        {
            result = value.ToLower().Equals("true");
        }
        validationErrorMessage = null;
        return true;
    }
        
    protected override string? FormatValueAsString(bool? value)
    {
        return value?.ToString().ToLower();
    }
}