using System;
using System.Collections.Generic;
using System.Linq;
using Jones.AspNetCore.Helper;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Components.Forms;

public abstract class CheckboxesBase<To, Ti> : JonesComponentBase
{
    [Parameter]
    public To[]? Options { get; set; }
        
    [Parameter]
    public Func<To, string?> GetOptionText { get; set; }
        
    [Parameter]
    public Func<To, Ti> ToItem { get; set; }

    [Parameter]
    public Ti[]? InitialItems { get; set; }
        
    [Parameter]
    public Action<Ti[]> OnChanged { get; set; }
        
    [Parameter]
    public string? SpacingClass { get; set; }

    private List<Ti> _items = new();
        
    protected override void OnInitialized()
    {
        base.OnInitialized();
            
        if (InitialItems?.Any() == true)
        {
            _items.AddRange(InitialItems);
        }
    }
        
    protected string GetId() => $"checkbox_{IdHelper.GetId()}";

    protected string? GetCheckedString(To option) => IsChecked(option) ? "checked" : null;

    private bool IsChecked(To option) => _items.Contains(ToItem(option));

    protected void CheckboxChanged(ChangeEventArgs e, To option)
    {
        var item = ToItem(option);
        if (_items.Contains(item))
        {
            _items.Remove(item);
        }
        else
        {
            _items.Add(item);
        }

        OnChanged(_items.ToArray());
    }

    public void SelectAll()
    {
        _items = Options.Select(p => ToItem(p)).ToList();
        OnChanged(_items.ToArray());
    }

    public void UnselectAll()
    {
        _items.Clear();
        OnChanged(_items.ToArray());
    }

    public void Inversion()
    {
        _items = Options.Select(p => ToItem(p)).Where(p => !_items.Contains(p)).ToList();
        OnChanged(_items.ToArray());
    }
}