using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.Components.Forms;

public class InputSelectData<TItem, TValue> : InputBase<TValue>
{
    private IEnumerable<TItem>? _items;

    [Parameter]
    public IEnumerable<TItem>? Items
    {
        get => _items;
        set
        {
            if (value == null && _items == null || value != null && _items != null && value.SequenceEqual(_items))
            {
                return;
            }

            IsDisabled = value?.Any() != true;
            _items = value;
            Console.WriteLine(GetType().Name + "，Items Count：" + _items?.Count());
            AdjustmentValue();
            SelectedChanged();
        }
    }
        
    [Parameter]
    public bool? IsRequired { get; set; }
        
    [Parameter]
    public Func<Task<IEnumerable<TItem>?>>? OnGetData { get; set; }

    [Parameter]
    public Func<TItem, string?>? OnGetValue { get; set; }
        
    [Parameter]
    public Func<TItem, string?>? OnGetText { get; set; }

    [Parameter]
    public Action? OnSelectedChanged { get; set; }

    [Parameter]
    public Action<TItem?>? OnSelectedItemChanged { get; set; }
        
    [Parameter]
    public string? NullValueText { get; set; }
        
    [Parameter]
    public bool? IsDisabled { get; set; }

    private int _sequence;

    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (ValueExpression != null)
        {
            var fieldName = FieldIdentifier.Create(ValueExpression).FieldName;
            EditContext.OnFieldChanged += (_, args) =>
            {
                if (args.FieldIdentifier.FieldName == fieldName)
                {
                    SelectedChanged();
                    SelectedItemChanged(Value);
                }
            };
        }
    }

    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(++_sequence, "select");
        builder.AddMultipleAttributes(++_sequence, AdditionalAttributes);
        builder.AddAttribute(++_sequence, "class", CssClass);
        builder.AddAttribute(++_sequence, "value", BindConverter.FormatValue(CurrentValueAsString));
        builder.AddAttribute(++_sequence, "onchange", EventCallback.Factory.CreateBinder<string?>(this, value => CurrentValueAsString = value, CurrentValueAsString));

        if (IsDisabled == true)
        {
            builder.AddAttribute(++_sequence, "disabled", "true");
        }

        var isHasAll = IsRequired != true && ValueExpression?.IsHasRequiredAttribute() != true;
        if (isHasAll)
        {
            builder.OpenElement(++_sequence, "option");
            builder.AddAttribute(++_sequence, "value", "");
            builder.AddContent(++_sequence, NullValueText ?? $"全部{ValueExpression?.GetDisplayName()}");
            builder.CloseElement();
        }

        if (Items != null)
        {
            foreach (var item in Items)
            {
                CreateOption(builder, item);
            }
        }
            
        builder.CloseElement();
    }

    private void CreateOption(RenderTreeBuilder builder, TItem item)
    {
        builder.OpenElement(++_sequence, "option");
        builder.AddAttribute(++_sequence, "value", GetValue(item));
        builder.AddContent(++_sequence, GetText(item));
        builder.CloseElement();
    }
        
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            _ = InitData();
        }
    }

    protected void SelectedChanged()
    {
        OnSelectedChanged?.Invoke();
    }

    protected void SelectedItemChanged(TValue? value)
    {
        OnSelectedItemChanged?.Invoke(Items == null || value == null ? default : Items.FirstOrDefault(p => GetValue(p) == FormatValueAsString(value)));
    }

    protected virtual async Task InitData()
    {
        await Get();
    }

    protected async Task Get()
    {
        Items = await GetData();
    }

    protected async Task<IEnumerable<TItem>?> GetData()
    {
        if (OnGetData == null)
        {
            return null;
        }
            
        return await OnGetData.Invoke();
    }

    private async void AdjustmentValue()
    {
        if (Items?.Any() != true)
        {
            await ResetValue(default);
        }
        else
        {
            if (Items.All(p => GetValue(p) != CurrentValueAsString))
            {
                if (IsRequired != true && ValueExpression?.IsHasRequiredAttribute() != true)
                {
                    await ResetValue(default);
                }
                else
                {
                    var valueString = GetValue(Items.First());
                    if (TryParseValueFromString(valueString, out var value, out _))
                    {
                        await ResetValue(value);
                    }
                }
            }
        }
    }

    private async Task ResetValue(TValue? value)
    {
        if (Value == null && value == null || value != null && value.Equals(Value))
        {
            return;
        }
        await ValueChanged.InvokeAsync(value);
        SelectedChanged();
        SelectedItemChanged(value);
    }

    protected string? GetValue(TItem item) => OnGetValue?.Invoke(item);
        
    protected string? GetText(TItem item) => OnGetText?.Invoke(item);

    /// <inheritdoc />
    protected override bool TryParseValueFromString(string? value, [MaybeNullWhen(false)] out TValue result,
        [NotNullWhen(false)] out string? validationErrorMessage)
    {
        try
        {
            if (BindConverter.TryConvertTo<TValue>(value, CultureInfo.CurrentCulture, out var parsedValue))
            {
                result = parsedValue;
                validationErrorMessage = null;
                return true;
            }
            else
            {
                result = default;
                validationErrorMessage = $"The {DisplayName ?? FieldIdentifier.FieldName} field is not valid.";
                return false;
            }
        }
        catch (InvalidOperationException ex)
        {
            throw new InvalidOperationException($"{GetType()} does not support the type '{typeof(TValue)}'.", ex);
        }
    }
}