using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Jones.AspNetCore.Components.Extensions;
using Jones.AspNetCore.Components.Types;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.Components.Forms;

public class AttributeInputNumber<[DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.All)] TValue> : InputNumber<TValue>
{
    private static readonly string _stepAttributeValue = GetStepAttributeValue();

    private static string GetStepAttributeValue()
    {
        // Unwrap Nullable<T>, because InputBase already deals with the Nullable aspect
        // of it for us. We will only get asked to parse the T for nonempty inputs.
        var targetType = Nullable.GetUnderlyingType(typeof(TValue)) ?? typeof(TValue);
        if (targetType == typeof(int) ||
            targetType == typeof(long) ||
            targetType == typeof(short) ||
            targetType == typeof(float) ||
            targetType == typeof(double) ||
            targetType == typeof(decimal))
        {
            return "any";
        }
        else
        {
            throw new InvalidOperationException($"'{targetType}' 类型不是一个数字类型");
        }
    }

    [Parameter] 
    public InputUpdateTrigger UpdateTrigger { get; set; } = InputUpdateTrigger.OnChange;
    
    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(0, "input");
        builder.AddAttribute(1, "step", _stepAttributeValue);
        builder.AddMultipleAttributes(2, AdditionalAttributes);
        builder.AddAttribute(3, "type", "number");
        builder.AddAttributeIfNotNullOrEmpty(4, "class", CssClass);
        builder.AddAttribute(5, "value", BindConverter.FormatValue(CurrentValueAsString));
        builder.AddAttribute(6, UpdateTrigger.ToString().ToLower(), EventCallback.Factory.CreateBinder<string?>(this, __value => CurrentValueAsString = __value, CurrentValueAsString));
        // 自定义
        builder.AddAttributeIfNotNullOrEmpty(7, "placeholder", ValueExpression?.GetDisplayPrompt() ?? ValueExpression?.GetDisplayName());
        // 自定义
        builder.AddAttributeIfNotNullOrEmpty(8, "required", IsRequired ? "true" : null);
        builder.AddElementReferenceCapture(9, __inputReference => Element = __inputReference);
        builder.CloseElement();
    }

    public bool IsRequired => ValueExpression?.IsHasRequiredAttribute() == true;
    
    protected override void OnInitialized()
    {
        if (DisplayName == null)
        {
            DisplayName = ValueExpression?.GetDisplayName();
        }
        
        base.OnInitialized();
    }

    protected override void OnAfterRender(bool firstRender)
    {
        if (firstRender)
        {
            if (ParsingErrorMessage == "The {0} field must be a number.")
            {
                ParsingErrorMessage = "'{0}' 必须为数字";
            }
        }
        
        base.OnAfterRender(firstRender);
    }

    private bool _isHaveSet;
    protected override bool TryParseValueFromString(string? value, out TValue result, out string? validationErrorMessage)
    {
        _isHaveSet = true;
        return base.TryParseValueFromString(value, out result, out validationErrorMessage);
    }

    protected override string? FormatValueAsString(TValue? value)
    {
        if (!_isHaveSet && (value == null || value.Equals(default(TValue))))
        {
            return null;
        }
        return base.FormatValueAsString(value);
    }
}