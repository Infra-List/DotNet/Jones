using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Numerics;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Components.Forms;

public class AttributeInputIntMoney : AttributeInputNumber<int>
{
    private bool _isHaveSet;
    protected override bool TryParseValueFromString(string? value, [MaybeNullWhen(false)] out int result, [NotNullWhen(false)] out string? validationErrorMessage)
    {
        _isHaveSet = true;
        if (!BindConverter.TryConvertTo<decimal>(value, CultureInfo.InvariantCulture, out var money))
        {
            return base.TryParseValueFromString(value, out result, out validationErrorMessage);
        }

        result = Convert.ToInt32(money * 100);
        validationErrorMessage = null;
        return true;
    }

    protected override string? FormatValueAsString(int value)
    {
        if (!_isHaveSet && value == default)
        {
            return null;
        }
        
        return (value / 100m).ToString("0.##");
    }
}

public class AttributeInputIntMoneyNullable : AttributeInputNumber<int?>
{
    protected override bool TryParseValueFromString(string? value, [MaybeNullWhen(false)] out int? result, [NotNullWhen(false)] out string? validationErrorMessage)
    {
        if (string.IsNullOrEmpty(value))
        {
            result = null;
            validationErrorMessage = null;
            return true;
        }
        
        if (!BindConverter.TryConvertTo<decimal?>(value, CultureInfo.InvariantCulture, out var money))
        {
            return base.TryParseValueFromString(value, out result, out validationErrorMessage);
        }

        result = money == null ? null : Convert.ToInt32(money * 100);
        validationErrorMessage = null;
        return true;
    }

    protected override string? FormatValueAsString(int? value)
    {
        return (value / 100m)?.ToString("0.##");
    }
}