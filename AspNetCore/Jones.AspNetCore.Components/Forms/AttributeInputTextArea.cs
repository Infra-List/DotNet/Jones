using System.Diagnostics.CodeAnalysis;
using Jones.AspNetCore.Components.Extensions;
using Jones.AspNetCore.Components.Types;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.Components.Forms;

public class AttributeInputTextArea : InputTextArea
{
    [Parameter] 
    public InputUpdateTrigger UpdateTrigger { get; set; } = InputUpdateTrigger.OnInput;
    
    /// <summary>
    /// Gets or sets the associated <see cref="ElementReference"/>.
    /// <para>
    /// May be <see langword="null"/> if accessed before the component is rendered.
    /// </para>
    /// </summary>
    [DisallowNull] public ElementReference? Element { get; protected set; }

    /// <inheritdoc />
    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(0, "textarea");
        builder.AddMultipleAttributes(1, AdditionalAttributes);
        builder.AddAttributeIfNotNullOrEmpty(2, "class", CssClass);
        builder.AddAttribute(3, "value", BindConverter.FormatValue(CurrentValue));
        builder.AddAttribute(4, UpdateTrigger.ToString().ToLower(), EventCallback.Factory.CreateBinder<string?>(this, __value => CurrentValueAsString = __value, CurrentValueAsString));
        // 自定义
        builder.AddAttributeIfNotNullOrEmpty(5, "placeholder", ValueExpression?.GetDisplayPrompt() ?? ValueExpression?.GetDisplayName());
        // 自定义
        builder.AddAttributeIfNotNullOrEmpty(6, "required", ValueExpression?.IsHasRequiredAttribute() == true ? "true" : null);
        builder.AddElementReferenceCapture(7, __inputReference => Element = __inputReference);
        builder.CloseElement();
    }

    /// <inheritdoc />
    protected override bool TryParseValueFromString(string? value, out string? result, [NotNullWhen(false)] out string? validationErrorMessage)
    {
        result = value;
        validationErrorMessage = null;
        return true;
    }
}