using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace Jones.AspNetCore.Components.Forms;

// https://www.meziantou.net/upload-files-with-drag-drop-or-paste-from-clipboard-in-blazor.htm
public abstract class FileInputBase : JonesComponentBase
{
    [Parameter] public bool IsProcessing { get; set; }
        
    [Parameter] public string? Placeholder { get; set; }
        
    [Parameter] public bool IsAllowDrop { get; set; } = true;
        
    [Parameter] public bool IsAllowPaste { get; set; } = true;

    [Parameter] public string? Accept { get; set; }
        
    private bool? _isMultiple;
    [Parameter]
    public bool? IsMultiple {
        get
        {
            if (_isMultiple != null)
                return _isMultiple;
            return MaximumFileCount <= 1 ? null : true;
        }
        set => _isMultiple = value;
    }
        
    [Parameter] public long MaxFileSize { get; set; } = 10 * 1024 * 1024;

    private int? _maximumFileCount;
    [Parameter] 
    public int MaximumFileCount {
        get
        {
            if (_isMultiple == false)
            {
                return 1;
            }

            return _maximumFileCount ?? (_isMultiple == true ? 10 : 1);
        }
        set => _maximumFileCount = value;
    }

    [Parameter]
    public EventCallback<InputFileChangeEventArgs> OnChange { get; set; }
        
    [Parameter] public Func<string, Task>? OnError { get; set; }
        
    [Parameter] public bool? IsRequired { get; set; }

    protected IDictionary<string, object> GetInputFileDtoAdditionalAttributes()
    {
        var ret = new Dictionary<string, object>();

        if (!string.IsNullOrEmpty(Accept))
        {
            ret.Add("accept", Accept);
        }

        if (IsMultiple == true)
        {
            ret.Add("multiple", "multiple");
        }
        return ret;
    }
        
    protected async void OnFilesChange(InputFileChangeEventArgs eventArgs)
    {
        IsProcessing = true;
        try
        {
            foreach (var file in eventArgs.GetMultipleFiles(MaximumFileCount))
            {
                if (file.Size > MaxFileSize)
                {
                    OnError?.Invoke($"上传的文件不能超过{MaxFileSize / 1024 / 1024}M");
                    return;
                }
            }
            await OnChange.InvokeAsync(eventArgs);
        }
        finally
        {
            IsProcessing = false;
            StateHasChanged();
        }
    }
}

public record FileInputOption(bool IsAllowDrop, bool IsAllowPaste, string? Accept, bool? IsMultiple, long MaxFileSize, int MaximumFileCount);