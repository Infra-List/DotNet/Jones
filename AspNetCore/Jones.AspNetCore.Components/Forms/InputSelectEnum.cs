using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Jones.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.Components.Forms;

public class InputSelectEnum<TEnum> : InputBase<TEnum>
{
    [Parameter]
    public bool? IsRequired { get; set; }
        
    [Parameter]
    public Action? OnSelectedChanged { get; set; }
        
    [Parameter]
    public string? NullValueText { get; set; }

    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (OnSelectedChanged != null && ValueExpression != null)
        {
            var fieldName = FieldIdentifier.Create(ValueExpression).FieldName;
            EditContext.OnFieldChanged += (_, args) =>
            {
                if (args.FieldIdentifier.FieldName == fieldName)
                {
                    OnSelectedChanged.Invoke();
                }
            };
        }
    }

    // Generate html when the component is rendered.
    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(0, "select");
        builder.AddMultipleAttributes(1, AdditionalAttributes);
        builder.AddAttribute(2, "class", CssClass);
        builder.AddAttribute(3, "value", BindConverter.FormatValue(CurrentValueAsString));
        builder.AddAttribute(4, "onchange", EventCallback.Factory.CreateBinder<string?>(this, value => CurrentValueAsString = value, CurrentValueAsString));

        // Add an option element per enum value
        var enumType = GetEnumType();
        var isHasAll = IsRequired != true && ValueExpression?.IsHasRequiredAttribute() != true;
        if (isHasAll)
        {
            builder.OpenElement(5, "option");
            builder.AddAttribute(6, "value", "");
            builder.AddContent(7, NullValueText ?? $"全部{ValueExpression?.GetDisplayName() ?? enumType.GetDescription()}");
            builder.CloseElement();
        }
        foreach (Enum value in Enum.GetValues(enumType))
        {
            builder.OpenElement(5, "option");
            builder.AddAttribute(6, "value", value.ToString());
            builder.AddContent(7, value.GetDisplayName() ?? value.ToString());
            builder.CloseElement();
        }

        builder.CloseElement(); // close the select element
    }

    protected override bool TryParseValueFromString(string? value, [MaybeNullWhen(false)] out TEnum result, [NotNullWhen(false)] out string? validationErrorMessage)
    {
        // Let's Blazor convert the value for us 😊
        if (BindConverter.TryConvertTo(value, CultureInfo.CurrentCulture, out TEnum? parsedValue))
        {
            result = parsedValue;
            validationErrorMessage = null;
            return true;
        }

        // Map null/empty value to null if the bound object is nullable
        if (string.IsNullOrEmpty(value))
        {
            var nullableType = Nullable.GetUnderlyingType(typeof(TEnum));
            if (nullableType != null)
            {
                result = default;
                validationErrorMessage = null;
                return true;
            }
        }

        // The value is invalid => set the error message
        result = default;
        validationErrorMessage = $"The {FieldIdentifier.FieldName} field is not valid.";
        return false;
    }

    // Get the actual enum type. It unwrap Nullable<T> if needed
    // MyEnum  => MyEnum
    // MyEnum? => MyEnum
    private Type GetEnumType()
    {
        var nullableType = Nullable.GetUnderlyingType(typeof(TEnum));
        if (nullableType != null)
            return nullableType;

        return typeof(TEnum);
    }
}