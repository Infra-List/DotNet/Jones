window.tencentMap = {
    version: "1.0.0"
};
window.tencentMap.locationPicker = {
    version: "1.0.0",
    init: function (dotNetObject, method) {
        window.addEventListener('message', function(event) {
            // 接收位置信息，用户选择确认位置点后选点组件会触发该事件，回传用户的位置信息
            const loc = event.data;
            if (loc && loc.module === 'locationPicker') {//防止其他应用也会向该页面post信息，需判断module是否为'locationPicker'
                dotNetObject.invokeMethodAsync(method, loc);
            }
        }, false);
    }
};