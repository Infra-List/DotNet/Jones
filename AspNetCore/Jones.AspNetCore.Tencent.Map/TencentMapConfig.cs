using System;

namespace Jones.AspNetCore.Tencent.Map;

public record TencentMapConfig
{
    public string Referer { get; set; }
    public string Key { get; set; }
    public string? SecretKey { get; set; }

    public virtual void Validate()
    {
        if (string.IsNullOrEmpty(Key))
            throw new ArgumentNullException(nameof(Key));
    }
}