using System;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.AspNetCore.Tencent.Map;

public static class ServiceCollectionExtensions
{
        
    public static IServiceCollection AddTencentMap(this IServiceCollection services, Action<TencentMapConfig> setupAction)
    {
        if (services == null)
        {
            throw new ArgumentNullException(nameof(services));
        }
            
        services.Configure(setupAction);
            
        return services;
    }
}