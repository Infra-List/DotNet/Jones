namespace Jones.AspNetCore.Tencent.Map.Components.LocationPicker;

public record LocationPickerResult
{
    public string Module { get; set; }
    public Location Latlng { get; set; }
    public string Poiaddress { get; set; }
    public string Poiname { get; set; }
    public string Cityname { get; set; }
        
    public class Location
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}