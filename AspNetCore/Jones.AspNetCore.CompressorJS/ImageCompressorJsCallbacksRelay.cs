using System;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.CompressorJS;

public class ImageCompressorJsCallbacksRelay : IDisposable
{
    public IDisposable DotNetReference { get; }

    private readonly Action<string> _onError;

    private readonly Action<byte[], string, long> _onSuccess;

    public ImageCompressorJsCallbacksRelay(Action<string> onError, Action<byte[], string, long> onSuccess)
    {
        _onError = onError;
        _onSuccess = onSuccess;
            
        DotNetReference = DotNetObjectReference.Create(this);
    }

    [JSInvokable]
    public void Error(string message) => _onError.Invoke(message);

    [JSInvokable]
    public void Success(byte[] content, string contentType, long size) => _onSuccess.Invoke(content, contentType, size);

    public void Dispose()
    {
        DotNetReference.Dispose();
    }
}