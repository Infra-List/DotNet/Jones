using System;
using System.Threading.Tasks;
using Jones.AspNetCore.Services;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.CompressorJS;

// https://github.com/fengyuanchen/compressorjs
// https://fengyuanchen.github.io/compressorjs/
// https://github.com/Chronostasys/Blazor.Cropper/blob/master/Blazor.Cropper/wwwroot/CropHelper.js
public class CompressorJSImageCompressorImpl : IImageCompressor
{
    private readonly ILogger<CompressorJSImageCompressorImpl> _logger;
    private readonly IJSRuntime _jsRuntime;

    public CompressorJSImageCompressorImpl(ILogger<CompressorJSImageCompressorImpl> logger, IJSRuntime jsRuntime)
    {
        _logger = logger;
        _jsRuntime = jsRuntime;
    }

    public async Task<ImageCompressorResult> Compress(byte[] image, string contentType, ImageCompressorOption option)
    {
        const string messagePrefix = "压缩图片";
        const string errorMsg = messagePrefix + "失败";
            
        var returnTask = new TaskCompletionSource<ImageCompressorResult>();
        var jsCallbacksRelay = new ImageCompressorJsCallbacksRelay(
            message => returnTask.SetResult(new ImageCompressorResult(false, Message: message)),
            (content, type, size) => returnTask.SetResult(new ImageCompressorResult(true, new ImageAfterCompression(content, type, size))));
        try
        {
            await _jsRuntime.InvokeVoidAsync("window.compressorjs.compress", image, contentType, option, jsCallbacksRelay.DotNetReference);
            return await returnTask.Task;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "{MessagePrefix} - [{ErrorMsg}] - 错误信息[{Message}]", messagePrefix, errorMsg, e.Message);
            return new ImageCompressorResult(false, Message: errorMsg);
        }
        finally
        {
            jsCallbacksRelay.Dispose();
        }
    }
}