function getCompressorJSOption(option, dotNetObject) {
    let compressorjsOption = {};

    if (option.quality) {
        compressorjsOption.quality = option.quality;
    }
    
    if (option.maxWidth) {
        compressorjsOption.maxWidth = option.maxWidth;
    }

    if (option.maxHeight) {
        compressorjsOption.maxHeight = option.maxHeight;
    }

    if (option.minWidth) {
        compressorjsOption.minWidth = option.minWidth;
    }

    if (option.minHeight) {
        compressorjsOption.minHeight = option.minHeight;
    }

    if (option.width) {
        compressorjsOption.width = option.width;
    }

    if (option.height) {
        compressorjsOption.height = option.height;
    }

    compressorjsOption.success = function (result) {
        result.arrayBuffer().then(buffer => dotNetObject.invokeMethodAsync("Success", new Uint8Array(buffer), result.type, result.size));
    }

    compressorjsOption.error = function (err) {
        console.error(err);
        dotNetObject.invokeMethodAsync("Error", err.message);
    }
    
    return compressorjsOption;
}

window.compressorjs = {
    version: "1.0.0",
    compress: function (image, contentType, option, dotNetObject) {
        return new Compressor(new Blob([image], { type: contentType }), getCompressorJSOption(option, dotNetObject));
    }
};