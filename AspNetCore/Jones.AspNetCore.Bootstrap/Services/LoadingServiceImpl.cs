using System.Threading.Tasks;
using Jones.AspNetCore.Services;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.Bootstrap.Services;

public class LoadingServiceImpl : ILoadingService
{
    private readonly IJSRuntime _jsRuntime;

    public LoadingServiceImpl(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }
    
    public async Task<IJSObjectReference> ShowLoadingModal(bool isProgress = false)
    {
        return await _jsRuntime.InvokeAsync<IJSObjectReference>("jonesBootstrap.modal.showLoadingModal", isProgress);
    }

    public async Task SetLoadingModalProgress(IJSObjectReference loadingModal, float percent, string? message)
    {
        await _jsRuntime.InvokeVoidAsync("jonesBootstrap.modal.setLoadingModalProgress", loadingModal, percent, message);
    }

    public async Task CloseLoadingModal(IJSObjectReference loadingModal)
    {
        await _jsRuntime.InvokeVoidAsync("jonesBootstrap.modal.closeLoadingModal", loadingModal);
    }
}