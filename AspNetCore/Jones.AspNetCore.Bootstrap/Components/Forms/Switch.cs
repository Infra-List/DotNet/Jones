using Jones.AspNetCore.Helper;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;

namespace Jones.AspNetCore.Bootstrap.Components.Forms;

public class Switch : InputCheckbox
{
    private string? _id;
    [Parameter]
    public string? Id 
    {
        get
        {
            _id ??=
                (ValueExpression == null 
                    ? null 
                    : $"{FieldIdentifier.Create(ValueExpression).FieldName}_{IdHelper.GetId()}");
            return _id;
        }
        set => _id = value;
    }

    [Parameter] 
    public string? OnText { get; set; } = "是";
        
    [Parameter] 
    public string? OffText { get; set; } = "否";

    [Parameter] 
    public bool IsShowText { get; set; } = true;

// <div class="form-check form-switch">
//   <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
//   <label class="form-check-label" for="flexSwitchCheckDefault">Default switch checkbox input</label>
// </div>
    
    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        builder.OpenElement(0, "div");
        builder.AddMultipleAttributes(1, AdditionalAttributes);
        builder.AddAttribute(2, "class", "form-check form-switch fs-4");
            
        builder.OpenElement(3, "input");
        builder.AddAttribute(4, "id", Id);
        builder.AddAttribute(5, "type", "checkbox");
        builder.AddAttribute(6, "class", "form-check-input mt-2");
        builder.AddAttribute(7, "checked", BindConverter.FormatValue(CurrentValue));
        builder.AddAttribute(8, "onchange", EventCallback.Factory.CreateBinder<bool>(this, value => CurrentValue = value, CurrentValue));
        builder.CloseElement();

        var currentValueText = CurrentValue ? OnText : OffText;
        if (IsShowText && currentValueText != null)
        {
            builder.OpenElement(9, "label");
            builder.AddAttribute(10, "for", Id);
            builder.AddAttribute(11, "class", "form-check-label fs-6");
            builder.AddContent(12, currentValueText);
            builder.CloseElement();
        }
            
        builder.CloseElement();
    }
}