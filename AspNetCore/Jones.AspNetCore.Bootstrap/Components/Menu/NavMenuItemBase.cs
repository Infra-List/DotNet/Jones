using System;
using System.Linq;
using Jones.AspNetCore.Components;
using Jones.AspNetCore.Components.Extensions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;

namespace Jones.AspNetCore.Bootstrap.Components.Menu;

public class NavMenuItemBase : JonesComponentBase, IDisposable
{
    [Parameter]
    public MenuItem MenuItem { get; set; }

    [Inject] 
    protected NavigationManager NavigationManger { get; set; } = default!;

    protected bool IsHasSubMenu => MenuItem.Items?.Any() ?? false;

    protected bool IsMatch { get; set; }

    protected bool IsSubMenuActive { get; set; }
    
    protected override void OnInitialized()
    {
        // We'll consider re-rendering on each location change
        NavigationManger.LocationChanged += OnLocationChanged;
    }
        
    protected override void OnParametersSet()
    {
        IsMatch = CheckIsMatch(NavigationManger.Uri);
        IsSubMenuActive = CheckIsSubMenuActive(NavigationManger.Uri);
    }
        
    public void Dispose()
    {
        // To avoid leaking memory, it's important to detach any event handlers in Dispose()
        NavigationManger.LocationChanged -= OnLocationChanged;
    }

    private void OnLocationChanged(object? sender, LocationChangedEventArgs args)
    {
        var isChanged = false;
        
        var isMatch = CheckIsMatch(args.Location);
        if (IsMatch != isMatch)
        {
            IsMatch = isMatch;
            isChanged = true;
        }
        
        var isSubMenuActive = CheckIsSubMenuActive(args.Location);
        if (IsSubMenuActive != isSubMenuActive)
        {
            IsSubMenuActive = isSubMenuActive;
            isChanged = true;
        }

        if (isChanged)
        {
            StateHasChanged();
        }
    }

    private bool CheckIsMatch(string currentUriAbsolute) => 
        MenuItem.Url != null && NavigationManger.ToAbsoluteUri(MenuItem.Url).IsMatch(currentUriAbsolute, MenuItem.Match);

    private bool CheckIsSubMenuActive(string currentUriAbsolute) =>
        MenuItem.Items?.Any(p => p.Url != null && NavigationManger.ToAbsoluteUri(p.Url).IsMatch(currentUriAbsolute, p.Match)) ?? false;
}