using System.Collections.Generic;
using Microsoft.AspNetCore.Components.Routing;

namespace Jones.AspNetCore.Bootstrap.Components.Menu;

public record MenuItem
{
    public IEnumerable<MenuItem>? Items { get; set; }
        
    /// <summary>
    /// 获得/设置 图标字符串
    /// </summary>
    public string? Icon { get; set; }
        
    /// <summary>
    /// 获得/设置 导航菜单文本内容
    /// </summary>
    public string? Text { get; set; }

    /// <summary>
    /// 获得/设置 导航菜单链接地址
    /// </summary>
    public string? Url { get; set; }
        
    /// <summary>
    /// 获得/设置 匹配方式 默认 NavLinkMatch.Prefix
    /// </summary>
    public NavLinkMatch Match { get; set; }
        
    public string? Roles { get; set; }
        
    public MenuItem(string text, string? url = null, string? icon = null, string? roles = null)
    {
        Text = text;
        Url = url;
        Icon = icon;
        Roles = roles;
    }
}