using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Bootstrap.Components.Description;

public interface IDescriptionItem
{
    [Parameter]
    ElementReference Ref { get; set; }

    [Parameter]
    string? Title { get; set; }

    [Parameter]
    RenderFragment? TitleTemplate { get; set; }

    [Parameter]
    int? Span { get; set; }

    [Parameter]
    RenderFragment? ChildContent { get; set; }

    string? GetTitle();
    string? GetText();
}