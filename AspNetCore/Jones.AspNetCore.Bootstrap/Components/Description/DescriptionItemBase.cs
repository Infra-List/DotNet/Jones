using System;
using Jones.AspNetCore.Components;
using Microsoft.AspNetCore.Components;

namespace Jones.AspNetCore.Bootstrap.Components.Description;

public class DescriptionItemBase<TField> : DisplayComponentBase<TField>, IDescriptionItem, IDisposable
{
    [CascadingParameter]
    public IDescription? Description { get; set; }
        
    public ElementReference Ref { get; set; }
        
    [Parameter]
    public int? Span { get; set; } = 1;

    [Parameter]
    public RenderFragment? ChildContent { get; set; }
        
    void IDisposable.Dispose()
    {
        Description?.Items.Remove(this);
        GC.SuppressFinalize(this);
    }

    protected override void OnInitialized()
    {
        Description?.Items.Add(this);
        base.OnInitialized();
    }
}