using System.Collections.Generic;

namespace Jones.AspNetCore.Bootstrap.Components.Description;

public interface IDescription
{
    IList<IDescriptionItem> Items { get; }
}