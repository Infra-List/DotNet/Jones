jonesBootstrap.backTop = {
    version: "1.0.0",
    init: function (sender) {
        window.onscroll = function () {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                sender.style.display = "block";
            } else {
                sender.style.display = "none";
            }
        };
        sender.addEventListener("click", function () {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        });
    }
};