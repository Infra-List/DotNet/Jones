using System.IO;
using Jones.AspNetCore.Models;
using Jones.AspNetCore.Utils;

namespace Jones.AspNetCore.Bootstrap.Extensions;

public static class FileViewModelExtensions
{
    public static string? PreviewFromBootstrapIcons(this FileViewModel file, bool isEarmark = true, bool isFill = false)
    {
        var builder = CssBuilder.Default("bi");
        var fileExtension = Path.GetExtension(file.Name).ToLowerInvariant();
        var icon = fileExtension switch
        {
            ".csv" or ".xls" or ".xlsx" => BootstrapIconsFileClass("excel", isEarmark, isFill),
            ".doc" or ".docx" or ".dot" or ".dotx" => BootstrapIconsFileClass("word", isEarmark, isFill),
            ".ppt" or ".pptx" => BootstrapIconsFileClass("ppt", isEarmark, isFill),
            ".wav" or ".mp3" => BootstrapIconsFileClass("music", isEarmark, isFill),
            ".mp4" or ".mov" or ".mkv" => BootstrapIconsFileClass("play", isEarmark, isFill),
            ".cs" or ".html" or ".vb" => BootstrapIconsFileClass("code", isEarmark, isFill),
            ".pdf" => BootstrapIconsFileClass("pdf", isEarmark, isFill),
            ".zip" or ".rar" or ".iso" => BootstrapIconsFileClass("zip", isEarmark, isFill),
            ".txt" or ".log" or ".iso" => BootstrapIconsFileClass("text", isEarmark, isFill),
            ".jpg" or ".jpeg" or ".png" or ".bmp" or ".gif" => BootstrapIconsFileClass("image", isEarmark, isFill),
            _ => BootstrapIconsFileClass(null, isEarmark, isFill)
        };
        builder.AddClass(icon);
        builder.AddClass("fa-2x");
        return builder.Build();
    }

    private static string BootstrapIconsFileClass(string? baseCss, bool isEarmark, bool isFill) => 
        $"bi-file{BootstrapIconsEarmarkClass(isEarmark)}{BootstrapIconsBaseClass(baseCss)}{BootstrapIconsFillClass(isFill)}";
        
    private static string BootstrapIconsBaseClass(string? baseCss) => baseCss != null ? $"-{baseCss}" : string.Empty;
    private static string BootstrapIconsEarmarkClass(bool isEarmark) => isEarmark ? "-earmark" : string.Empty;
    private static string BootstrapIconsFillClass(bool isFill) => isFill ? "-fill" : string.Empty;
}