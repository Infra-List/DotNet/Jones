using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Bootstrap.Types;

public enum Size
{
    /// <summary>
    /// xs 超小设置 576px
    /// </summary>
    [Display(Name = "xs")]
    ExtraSmall,

    /// <summary>
    /// sm 小设置 576px
    /// </summary>
    [Display(Name = "sm")]
    Small,

    /// <summary>
    /// md 中等设置 768px
    /// </summary>
    [Display(Name = "md")]
    Medium,

    /// <summary>
    /// lg 大设置 992px
    /// </summary>
    [Display(Name = "lg")]
    Large,

    /// <summary>
    /// xl 超大设置 1200px
    /// </summary>
    [Display(Name = "xl")]
    ExtraLarge
}