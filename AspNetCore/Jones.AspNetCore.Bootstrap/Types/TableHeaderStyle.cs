﻿using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Bootstrap.Types;

/// <summary>
/// 表格 thead 样式枚举
/// </summary>
public enum TableHeaderStyle
{
    /// <summary>
    /// 浅色
    /// </summary>
    [Display(Name = "table-light")]
    Light,

    /// <summary>
    /// 深色
    /// </summary>
    [Display(Name = "table-dark")]
    Dark
}