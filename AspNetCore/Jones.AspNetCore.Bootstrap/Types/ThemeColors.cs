using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Bootstrap.Types;

public enum ThemeColor
{
    /// <summary>
    /// primary
    /// </summary>
    [Display(Name = "primary")]
    Primary,

    /// <summary>
    /// secondary
    /// </summary>
    [Display(Name = "secondary")]
    Secondary,

    /// <summary>
    /// success
    /// </summary>
    [Display(Name = "success")]
    Success,

    /// <summary>
    /// danger
    /// </summary>
    [Display(Name = "danger")]
    Danger,

    /// <summary>
    /// warning
    /// </summary>
    [Display(Name = "warning")]
    Warning,

    /// <summary>
    /// info
    /// </summary>
    [Display(Name = "info")]
    Info,

    /// <summary>
    /// light
    /// </summary>
    [Display(Name = "light")]
    Light,

    /// <summary>
    /// dark
    /// </summary>
    [Display(Name = "dark")]
    Dark,
}