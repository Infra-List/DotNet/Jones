using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jones.AspNetCore.Bootstrap.Types;

[Description("内容垂直对齐")]
public enum VerticalAlign
{
    [Display(Name = "top")]
    Top,
    [Display(Name = "middle")]
    Middle,
    [Display(Name = "bottom")]
    Bottom
}