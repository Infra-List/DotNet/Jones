window.jonesBootstrap = {
    version: "1.0.0",
    sdkVersion: function () {
        return bootstrap.Tooltip.VERSION;
    }
};
jonesBootstrap.backTop = {
    version: "1.0.0",
    init: function (sender) {
        window.onscroll = function () {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                sender.style.display = "block";
            } else {
                sender.style.display = "none";
            }
        };
        sender.addEventListener("click", function () {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        });
    }
};
jonesBootstrap.fileInput = {
    version: "1.0.0",
    init: function (option, dropZoneElement, inputFileElement) {

        const hoverClass = "input-file-container-focus";
        // Add a class when the user drags a file over the drop zone
        function onDragHover(e) {
            e.preventDefault();
            dropZoneElement.classList.add(hoverClass);
        }

        function onDragLeave(e) {
            e.preventDefault();
            dropZoneElement.classList.remove(hoverClass);
        }

        // Handle the paste and drop events
        function onDrop(e) {
            e.preventDefault();
            dropZoneElement.classList.remove(hoverClass);

            // Set the files property of the input element and raise the change event
            inputFileElement.files = e.dataTransfer.files;
            const event = new Event('change', { bubbles: true });
            inputFileElement.dispatchEvent(event);
        }

        function onPaste(e) {
            // Set the files property of the input element and raise the change event
            inputFileElement.files = e.clipboardData.files;
            const event = new Event('change', { bubbles: true });
            inputFileElement.dispatchEvent(event);
        }

        // Register all events
        if (option.isAllowDrop) {
            dropZoneElement.addEventListener("dragenter", onDragHover);
            dropZoneElement.addEventListener("dragover", onDragHover);
            dropZoneElement.addEventListener("dragleave", onDragLeave);
            dropZoneElement.addEventListener("drop", onDrop);
        }

        if (option.isAllowPaste) {
            dropZoneElement.addEventListener('paste', onPaste);
        }

        // The returned object allows to unregister the events when the Blazor component is destroyed
        return {
            dispose: () => {
                if (option.isAllowDrop) {
                    dropZoneElement.removeEventListener('dragenter', onDragHover);
                    dropZoneElement.removeEventListener('dragover', onDragHover);
                    dropZoneElement.removeEventListener('dragleave', onDragLeave);
                    dropZoneElement.removeEventListener("drop", onDrop);
                }

                if (option.isAllowPaste) {
                    dropZoneElement.removeEventListener('paste', onPaste);
                }
            }
        }
    }
};
jonesBootstrap.modal = {
    version: "1.0.0",
    getModal : (sender) => {
        const modal = bootstrap.Modal.getInstance(sender);
        if (modal == null) {
            return new bootstrap.Modal(sender);
        } else {
            return modal;
        }
    },
    toggle: function(sender){
        this.getModal(sender).toggle();
    },
    show: function(sender){
        this.getModal(sender).show();
    },
    hide: function(sender){
        this.getModal(sender).hide();
    },
    handleUpdate: function(sender){
        this.getModal(sender).handleUpdate();
    },
    dispose: function(sender){
        $(sender).remove();
        // this.getModal(sender).dispose();
    },
    onShow: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('shown.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onShown: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('shown.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHide: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hide.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHidden: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hidden.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    onHidePrevented: (sender, dotNetObject, method) => {
        const $sender = $(sender);
        $sender.on('hidePrevented.bs.modal', function (event) {
            dotNetObject.invokeMethodAsync(method);
        });
    },
    showLoadingModal: (isProgress) => {
        let modalLoading = $('<div class="modal fade mt-5" data-bs-backdrop="static" tabindex="-1" aria-hidden="true">' +
            '<div class="modal-dialog">' +
            '<div class="progress active">' +
            '<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:100%"></div></div></div></div>');
        if (isProgress) {
            $(modalLoading).find(".progress-bar").css("width", "0");
        } else {
            $(modalLoading).find(".progress-bar").css("width", "100%");
        }
        new bootstrap.Modal(modalLoading).show();
        return modalLoading;
    },
    setLoadingModalProgress: (loadingModal, percent, message) => {
        $(loadingModal).find(".progress-bar").html(message);
        $(loadingModal).find(".progress-bar").css("width", percent + "%");
        // bootstrap.Modal.getInstance(loadingModal).handleUpdate();
    },
    closeLoadingModal: (loadingModal) => {
        // bootstrap.Modal.getInstance(loadingModal).hide();
        // https://github.com/twbs/bootstrap/issues/25008
        const int = setInterval(function () {
            if ($(loadingModal).is(".show"))
                bootstrap.Modal.getInstance(loadingModal).hide();
            else
                clearInterval(int);
        }.bind(this), 100);
    },
};