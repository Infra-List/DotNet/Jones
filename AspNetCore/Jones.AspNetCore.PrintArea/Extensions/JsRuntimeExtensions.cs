using System.Reflection;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace Jones.AspNetCore.PrintArea.Extensions;

// https://github.com/baim216/printArea
public static class JsRuntimeExtensions
{
    private const string JavaScriptLib = "./_content/Jones.AspNetCore.PrintArea/js/printarea.extensions.js";
    
    public static async ValueTask PrintArea(this IJSRuntime jsRuntime, ElementReference element, PrintAreaOption? option)
    {
        var module = await jsRuntime.InvokeAsync<IJSObjectReference>("import", $"{JavaScriptLib}?v={Assembly.GetExecutingAssembly().GetName().Version}");
        await module.InvokeVoidAsync("printArea", element, option);
    }
}