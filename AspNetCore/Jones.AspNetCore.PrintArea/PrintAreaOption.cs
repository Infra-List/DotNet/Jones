namespace Jones.AspNetCore.PrintArea;

public class PrintAreaOption
{
    public string? Mode { get; set; }
    public string? PopTitle { get; set; }
    public bool? PopClose { get; set; }
    public string? ExtraCss { get; set; }
    public string? ExtraHead { get; set; }
}