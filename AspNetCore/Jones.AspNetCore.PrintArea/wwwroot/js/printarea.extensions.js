import '/_content/Jones.AspNetCore.PrintArea/js/printarea.js';

function getPrintAreaOptions(option) {
    let printAreaOption = {};
    if (option) {
        if (option.mode) {
            printAreaOption.mode = option.mode;
        }
        if (option.popTitle) {
            printAreaOption.popTitle = option.popTitle;
        }
        if (option.popClose !== undefined) {
            printAreaOption.popClose = option.popClose;
        }
        if (option.extraCss) {
            printAreaOption.extraCss = option.extraCss;
        }
        if (option.extraHead) {
            printAreaOption.extraHead = option.extraHead;
        }
    }
    return printAreaOption;
}

export function printArea(element, option) {
    new PrintArea(element, getPrintAreaOptions(option)).print();
}