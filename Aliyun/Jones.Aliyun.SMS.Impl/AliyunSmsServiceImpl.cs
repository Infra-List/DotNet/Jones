using System.Text.Encodings.Web;
using System.Text.Json;
using AlibabaCloud.OpenApiClient.Models;
using AlibabaCloud.SDK.Dysmsapi20170525;
using AlibabaCloud.SDK.Dysmsapi20170525.Models;
using Jones.Aliyun.SMS.Dto;
using Jones.Aliyun.SMS.Entities;
using Jones.Aliyun.SMS.Impl.Entities;
using Jones.Aliyun.SMS.Types;
using Jones.Extensions;
using Jones.JsonConverters;

namespace Jones.Aliyun.SMS.Impl;

public class AliyunSmsServiceImpl : IAliyunSmsService
{
    private static readonly JsonSerializerOptions JsonOptions = new()
    {
        PropertyNameCaseInsensitive = true,    //忽略大小写
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,    // 驼峰式
        Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,    // 序列化中文时的编码问题
        Converters =
        {
            new DateTimeConverter()
        }
    };

    private static bool IsSendSmsSuccess(string code) => code == SendSmsResultCode.Success;
    
    private static Client CreateClient(SmsClientInfo smsClientInfo)
    {
        return new Client(new Config
        {
            AccessKeyId = smsClientInfo.AccessKeyId,
            AccessKeySecret = smsClientInfo.AccessKeySecret,
            Endpoint = smsClientInfo.Endpoint
        });
    }
    
    // https://help.aliyun.com/document_detail/419273.htm?spm=a2c4g.11186623.0.0.34376cf0Mis8Mz
    public async Task<SendSmsResult> SendSms(SmsClientInfo smsClientInfo, string phoneNumbers, string signName, string templateCode, string? templateParam, string? orderId)
    {
        var client = CreateClient(smsClientInfo);
        var response = await client.SendSmsAsync(new SendSmsRequest
        {
            PhoneNumbers = phoneNumbers,
            SignName = signName,
            TemplateCode = templateCode,
            TemplateParam = templateParam,
            OutId = orderId
        });
        var result = response.Body;
        return new SendSmsResult(IsSendSmsSuccess(result.Code), result.Code, result.Message, result.BizId, result.RequestId);
    }
    
    // https://help.aliyun.com/document_detail/419274.html
    public async Task<SendSmsResult> SendBatchSms(SmsClientInfo smsClientInfo, string[] phoneNumbers, string[] signCodes, string templateCode, string? templateParam)
    {
        var client = CreateClient(smsClientInfo);
        var response = await client.SendBatchSmsAsync(new SendBatchSmsRequest
        {
            PhoneNumberJson = JsonSerializer.Serialize(phoneNumbers, JsonOptions),
            SignNameJson = JsonSerializer.Serialize(signCodes, JsonOptions),
            TemplateCode = templateCode,
            TemplateParamJson = templateParam
        });
        var result = response.Body;
        return new SendSmsResult(IsSendSmsSuccess(result.Code), result.Code, result.Message, result.BizId, result.RequestId);
    }

    public async Task<QuerySendDetailsResult?> QuerySendDetails(SmsClientInfo smsClientInfo, string bizId, string phoneNumber, string outId, DateTime sendTime)
    {
        var client = CreateClient(smsClientInfo);
        var response = await client.QuerySendDetailsAsync(new QuerySendDetailsRequest
        {
            BizId = bizId,
            PhoneNumber = phoneNumber,
            SendDate = sendTime.ToString("yyyyMMdd"),
            PageSize = 10,
            CurrentPage = 1
        });
        var result = response.Body;
        var sms = result.SmsSendDetailDTOs.SmsSendDetailDTO.FirstOrDefault(p =>
            p.OutId == outId && p.PhoneNum == phoneNumber);
        if (sms?.SendStatus == null)
        {
            return null;
        }

        var size = result.TotalCount.ToSafeInt();
        return new QuerySendDetailsResult
        {
            SendStatus = Convert.ToInt32(sms.SendStatus),
            Size = size == null 
                ? null 
                : result.SmsSendDetailDTOs.SmsSendDetailDTO.Count == 1 
                    ? size
                    : 1,
            ErrCode = sms.ErrCode,
            TemplateCode = sms.TemplateCode,
            Content = sms.Content,
            ReceiveTime = ToDateTime(sms.ReceiveDate),
            SendTime = ToDateTime(sms.SendDate)
        };
    }

    // https://help.aliyun.com/document_detail/101867.html?spm=5176.25163407.help.dexternal.7058bb6ehGKfWW
    public Task<SmsReportResult[]?> SmsReport(string content)
    {
        var smsReportList = JsonSerializer.Deserialize<SmsReport[]>(content, JsonOptions);
        return Task.FromResult(smsReportList?
            .Select(p => 
                new SmsReportResult(p.phone_number, p.biz_id, p.out_id, p.success, p.err_code, 
                    p.err_msg, p.sms_size?.ToSafeInt(), p.send_time.ToUniversalTime(), p.report_time.ToUniversalTime()))
            .ToArray());
    }

    private static DateTime? ToDateTime(string? source) => string.IsNullOrEmpty(source)
        ? null
        : DateTime.ParseExact(source, "yyyy-MM-dd HH:mm:ss", null).ToUniversalTime();
}