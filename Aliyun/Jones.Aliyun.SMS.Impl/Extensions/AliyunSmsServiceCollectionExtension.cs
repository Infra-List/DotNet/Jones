using Microsoft.Extensions.DependencyInjection;

namespace Jones.Aliyun.SMS.Impl.Extensions;

public static class AliyunSmsServiceCollectionExtension
{
    public static IServiceCollection AddAliyunSmsService(this IServiceCollection services)
    {
        services.AddTransient<IAliyunSmsService, AliyunSmsServiceImpl>();
        return services;
    }
}