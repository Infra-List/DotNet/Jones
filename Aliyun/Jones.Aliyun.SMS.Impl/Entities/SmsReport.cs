namespace Jones.Aliyun.SMS.Impl.Entities;

public record SmsReport
{
    public string phone_number { get; set; }
    public DateTime send_time { get;  set;}
    public DateTime report_time { get; set; }
    public bool success { get; set; }
    public string err_code { get; set; }
    public string? err_msg { get; set; }
    public string? sms_size { get; set; }
    public string biz_id { get; set; }
    public string? out_id { get; set; }
}