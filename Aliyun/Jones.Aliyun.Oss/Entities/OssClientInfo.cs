using System.ComponentModel.DataAnnotations;

namespace Jones.Aliyun.Oss.Entities;

// https://www.alibabacloud.com/help/zh/object-storage-service/latest/description

public record OssClientInfo
{
    [Display(Name = "访问域名", Prompt = "Endpoint 表示OSS对外服务的访问域名。OSS以HTTP RESTful API的形式对外提供服务，当访问不同地域的时候，需要不同的域名。通过内网和外网访问同一个地域所需要的域名也是不同的。")]
    public string Endpoint { get; }
    
    public string AccessKeyId { get; }
    
    public string AccessKeySecret { get; }

    public OssClientInfo(string endpoint, string accessKeyId, string accessKeySecret)
    {
        Endpoint = endpoint;
        AccessKeyId = accessKeyId;
        AccessKeySecret = accessKeySecret;
    }
}