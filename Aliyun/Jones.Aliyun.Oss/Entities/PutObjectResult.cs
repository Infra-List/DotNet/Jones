using System.ComponentModel.DataAnnotations;

namespace Jones.Aliyun.Oss.Entities;

// https://www.alibabacloud.com/help/zh/object-storage-service/latest/putobject

public record PutObjectResult
{
    [Display(Name = "ETag", Prompt = "Object生成时会创建相应的ETag ，ETag用于标识一个Object的内容。")]
    public string ETag { get; }
    public string VersionId { get; }
    public string ObjectName { get; }

    public PutObjectResult(string eTag, string versionId, string objectName)
    {
        ETag = eTag;
        VersionId = versionId;
        ObjectName = objectName;
    }
}