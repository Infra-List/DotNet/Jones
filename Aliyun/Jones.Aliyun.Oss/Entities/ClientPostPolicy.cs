namespace Jones.Aliyun.Oss.Entities;

public record ClientPostPolicy(string Folder, string OssAccessKeyId, string Policy, string Signature);