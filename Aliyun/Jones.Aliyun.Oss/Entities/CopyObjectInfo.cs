namespace Jones.Aliyun.Oss.Entities;

public record CopyObjectInfo(string SourceBucketName, string SourceObjectName, string DestBucketName, string DestObjectName);