namespace Jones.Aliyun.Oss.Entities;

public record CopyObjectResult
{
    public DateTime LastModified { get; }

    public string ETag { get; }

    public string VersionId { get; }

    public string CopySourceVersionId { get; }

    public CopyObjectResult(DateTime lastModified, string eTag, string versionId, string copySourceVersionId)
    {
        LastModified = lastModified;
        ETag = eTag;
        VersionId = versionId;
        CopySourceVersionId = copySourceVersionId;
    }
}