using System.ComponentModel.DataAnnotations;

namespace Jones.Aliyun.Oss.Entities;

// https://www.alibabacloud.com/help/zh/object-storage-service/latest/get-bucket-stat

[Display(Name = "Bucket统计", Prompt = "调用GetBucketStat接口获取到的指定存储空间（Bucket）的存储容量以及文件（Object）数量。")]
public record BucketStat
{
    [Display(Name = "总存储量", Prompt = "Bucket的总存储量，单位字节")]
    public ulong Storage { get; }
    
    [Display(Name = "Object数量", Prompt = "Bucket中总的Object数量")]
    public ulong ObjectCount { get; }
    
    [Display(Name = "Multipart Upload 数量", Prompt = "Bucket中已经初始化但还未完成（Complete）或者还未中止（Abort）的Multipart Upload数量。")]
    public ulong MultipartUploadCount { get; }

    public BucketStat(ulong storage, ulong objectCount, ulong multipartUploadCount)
    {
        Storage = storage;
        ObjectCount = objectCount;
        MultipartUploadCount = multipartUploadCount;
    }
}