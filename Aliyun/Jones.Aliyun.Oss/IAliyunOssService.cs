using Jones.Aliyun.Oss.Entities;

namespace Jones.Aliyun.Oss;

public interface IAliyunOssService
{
    ClientPostPolicy GeneratePostPolicy(OssClientInfo ossClientInfo, string folder, TimeSpan effectiveDuration, long? contentLengthMin, long? contentLengthMax);
    
    BucketStat GetBucketStat(OssClientInfo ossClientInfo, string bucketName);
    
    Task<PutObjectResult> CreateFolder(OssClientInfo ossClientInfo, string bucketName, string path);
    
    Task Delete(OssClientInfo ossClientInfo, string bucketName, params string[] objectNames);

    Task<CopyObjectResult> Copy(OssClientInfo ossClientInfo, string sourceBucketName, string sourceObjectName, string destBucketName, string destObjectName);
    Task<CopyObjectResult[]> Copy(OssClientInfo ossClientInfo, CopyObjectInfo[] objects);
}