using System.Runtime.Serialization;

namespace Jones.Aliyun.Oss;

public class AliyunOssException : Exception
{
    public AliyunOssException()
    {
    }

    protected AliyunOssException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public AliyunOssException(string? message) : base(message)
    {
    }

    public AliyunOssException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}