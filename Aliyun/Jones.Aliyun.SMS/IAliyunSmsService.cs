using Jones.Aliyun.SMS.Dto;
using Jones.Aliyun.SMS.Entities;

namespace Jones.Aliyun.SMS;

public interface IAliyunSmsService
{
    // 多个phoneNumber用英文逗号（,）分隔
    Task<SendSmsResult> SendSms(SmsClientInfo smsClientInfo, string phoneNumbers, string signName, string templateCode, string? templateParam, string? orderId);
    Task<SendSmsResult> SendBatchSms(SmsClientInfo smsClientInfo, string[] phoneNumbers, string[] signCodes, string templateCode, string? templateParam);
    
    Task<QuerySendDetailsResult?> QuerySendDetails(SmsClientInfo smsClientInfo, string bizId, string phoneNumber, string outId, DateTime sendTime);

    Task<SmsReportResult[]?> SmsReport(string content);
}