using System.ComponentModel.DataAnnotations;

namespace Jones.Aliyun.SMS.Dto;

// https://help.aliyun.com/document_detail/419277.html
public record QuerySendDetailsResult
{
    [Display(Name = "短信发送状态", Prompt = "包括：1：等待回执。2：发送失败。3：发送成功。")]
    public int SendStatus { get; set; }
    
    public int? Size { get; set; }
    
    public string ErrCode { get; set; }
    
    public string TemplateCode { get; set; }
    
    public string Content { get; set; }
    
    public DateTime? ReceiveTime { get; set; }

    public DateTime? SendTime { get; set; }
}