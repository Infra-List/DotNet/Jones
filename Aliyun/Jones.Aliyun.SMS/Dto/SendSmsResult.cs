using System.ComponentModel.DataAnnotations;

namespace Jones.Aliyun.SMS.Dto;

// https://help.aliyun.com/document_detail/419273.html
public record SendSmsResult
{
    public bool IsSuccess { get; }
    
    [Display(Name = "请求状态码", Prompt = "返回OK代表请求成功，其他错误码，请参见文档")]
    // https://help.aliyun.com/document_detail/101346.htm?spm=a2c4g.11186623.0.0.29d71c33K7Hd7X
    public string Code { get; }
    
    [Display(Name = "状态码的描述")]
    public string? Message { get; }
    
    
    [Display(Name = "发送回执Id", Prompt = "可根据发送回执 Id 在接口 QuerySendDetails 中查询具体的发送状态")]
    public string? BizId { get; }
    
    [Display(Name = "请求Id")]
    public string? RequestId { get; }

    public SendSmsResult(bool isSuccess, string code, string? message, string? bizId, string? requestId)
    {
        IsSuccess = isSuccess;
        Code = code;
        Message = message;
        BizId = bizId;
        RequestId = requestId;
    }
}