using System.ComponentModel.DataAnnotations;

namespace Jones.Aliyun.SMS.Dto;

// https://help.aliyun.com/document_detail/101867.html?spm=5176.25163407.help.dexternal.7058bb6ehGKfWW
public record SmsReportResult
{
    public string PhoneNumber { get; }
    
    [Display(Name = "发送回执ID，即发送流水号")]
    public string BizId { get; }
    
    [Display(Name = "用户序列号")]
    public string? OutId { get; }
        
    [Display(Name = "是否接收成功")]
    public bool IsSuccess { get; }
    
    public string Code { get; }
    public string? Message { get; }
    public int? Size { get; }
    public DateTime SendTime { get; }
    public DateTime ReportTime { get; }

    public SmsReportResult(string phoneNumber, string bizId, string? outId, bool isSuccess, string code, string? message, int? size, DateTime sendTime, DateTime reportTime)
    {
        PhoneNumber = phoneNumber;
        BizId = bizId;
        OutId = outId;
        IsSuccess = isSuccess;
        Code = code;
        Message = message;
        Size = size;
        SendTime = sendTime;
        ReportTime = reportTime;
    }
}