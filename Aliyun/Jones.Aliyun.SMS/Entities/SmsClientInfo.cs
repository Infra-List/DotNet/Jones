namespace Jones.Aliyun.SMS.Entities;

public record SmsClientInfo
{
    public string AccessKeyId { get; }
    
    public string AccessKeySecret { get; }
    
    public string Endpoint { get; }

    public SmsClientInfo(string accessKeyId, string accessKeySecret, string endpoint)
    {
        AccessKeyId = accessKeyId;
        AccessKeySecret = accessKeySecret;
        Endpoint = endpoint;
    }
}