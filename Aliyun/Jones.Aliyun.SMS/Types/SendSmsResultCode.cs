using System.ComponentModel.DataAnnotations;

namespace Jones.Aliyun.SMS.Types;

public static class SendSmsResultCode
{
    [Display(Name = "成功")]
    public const string Success = "OK";
}