namespace Jones.Aliyun.SMS.Types;

public static class QuerySendStatus
{
    public const long WaitingForReceipt = 1;
    public const long Fail = 2;
    public const long Success = 3;
}