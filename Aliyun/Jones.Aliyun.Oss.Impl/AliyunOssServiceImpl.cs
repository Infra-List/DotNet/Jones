using System.Net;
using System.Security.Cryptography;
using System.Text;
using Aliyun.OSS;
using Jones.Aliyun.Oss.Entities;
using BucketStat = Jones.Aliyun.Oss.Entities.BucketStat;
using CopyObjectResult = Jones.Aliyun.Oss.Entities.CopyObjectResult;
using PutObjectResult = Jones.Aliyun.Oss.Entities.PutObjectResult;

namespace Jones.Aliyun.Oss.Impl;

public class AliyunOssServiceImpl : IAliyunOssService
{
    // https://www.alibabacloud.com/help/zh/object-storage-service/latest/description
    private static OssClient GetOssClient(OssClientInfo ossClientInfo) => new (ossClientInfo.Endpoint, ossClientInfo.AccessKeyId, ossClientInfo.AccessKeySecret);
    
    // https://www.cnblogs.com/imust2008/p/6869146.html
    // https://help.aliyun.com/document_detail/31926.html
    public ClientPostPolicy GeneratePostPolicy(OssClientInfo ossClientInfo, string folder, TimeSpan effectiveDuration, long? contentLengthMin, long? contentLengthMax)
    {
        var expiration = DateTime.Now.Add(effectiveDuration);
        var policyConditions = new PolicyConditions();
        policyConditions.AddConditionItem(MatchMode.StartWith, PolicyConditions.CondKey, folder);//上传目录
        if (contentLengthMin != null && contentLengthMax != null)
        {
            policyConditions.AddConditionItem(PolicyConditions.CondContentLengthRange, contentLengthMin.Value, contentLengthMax.Value);//允许上传的文件大小限制       
        }
        var ossClient = GetOssClient(ossClientInfo);
        var postPolicy = ossClient.GeneratePostPolicy(expiration, policyConditions);//给policyConds添加过期时间并json序列化（格式iso8601:"yyyy-MM-dd'T'HH:mm:ss.fff'Z'"）

        /*生成的Policy范例
            {"expiration":"2017-05-17T20:23:23Z","conditions":[["content-length-range",0,1048576000],["starts-with","$key","zhangsan"]]}
            */
        //第二步 将policy 的json字符串进行base64编码

        var base64Policy = Convert.ToBase64String(Encoding.UTF8.GetBytes(postPolicy));

        //第三步，生成签名(哈希算法)
        var signature = ComputeSignature(ossClientInfo.AccessKeySecret, base64Policy);//生成签名
            
        // //以下返回给前端表单域或者阿里云OSS的js api
        // TimeSpan ts = expiration - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        // var expire = Convert.ToInt64(ts.TotalSeconds);
            
        return new ClientPostPolicy(folder, ossClientInfo.AccessKeyId, base64Policy, signature);
    }
    
    private static string ComputeSignature(string key, string data)
    {
        using var hmacSha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key.ToCharArray()));
        return Convert.ToBase64String(hmacSha1.ComputeHash(Encoding.UTF8.GetBytes(data.ToCharArray())));
    }

    // https://www.alibabacloud.com/help/zh/object-storage-service/latest/get-bucket-stat
    public BucketStat GetBucketStat(OssClientInfo ossClientInfo, string bucketName)
    {
        var ossClient = GetOssClient(ossClientInfo);
        var bucketStat = ossClient.GetBucketStat(bucketName);
        return new BucketStat(bucketStat.Storage, bucketStat.ObjectCount, bucketStat.MultipartUploadCount);
    }

    public async Task<PutObjectResult> CreateFolder(OssClientInfo ossClientInfo, string bucketName, string path)
    {
        // https://developer.aliyun.com/ask/150822
        if (path.Last() != '/')
        {
            path += "/";
        }
        return await Upload(ossClientInfo, path, new MemoryStream(), bucketName);
    }

    // https://www.alibabacloud.com/help/zh/object-storage-service/latest/putobject
    private Task<PutObjectResult> Upload(OssClientInfo ossClientInfo, string objectName, Stream content, string bucketName)
    {
        var ossClient = GetOssClient(ossClientInfo);
        var putObjectResult = ossClient.PutObject(bucketName, objectName, content);
        if (putObjectResult.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AliyunOssException($"http status code : {(int)putObjectResult.HttpStatusCode}");
        }
        return Task.FromResult(new PutObjectResult(putObjectResult.ETag, putObjectResult.VersionId, objectName));
    }

    // https://www.alibabacloud.com/help/zh/object-storage-service/latest/deleteobject
    public Task Delete(OssClientInfo ossClientInfo, string bucketName, params string[] objectNames)
    {
        var ossClient = GetOssClient(ossClientInfo);
        var deleteObjectsResult = ossClient.DeleteObjects(new DeleteObjectsRequest(bucketName, objectNames));
        if (deleteObjectsResult.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AliyunOssException($"http status code : {(int)deleteObjectsResult.HttpStatusCode}");
        }
        return Task.CompletedTask;
    }

    // https://www.alibabacloud.com/help/zh/object-storage-service/latest/copyobject
    public Task<CopyObjectResult> Copy(OssClientInfo ossClientInfo, string sourceBucketName, string sourceObjectName, string destBucketName, string destObjectName)
    {
        var ossClient = GetOssClient(ossClientInfo);
        var copyObjectResult = ossClient.CopyObject(new CopyObjectRequest(sourceBucketName, sourceObjectName, destBucketName, destObjectName));
        if (copyObjectResult.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new AliyunOssException($"http status code : {(int)copyObjectResult.HttpStatusCode}");
        }

        return Task.FromResult(new CopyObjectResult(copyObjectResult.LastModified, copyObjectResult.ETag, copyObjectResult.VersionId, copyObjectResult.CopySourceVersionId));
    }

    public Task<CopyObjectResult[]> Copy(OssClientInfo ossClientInfo, CopyObjectInfo[] objects) =>
        Task.WhenAll(objects.Select(file => Copy(ossClientInfo, file.SourceBucketName, file.SourceObjectName, file.DestBucketName, file.DestObjectName)));
}