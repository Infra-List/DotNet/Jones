using Microsoft.Extensions.DependencyInjection;

namespace Jones.Aliyun.Oss.Impl.Extensions;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddAliyunOssService(this IServiceCollection services)
    {
        services.AddTransient<IAliyunOssService, AliyunOssServiceImpl>();
        return services;
    }
}