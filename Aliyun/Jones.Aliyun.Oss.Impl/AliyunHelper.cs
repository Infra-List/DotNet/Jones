using System.Security.Cryptography;
using System.Text;
using Aliyun.OSS;
using Jones.Aliyun.Oss.Entities;

namespace Jones.Aliyun.Oss.Impl;

public static class AliyunHelper
{
    public static string GetHost(string endpoint, string bucketName) => $"https://{bucketName}.{endpoint}/";
    
    // https://www.cnblogs.com/imust2008/p/6869146.html
    public static ClientPostPolicy GetPostObjectSignature(OssClientInfo ossClientInfo, string folder, TimeSpan effectiveDuration)
    {
        var expiration = DateTime.Now.Add(effectiveDuration);
        var policyConds = new PolicyConditions();
        policyConds.AddConditionItem(MatchMode.StartWith, PolicyConditions.CondKey, folder);//上传目录
        policyConds.AddConditionItem(PolicyConditions.CondContentLengthRange, 1, 1048576000);//允许上传的文件大小限制       
        var ossClient = new OssClient(ossClientInfo.Endpoint, ossClientInfo.AccessKeyId, ossClientInfo.AccessKeySecret);//调用阿里云SDK的API
        var postPolicy = ossClient.GeneratePostPolicy(expiration, policyConds);//给policyConds添加过期时间并json序列化（格式iso8601:"yyyy-MM-dd'T'HH:mm:ss.fff'Z'"）

        /*生成的Policy范例
            {"expiration":"2017-05-17T20:23:23Z","conditions":[["content-length-range",0,1048576000],["starts-with","$key","zhangsan"]]}
            */
        //第二步 将policy 的json字符串进行base64编码

        var base64Policy = Convert.ToBase64String(Encoding.UTF8.GetBytes(postPolicy));

        //第三步，生成签名(哈希算法)
        var signature = ComputeSignature(ossClientInfo.AccessKeySecret, base64Policy);//生成签名
            
        // //以下返回给前端表单域或者阿里云OSS的js api
        // TimeSpan ts = expiration - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        // var expire = Convert.ToInt64(ts.TotalSeconds);
            
        return new ClientPostPolicy(folder, ossClientInfo.AccessKeyId, base64Policy, signature);
    }
    
    private static string ComputeSignature(string key, string data)
    {
        using var hmacSha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key.ToCharArray()));
        return Convert.ToBase64String(hmacSha1.ComputeHash(Encoding.UTF8.GetBytes(data.ToCharArray())));
    }
}