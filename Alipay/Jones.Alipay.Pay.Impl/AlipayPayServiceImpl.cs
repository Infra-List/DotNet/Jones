using Aop.Api;
using Aop.Api.Request;
using Aop.Api.Util;
using Jones.Alipay.Pay.Entities;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Jones.Alipay.Pay.Impl;

public class AlipayPayServiceImpl : IAlipayPayService
{
    private readonly AlipayPayConfig _config;

    public AlipayPayServiceImpl(IOptions<AlipayPayConfig> config)
    {
        _config = config.Value;
    }

    // https://opendocs.alipay.com/open/02fkbf
    #region 付款码支付

    public AlipayCommonResponse<PaymentCodePayOrderResult> PaymentCodePayOrder(AlipayAppInfo app, string paymentCode, string outTradeNo, int totalAmount, string subject, string notifyUrl)
    {
        var client = GetClient(GetApp(app));
        var request = new AlipayTradePayRequest();
        var bizContent = new Dictionary<string, object>
        {
            { "out_trade_no", outTradeNo },
            { "total_amount", totalAmount / 100.00 },
            { "subject", subject },
            { "scene", "bar_code" },
            { "auth_code", paymentCode }
        };

        //扩展信息
        if (app.ServiceProviderId != null)
        {
            var extendParams = new Dictionary<string, object>();
            extendParams.Add("sys_service_provider_id", app.ServiceProviderId);
            bizContent.Add("extend_params", extendParams);
        }
        
        request.SetNotifyUrl(notifyUrl);

        var contentJson = JsonConvert.SerializeObject(bizContent);
        request.BizContent = contentJson;
        var response = client.Execute(request);
        Console.WriteLine(response.Body);

        return new AlipayCommonResponse<PaymentCodePayOrderResult>(!response.IsError, response.Body, response.Code,
            response.Msg, response.SubCode, response.SubMsg,
            response.IsError 
                ? null 
                : new PaymentCodePayOrderResult(response.BuyerLogonId, response.BuyerUserId, response.TradeNo, 
                    ToMoney(response.TotalAmount)!.Value, ToMoney(response.ReceiptAmount)!.Value, 
                    ToMoney(response.BuyerPayAmount), ToMoney(response.PointAmount), 
                    ToMoney(response.InvoiceAmount), ToDateTime(response.GmtPayment)));
    }

    #endregion

    // https://opendocs.alipay.com/open/02fkbh
    #region 订单查询

    public async Task<AlipayCommonResponse<OrderQueryResult>> Query(AlipayAppInfo app, string? outTradeNo, string? tradeNo)
    {
        var client = GetClient(GetApp(app));
        var request= new AlipayTradeQueryRequest() ;
        var bizContent = new Dictionary<string, object>();
        if (outTradeNo != null)
        {
            bizContent.Add("out_trade_no", outTradeNo);
        }
        if (tradeNo != null)
        {
            bizContent.Add("trade_no", tradeNo);
        }
        var contentJson = JsonConvert.SerializeObject(bizContent);
        request.BizContent = contentJson;
        var response=client.Execute(request);
        // Console.WriteLine(response.Body);
        return new AlipayCommonResponse<OrderQueryResult>(!response.IsError, response.Body, response.Code,
            response.Msg, response.SubCode, response.SubMsg,
            response.IsError 
                ? null 
                : new OrderQueryResult(response.TradeStatus, response.BuyerLogonId, response.BuyerUserId, response.TradeNo, 
                    ToMoney(response.TotalAmount)!.Value, ToMoney(response.ReceiptAmount)!.Value, 
                    ToMoney(response.BuyerPayAmount), ToMoney(response.PointAmount), 
                    ToMoney(response.InvoiceAmount)));
    }

    #endregion

    // https://opendocs.alipay.com/open/194/103296
    #region 支付通知
    
    public async Task<CheckData<TradeStatusSyncNotifyResult>> Notify(AlipayAppInfo app, Dictionary<string, string> urlParams)
    {
        var alipayApp = GetApp(GetApp(app));
        if (!AlipaySignature.RSACheckV1(urlParams, alipayApp.AlipayPublicKey, _config.Charset, _config.SignType, false))
        {
            return new CheckData<TradeStatusSyncNotifyResult>(false, "验签失败", null);
        }

        if (urlParams["notify_type"] != "trade_status_sync")
        {
            return new CheckData<TradeStatusSyncNotifyResult>(false, $"通知类型异常：{urlParams["notify_type"]}", null); 
        }

        return new CheckData<TradeStatusSyncNotifyResult>(true, null,
            new TradeStatusSyncNotifyResult(urlParams["trade_status"], urlParams["buyer_logon_id"], 
                urlParams["buyer_id"], urlParams["trade_no"], ToMoney(urlParams["total_amount"])!.Value, 
                ToMoney(urlParams["receipt_amount"])!.Value, ToMoney(urlParams["buyer_pay_amount"]), 
                ToMoney(urlParams["point_amount"]), ToMoney(urlParams["invoice_amount"]), 
                ToDateTime(urlParams["notify_time"]), 
                ToDateTime(urlParams["gmt_payment"])));
    }

    #endregion

    // https://opendocs.alipay.com/open/02fkbi
    #region 关闭订单

    public async Task<AlipayCommonResponse> Close(AlipayAppInfo app, string? outTradeNo, string? tradeNo, string notifyUrl)
    {
        var client = GetClient(GetApp(app));
        var  request= new AlipayTradeCloseRequest() ;
        var bizContent = new Dictionary<string, object>();
        if (outTradeNo != null)
        {
            bizContent.Add("out_trade_no", outTradeNo);
        }
        if (tradeNo != null)
        {
            bizContent.Add("trade_no", tradeNo);
        }
        var contentJson = JsonConvert.SerializeObject(bizContent);
        request.BizContent = contentJson;
        request.SetNotifyUrl(notifyUrl);
        var response=client.Execute(request);
        // Console.WriteLine(response.Body);
        return new AlipayCommonResponse(!response.IsError, response.Body, response.Code,
            response.Msg, response.SubCode, response.SubMsg);
    }

    #endregion

    #region 关闭订单通知

    public Task<CheckData<TradeStatusSyncNotifyResult>> CloseNotify(AlipayAppInfo app,
        Dictionary<string, string> urlParams) => Notify(app, urlParams);

    #endregion

    // https://opendocs.alipay.com/open/02fkbg
    #region 退款

    public async Task<AlipayCommonResponse<RefundResult>> Refund(AlipayAppInfo app, string tradeNo, string outRequestNo, int refundAmount, string? refundReason)
    {
        var client = GetClient(GetApp(app));
        var request= new AlipayTradeRefundRequest();
        var bizContent = new Dictionary<string, object>
        {
            { "trade_no", tradeNo },
            { "refund_amount", refundAmount / 100.00 },
            { "out_request_no", outRequestNo }
        };
        if (refundReason != null)
        {
            bizContent.Add("refund_reason", refundReason);
        }

        // 返回参数选项，按需传入
        var queryOptions = new List<string> { "refund_detail_item_list" };
        bizContent.Add("query_options", queryOptions);

        var contentJson = JsonConvert.SerializeObject(bizContent);
        request.BizContent = contentJson;
        var response=client.Execute(request);
        // Console.WriteLine(response.Body);
        return new AlipayCommonResponse<RefundResult>(!response.IsError, response.Body, response.Code,
            response.Msg, response.SubCode, response.SubMsg,
            response.IsError 
                ? null 
                : new RefundResult(response.FundChange, ToMoney(response.RefundFee)!.Value,
                    ToMoney(response.SendBackFee), ToMoney(response.RefundHybAmount)));
    }

    #endregion

    // https://opendocs.alipay.com/open/02j5ap
    #region 退款查询

    public async Task<AlipayCommonResponse<RefundQueryResult>> RefundQuery(AlipayAppInfo app, string tradeNo, string outRequestNo)
    {
        var client = GetClient(GetApp(app));
        var request= new AlipayTradeFastpayRefundQueryRequest();
        var bizContent = new Dictionary<string, object>
        {
            { "trade_no", tradeNo },
            { "out_request_no", outRequestNo }
        };

        // 返回参数选项，按需传入
        var queryOptions = new List<string> { "refund_detail_item_list", "gmt_refund_pay" };
        bizContent.Add("query_options", queryOptions);

        var contentJson = JsonConvert.SerializeObject(bizContent);
        request.BizContent = contentJson;
        var response=client.Execute(request);
        // Console.WriteLine(response.Body);
        return new AlipayCommonResponse<RefundQueryResult>(!response.IsError, response.Body, response.Code,
            response.Msg, response.SubCode, response.SubMsg,
            response.IsError 
                ? null 
                : new RefundQueryResult(response.RefundStatus, ToMoney(response.TotalAmount), 
                    ToMoney(response.RefundAmount), ToMoney(response.SendBackFee), 
                    ToMoney(response.RefundHybAmount), ToDateTime(response.GmtRefundPay)));
    }

    #endregion

    private AlipayAppInfo GetApp(AlipayAppInfo app) => _config.IsSandbox && _config.SandboxApp != null
        ? new AlipayAppInfo(_config.SandboxApp.Id, _config.SandboxApp.MerchantPrivateKey,
            _config.SandboxApp.AlipayPublicKey, _config.SandboxApp.ServiceProviderId)
        : app;
    
    private IAopClient GetClient(AlipayAppInfo app) => new DefaultAopClient(GetServerUrl(), app.Id, app.MerchantPrivateKey, 
        "json", _config.Version, _config.SignType, app.AlipayPublicKey, _config.Charset, false);

    private string GetServerUrl() => _config.IsSandbox && _config.SandboxApp != null ? _config.SandboxApp.ServerUrl : _config.ServerUrl;

    private static int? ToMoney(string? source) => decimal.TryParse(source, out var money) ? (int)(money * 100) : null;

    private static DateTime? ToDateTime(string? source) => string.IsNullOrEmpty(source)
        ? null
        : DateTime.ParseExact(source, "yyyy-MM-dd HH:mm:ss", null).ToUniversalTime();
}