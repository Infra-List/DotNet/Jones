using Microsoft.Extensions.DependencyInjection;

namespace Jones.Alipay.Pay.Impl.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddAlipayPayService(this IServiceCollection services)
    {
        services.AddTransient<IAlipayPayService, AlipayPayServiceImpl>();

        return services;
    }
}