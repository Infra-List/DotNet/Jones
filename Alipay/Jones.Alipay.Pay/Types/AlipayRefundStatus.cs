using System.ComponentModel.DataAnnotations;

namespace Jones.Alipay.Pay.Types;

public class AlipayRefundStatus
{
    [Display(Description = "退款成功")]
    public const string Success = "REFUND_SUCCESS";
    
    
    [Display(Description = "本次退款是否发生了资金变化")]
    public const string FundChangeN = "Fund_Change_N";
    
    public static bool IsProcessing(string? state) => state is null or FundChangeN;
}