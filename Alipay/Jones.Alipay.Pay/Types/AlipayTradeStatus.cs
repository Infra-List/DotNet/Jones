using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jones.Alipay.Pay.Types;

// https://opensupport.alipay.com/support/helpcenter/193/201602472371
[Description("交易状态")]
public static class AlipayTradeStatus
{
    [Display(Description = "等待买家付款")]
    // 交易创建，等待买家付款
    public const string WaitBuyerPay = "WAIT_BUYER_PAY";

    [Display(Description = "交易关闭")]
    // 未付款交易超时关闭，或支付完成后全额退款
    public const string Closed = "TRADE_CLOSED";
    
    [Display(Description = "交易支付成功")]
    // 交易支付成功
    public const string Success = "TRADE_SUCCESS";
    
    [Display(Description = "交易结束")]
    // 交易结束，不可退款
    public const string Finished = "TRADE_FINISHED";

    public static bool IsCompleted(string? tradeStatus) => 
        tradeStatus is Closed or Success or Finished;
    
    public static bool IsSuccess(string? tradeStatus) => tradeStatus is Success or Finished;

    public static bool IsUserPaying(string? tradeStatus) => tradeStatus is null or WaitBuyerPay;
}