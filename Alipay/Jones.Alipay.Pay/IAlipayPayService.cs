using Jones.Alipay.Pay.Entities;

namespace Jones.Alipay.Pay;

public interface IAlipayPayService
{
    // 付款码支付
    AlipayCommonResponse<PaymentCodePayOrderResult> PaymentCodePayOrder(AlipayAppInfo app, 
        string paymentCode, string outTradeNo, int totalAmount, string subject, string notifyUrl);
    
    // 订单查询
    Task<AlipayCommonResponse<OrderQueryResult>> Query(AlipayAppInfo app, string? outTradeNo, string? tradeNo);

    // 支付通知
    Task<CheckData<TradeStatusSyncNotifyResult>> Notify(AlipayAppInfo app, Dictionary<string, string> urlParams);
    
    // 关闭订单
    Task<AlipayCommonResponse> Close(AlipayAppInfo app, string? outTradeNo, string? tradeNo, string notifyUrl);
    // 关闭订单通知
    Task<CheckData<TradeStatusSyncNotifyResult>> CloseNotify(AlipayAppInfo app, Dictionary<string, string> urlParams);
    
    // 退款
    Task<AlipayCommonResponse<RefundResult>> Refund(AlipayAppInfo app, string tradeNo, string outRequestNo, 
        int refundAmount, string? refundReason);
    
    // 退款查询
    Task<AlipayCommonResponse<RefundQueryResult>> RefundQuery(AlipayAppInfo app, string tradeNo, string outRequestNo);
}