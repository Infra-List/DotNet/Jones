namespace Jones.Alipay.Pay;

public record AlipayPayConfig
{
    public bool IsSandbox { get; set; }
    public SandboxAppInfo? SandboxApp { get; set; }
    public string Version { get; set; } = "1.0";
    public string SignType { get; set; } = "RSA2";
    public string Charset { get; set; } = "utf-8";
    public string ServerUrl { get; set; } = "https://openapi.alipay.com/gateway.do";

    public record SandboxAppInfo
    {
        public string Id { get; set; }
        public string MerchantPrivateKey { get; set; }
        public string AlipayPublicKey { get; set; }
        public string? ServiceProviderId { get; set; }
        public string ServerUrl { get; set; }
    }
}