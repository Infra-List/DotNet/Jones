using Jones.Alipay.Pay.Entities;

namespace Jones.Alipay.Pay.Extensions;

public static class AlipayCommonResponseExtensions
{
    public static string? ErrorMessage(this AlipayCommonResponse source) => 
        source.Message == null && source.SubMessage == null 
            ? null 
            : string.Join(Environment.NewLine, new[] { source.Message, source.SubMessage }.Where(p => !string.IsNullOrEmpty(p)));
}