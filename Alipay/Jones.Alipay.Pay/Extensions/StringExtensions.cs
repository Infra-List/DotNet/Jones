using System.Text.RegularExpressions;

namespace Jones.Alipay.Pay.Extensions;

public static class StringExtensions
{
    // http://www.qilin668.com/a/5e4be4e8a1b78z4.html
    // https://opensupport.alipay.com/support/knowledge/24119/201602053270?ant_source=zsearch
    public static bool IsAlipayPaymentCode(this string source) => new Regex(@"^(?:2[5-9]|30)\d{14,18}$").IsMatch(source);
}