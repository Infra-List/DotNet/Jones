namespace Jones.Alipay.Pay.Entities;

public record TradeStatusSyncNotifyResult(
    string TradeStatus,
    string BuyerLogonId,
    string BuyerId,
    string TradeNo,
    int TotalAmount,
    int ReceiptAmount,
    int? BuyerPayAmount, 
    int? PointAmount, 
    int? InvoiceAmount, 
    DateTime? NotifyTime, 
    DateTime? GmtPayment);