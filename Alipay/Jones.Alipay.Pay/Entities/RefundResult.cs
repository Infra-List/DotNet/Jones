using System.ComponentModel.DataAnnotations;

namespace Jones.Alipay.Pay.Entities;

public record RefundResult(
    [Display(Name = "本次退款是否发生了资金变化")]
    string FundChange,  // 示例值：Y
    
    
    [Display(Name = "退款总金额", Prompt = "指该笔交易累计已经退款成功的金额")]
    int RefundFee, 
    
    [Display(Name = "本次商户实际退回金额", Prompt = "如需获取该值，需在入参query_options中传入 refund_detail_item_list")]
    int? SendBackFee, 
    
    [Display(Name = "本次请求退惠营宝金额")]
    int? RefundHybAmount);