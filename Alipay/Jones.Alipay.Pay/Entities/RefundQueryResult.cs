using System.ComponentModel.DataAnnotations;

namespace Jones.Alipay.Pay.Entities;

public record RefundQueryResult(
    [Display(Name = "退款状态", Description = @"
        枚举值：
        REFUND_SUCCESS 退款处理成功；
        未返回该字段表示退款请求未收到或者退款失败；
        注：如果退款查询发起时间早于退款时间，或者间隔退款发起时间太短，可能出现退款查询时还没处理成功，后面又处理成功的情况，建议商户在退款发起后间隔10秒以上再发起退款查询请求。")]
    string? RefundStatus,
    
    [Display(Name = "该笔退款所对应的交易的订单金额")]
    int? TotalAmount,
    
    
    [Display(Name = "本次退款请求，对应的退款金额")]
    int? RefundAmount, 
    
    [Display(Name = "本次商户实际退回金额", Prompt = "默认不返回该信息，需要在入参的query_options中指定 `refund_detail_item_list` 值时才返回该字段信息。")]
    int? SendBackFee, 
    
    [Display(Name = "本次退款金额中退惠营宝的金额")]
    int? RefundHybAmount,
    
    [Display(Name = "退款时间", Prompt = "默认不返回该信息，需要在入参的query_options中指定 `gmt_refund_pay` 值时才返回该字段信息。")]
    DateTime? GmtRefundPay);