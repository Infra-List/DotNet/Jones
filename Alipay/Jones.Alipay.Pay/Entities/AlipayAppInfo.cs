namespace Jones.Alipay.Pay.Entities;

public record AlipayAppInfo(string Id, string MerchantPrivateKey, string AlipayPublicKey, string? ServiceProviderId);