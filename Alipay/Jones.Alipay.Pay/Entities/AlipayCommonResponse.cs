using System.ComponentModel.DataAnnotations;

namespace Jones.Alipay.Pay.Entities;

public record AlipayCommonResponse(
    bool IsSuccess,
    string? Content,
    
    // https://opendocs.alipay.com/common/02km9f
    // https://opendocs.alipay.com/support/01rfob
    // 当唤起输入支付密码页面时，接口同步响应会返回 10003，order success pay inprocess 的数据信息
    [Display(Name = "网关返回码")]
    string Code, 
    
    [Display(Name = "网关返回码描述")]
    string? Message,
    [Display(Name = "业务返回码，参见具体的API接口文档")]
    string? SubCode, 
    [Display(Name = "业务返回码描述，参见具体的API接口文档")]
    string? SubMessage);

public record AlipayCommonResponse<T>(bool IsSuccess, string? Content, string? Code, string? Message, 
        string? SubCode, string? SubMessage, T? Data) 
    : AlipayCommonResponse(IsSuccess, Content, Code, Message, SubCode, SubMessage);