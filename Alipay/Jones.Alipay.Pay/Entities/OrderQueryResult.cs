using System.ComponentModel.DataAnnotations;

namespace Jones.Alipay.Pay.Entities;

public record OrderQueryResult(
    [Display(Name = "交易状态", Prompt = "WAIT_BUYER_PAY（交易创建，等待买家付款）、TRADE_CLOSED（未付款交易超时关闭，或支付完成后全额退款）、TRADE_SUCCESS（交易支付成功）、TRADE_FINISHED（交易结束，不可退款）")]
    string TradeStatus, 
    
    [Display(Name = "买家支付宝账号")]
    string BuyerLogonId, 
    
    [Display(Name = "买家支付宝Id", Prompt = "买家在支付宝的用户Id")]
    string BuyerUserId, 
    
    [Display(Name = "支付宝交易号")]
    string TradeNo, 
    
    [Display(Name = "交易金额")]
    int TotalAmount, 
    
    [Display(Name = "实收金额")]
    int ReceiptAmount,
    
    [Display(Name = "买家付款的金额")]
    int? BuyerPayAmount, 
    
    [Display(Name = "使用集分宝付款的金额")]
    int? PointAmount, 
    
    [Display(Name = "交易中可给用户开具发票的金额")]
    int? InvoiceAmount);