using System.ComponentModel.DataAnnotations;

namespace Jones.Alipay.Pay.Entities;

public record PaymentCodePayOrderResult(
    [Display(Name = "买家支付宝账号")] string BuyerLogonId,
    [Display(Name = "买家支付宝Id", Prompt = "买家在支付宝的用户Id")] string BuyerUserId,
    [Display(Name = "支付宝交易号")] string TradeNo,
    [Display(Name = "交易金额")] int TotalAmount,
    [Display(Name = "实收金额")] int ReceiptAmount,
    [Display(Name = "买家付款的金额")] int? BuyerPayAmount,
    [Display(Name = "使用集分宝付款的金额")] int? PointAmount,
    [Display(Name = "交易中可给用户开具发票的金额")] int? InvoiceAmount,
    [Display(Name = "交易支付时间")] DateTime? GmtPayment);