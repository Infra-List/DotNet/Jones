using System.ComponentModel.DataAnnotations;

namespace Jones.Scheduler.Models;

public class JobOperatingViewModel
{
    [Display(Name = "作业名称")]
    public string JobName { get; set; }
}