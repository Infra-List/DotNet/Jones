using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Jones.Scheduler.Dto;

namespace Jones.Scheduler;

public interface ISchedulerService
{
    Task StartAsync();
    Task StartAsync(CancellationToken cancellationToken);

    Task StopAsync();
    Task StopAsync(CancellationToken cancellationToken);
        
    Task<bool> IsJobExists<T>();
    Task<bool> IsJobExists(string jobName);
        
    Task TriggerJob<T>(IDictionary<string, object>? data = null);
    Task TriggerJob(string jobName, IDictionary<string, object>? data = null);

    Task InterruptJob<T>();
    Task InterruptJob(string jobName);

    Task<IEnumerable<SchedulerJob>> GetSchedulerJobs();

    Task RunStatistics(string jobName);
}