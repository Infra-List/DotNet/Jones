using System;
using Jones.Scheduler.Types;

namespace Jones.Scheduler.Dto;

public record SchedulerJob
{
    public string Name { get; }
        
    public string TypeName { get; }
        
    public string? Description { get; }
        
    public string? CronExpression { get; }
    
    public DateTime? NextValidTime { get; }

    public JobStatus Status { get; }
        
    public SchedulerJobStatistics? Statistics { get; }

    public SchedulerJob(string name, string typeName, string? description, string? cronExpression, DateTime? nextValidTime, JobStatus status, SchedulerJobStatistics? statistics)
    {
        Name = name;
        TypeName = typeName;
        Description = description;
        CronExpression = cronExpression;
        NextValidTime = nextValidTime;
        Status = status;
        Statistics = statistics;
    }
}