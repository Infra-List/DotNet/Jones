namespace Jones.Scheduler.Dto;

public record SchedulerJobStatistics
{
    public int RunCount { get; set; }
    public long LastRunTimestamp { get; set; }
}