using System.ComponentModel.DataAnnotations;

namespace Jones.Scheduler.Types;

public enum JobStatus
{
    [Display(Name = "初始化")]
    Init = 0,
    [Display(Name = "已排程")]
    Scheduled = 1,
    [Display(Name = "执行中")]
    Running = 2,
    [Display(Name = "已停止")]
    Stopped = 3,
}