using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jones.Scheduler.Extensions;

public static class SchedulerExtensions
{
    public static Task RunJob<T>(this ISchedulerService schedulerService, int? millisecondsDelay = null, IDictionary<string, object>? data = null)
    {
        return millisecondsDelay == null
            ? schedulerService.TriggerJob<T>(data)
            : DelayRun(millisecondsDelay.Value, () => schedulerService.TriggerJob<T>(data));
    }
    public static Task RunJob<T>(this ISchedulerService schedulerService, string jobName, int? millisecondsDelay = null, IDictionary<string, object>? data = null)
    {
        return millisecondsDelay == null
            ? schedulerService.TriggerJob<T>(data)
            : DelayRun(millisecondsDelay.Value, () => schedulerService.TriggerJob(jobName, data));
    }
        
    private static async Task DelayRun(int millisecondsDelay, Action action)
    {
        await Task.Delay(millisecondsDelay);
        action.Invoke();
    }
}