namespace Jones.OpenTelemetry.Prometheus;

public record OpenTelemetryPrometheusConfig
{
    public bool? IsEnabled { get; set; } = true;
}