using Jones.Helper;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;

namespace Jones.OpenTelemetry.Prometheus.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddOpenTelemetryWithPrometheus(this IServiceCollection services, OpenTelemetryPrometheusConfig config, string serviceName, string? serviceInstanceId, string? serviceNamespace)
    {
        if (config.IsEnabled != true)
        {
            return services;
        }
        
        // https://learn.microsoft.com/zh-cn/dotnet/core/diagnostics/observability-with-otel
        // https://grafana.com/grafana/dashboards/19924-asp-net-core/
        // https://grafana.com/grafana/dashboards/17706-asp-net-otel-metrics/
        // https://grafana.com/grafana/dashboards/19896-asp-net-otel-metrics-from-otel-collector/
        services.AddOpenTelemetry()
            .ConfigureResource(resource => resource.AddService(
                serviceName, 
                serviceNamespace,
                serviceVersion: EntryAssemblyHelper.GetLiteAssemblyVersion(),
                serviceInstanceId: serviceInstanceId))
            .WithMetrics(metrics => metrics
                // Metrics provider from OpenTelemetry
                .AddAspNetCoreInstrumentation()
                .AddHttpClientInstrumentation()
                .AddRuntimeInstrumentation()
                .AddProcessInstrumentation()
                // Metrics provides by ASP.NET Core in .NET 8
                .AddMeter("Microsoft.AspNetCore.Hosting")
                .AddMeter("Microsoft.AspNetCore.Server.Kestrel")
                .AddPrometheusExporter()
            ).WithTracing(tracing =>
            {
                tracing.AddAspNetCoreInstrumentation();
                tracing.AddHttpClientInstrumentation();
            });
        return services;
    }

    public static void UseOpenTelemetryWithPrometheus(this WebApplication app, OpenTelemetryPrometheusConfig config)
    {
        if (config.IsEnabled != true)
        {
            return;
        }
        
        app.MapPrometheusScrapingEndpoint();
    }
}