namespace Jones.IdentityServer4
{
    public record IdentityServiceConfig
    {
        public string Authority { get; set; }
        public string Audience { get; set; }
    }
}