﻿using System.Linq;
using Microsoft.AspNetCore.Http;

namespace Jones.IdentityServer4
{
    public class SingleSignOnService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SingleSignOnService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private string? GetAccessToken() => _httpContextAccessor.HttpContext?.Request.Headers["Authorization"]
            .FirstOrDefault()?.Remove(0, "Bearer ".Length);

        public int? GetCurrentUserId()
        {
            var accessToken = GetAccessToken();
            return ValidationHelper.GetCurrentUserId(accessToken);
        }

        public string? GetCurrentUserPhoneNumber()
        {
            var accessToken = GetAccessToken();
            return ValidationHelper.GetCurrentUserPhoneNumber(accessToken);
        }
    }
}