using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Validation;

namespace Jones.IdentityServer4
{
    public static class ValidationHelper
    {
        public const string PhoneNumberClaimType = "phonenumber";
        public static GrantValidationResult GetInvalidGrantResult(string? errorDescription, Dictionary<string, object>? customResponse = null)
        {
            return new GrantValidationResult(TokenRequestErrors.InvalidGrant, errorDescription, customResponse);
        }
        
        public static GrantValidationResult GetSuccessGrantResult(
            int userId, 
            string authenticationMethod = "custom", 
            IEnumerable<Claim>? claims = null,
            string identityProvider = IdentityServerConstants.LocalIdentityProvider,
            Dictionary<string, object>? customResponse = null)
        {
            return new GrantValidationResult(userId.ToString(), authenticationMethod, claims, identityProvider, customResponse);
        }
        
        public static int? GetCurrentUserId(string? accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                return null;
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadToken(accessToken) as JwtSecurityToken;
            // var claims = jwtToken?.Claims.ToArray();
            // var claim = claims?.FirstOrDefault(p => p.Type == ClaimTypes.SerialNumber);
            if (int.TryParse(jwtToken?.Subject, out var userId))
            {
                return userId;
            }
            return null;
        }
        
        public static string? GetCurrentUserPhoneNumber(string? accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                return null;
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadToken(accessToken) as JwtSecurityToken;
            return jwtToken?.Claims?.FirstOrDefault(p => p.Type == PhoneNumberClaimType)?.Value;
        }
    }
}