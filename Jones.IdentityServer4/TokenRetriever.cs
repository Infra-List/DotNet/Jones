using System;
using IdentityModel.AspNetCore.OAuth2Introspection;
using Microsoft.AspNetCore.Http;

namespace Jones.IdentityServer4
{
    public class TokenRetriever
    {
        private static readonly Func<HttpRequest, string> AuthHeaderTokenRetriever = TokenRetrieval.FromAuthorizationHeader();
        public static readonly Func<HttpRequest, string> QueryStringTokenRetriever = TokenRetrieval.FromQueryString();

        public static string FromHeaderAndQueryString(HttpRequest request)
        {
            var token = AuthHeaderTokenRetriever(request);

            if (string.IsNullOrEmpty(token))
            {
                token = QueryStringTokenRetriever(request);
            }

            return token;
        }
    }
}