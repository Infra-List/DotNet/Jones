using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Jones.Serilog.Sinks.EntityFrameworkCore.Quartz;

[DisallowConcurrentExecution]
public class LoggerDataMaintenanceJob : IJob
{
    private readonly ILogger<LoggerDataMaintenanceJob> _logger;
    private readonly IServiceProvider _serviceProvider;

    public LoggerDataMaintenanceJob(ILogger<LoggerDataMaintenanceJob> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        const string messagePrefix = "[日志文件维护Job]";
        _logger.LogInformation("{MessagePrefix} - 任务启动", messagePrefix);
            
        using var scope = _serviceProvider.CreateScope();
        var logService = scope.ServiceProvider.GetService<ILogService>();
        if (logService == null)
        {
            _logger.LogWarning("{MessagePrefix} - 没有找到 ILogService", messagePrefix);
            return;
        }
            
        try
        {
            await logService.DataMaintenance();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "{MessagePrefix} - 日志文件维护出错 - 错误信息[{Message}]", messagePrefix, e.Message);
        }
    }
}