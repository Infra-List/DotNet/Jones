using Jones.Quartz;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.Serilog.Sinks.EntityFrameworkCore.Quartz;

public static class SerilogSinksEntityFrameworkCoreQuartzExtension
{
    public static IServiceCollection AddEntityFrameworkCoreLoggerQuartz(this IServiceCollection services, string? retentionCheckCron)
    {
        if (!Extensions.ServiceCollectionExtensions.IsUseEntityFrameworkCoreLogger)
        {
            return services;
        }

        services.AddJob<LoggerDataMaintenanceJob>("日志文件维护", retentionCheckCron);
            
        return services;
    }
}