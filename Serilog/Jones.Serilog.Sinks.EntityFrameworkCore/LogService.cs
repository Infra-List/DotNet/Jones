using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Jones.EntityFrameworkCore.Extensions;
using Jones.Extensions;
using Jones.Serilog.Sinks.EntityFrameworkCore.Dto;
using Jones.Serilog.Sinks.EntityFrameworkCore.Models;
using Jones.Specification;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Serilog.Events;

namespace Jones.Serilog.Sinks.EntityFrameworkCore;

public interface ILogService
{
    Task<Paging<LogListItemDto>> Page(LogQueryViewModel filter, PagingParams pager);
        
    Task<LogDto?> Get(int id);

    Task DataMaintenance(int? retentionPeriodDays = null);
}
    
public class LogService : ILogService
{
    private readonly LoggerConfig _loggerConfig;

    private readonly LoggerDbContext _loggerDbContext;

    public LogService(IOptions<LoggerConfig> loggerConfig, LoggerDbContext loggerDbContext)
    {
        _loggerConfig = loggerConfig.Value;
        _loggerDbContext = loggerDbContext;
    }

    public async Task<Paging<LogListItemDto>> Page(LogQueryViewModel filter, PagingParams pager)
    {
        Specification<Log> specification = new TrueSpecification<Log>();
        if (filter.Id != null)
        {
            specification &= new DirectSpecification<Log>(p => p.Id == filter.Id);
        }
        else
        {
            if (filter.Level != null)
            {
                specification &= new DirectSpecification<Log>(p => p.Level == filter.Level);
            }

            if (!filter.Content.IsNullOrEmpty())
            {
                specification &= new DirectSpecification<Log>(p => 
                    EF.Functions.Like(p.Message, $"%{filter.Content}%") || EF.Functions.Like(p.Exception, $"%{filter.Content}%"));
                    
                // specification &= new DirectSpecification<Log>(p => p.Message.Contains(filter.Content) || (p.Exception != null && p.Exception.Contains(filter.Content)));
            }

            if (filter.FromTimestamp != null)
            {
                specification &= new DirectSpecification<Log>(p => p.TimeStamp >= filter.FromTimestamp);
            }

            if (filter.ToTimestamp != null)
            {
                specification &= new DirectSpecification<Log>(p => p.TimeStamp <= filter.ToTimestamp);
            }
        }
            
        var paging = await _loggerDbContext.Log
            .Where(specification.SatisfiedBy())
            .OrderByDescending(p => p.TimeStamp)
            .ThenByDescending(p => p.Id)
            .ToPagingAsync(pager.Page ?? 1, _loggerConfig.GetPageSize(pager.PageSize));

        return new Paging<LogListItemDto>(
            paging.Items.Select(log => new LogListItemDto(log.Id, log.Level, log.Message, log.Exception, log.TimeStamp)).ToArray(), 
            paging.Page, paging.PageSize, paging.TotalCount);
    }

    public async Task<LogDto?> Get(int id)
    {
        var log = await _loggerDbContext.Log.FirstOrDefaultAsync(p => p.Id == id);
        return log == null ? null : new LogDto(log.Id, log.Level, log.Message, log.Exception, log.LogEvent, log.TimeStamp);
    }

    public async Task DataMaintenance(int? retentionPeriodDays = null)
    {
        retentionPeriodDays ??= _loggerConfig.RetentionPeriodDays;
        if (retentionPeriodDays == null)
        {
            return;
        }
            
        var epoch = DateTimeOffset.Now.AddDays(-retentionPeriodDays.Value).ToUnixTimeMilliseconds();
        await _loggerDbContext.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM Log WHERE TimeStamp < {epoch}");
    }

    // private DateTime GetDateTime(long timeStamp) =>
    //     DateTimeOffset.FromUnixTimeMilliseconds(timeStamp).LocalDateTime;
}

public record LogQueryViewModel
{
    [Display(Name = "Id")]
    public int? Id { get; set; }
        
    [Display(Name = "等级")]
    [EnumDataType(typeof(LogEventLevel))]
    public LogEventLevel? Level { get; set; }

    [Display(Name = "内容")]
    public string? Content { get; set; }

    [Display(Name = "从")]
    public long? FromTimestamp { get; set; }

    [Display(Name = "到")]
    public long? ToTimestamp { get; set; }
}