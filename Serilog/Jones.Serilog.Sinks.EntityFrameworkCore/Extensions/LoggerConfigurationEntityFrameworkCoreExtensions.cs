using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Debugging;

namespace Jones.Serilog.Sinks.EntityFrameworkCore.Extensions;

// https://beckjin.com/2020/02/14/aspnet-log-serilog/
// https://zangchuantao.com/tech-zh/2019/asp-net-core-serilog-configuration-appsettings-json/
// https://gitee.com/zijiaxing/serilog-sinks-mysql
// https://nblumhardt.com/2020/09/serilog-inject-dependencies/
public static class ServiceCollectionExtensions
{
    public static bool IsUseEntityFrameworkCoreLogger { get; private set; }
        
    public static IServiceCollection AddEntityFrameworkCoreLogger(this IServiceCollection services, IConfiguration configuration, Action<DbContextOptionsBuilder> optionsAction)
    {
        services.Configure<LoggerConfig>(configuration.GetSection("Logger"));
            
        services.AddDbContext<LoggerDbContext>(optionsAction);
            
        services.AddTransient<ILogService, LogService>();
            
        IsUseEntityFrameworkCoreLogger = true;
        return services;
    }

    public static IServiceProvider UseEntityFrameworkCoreLogger(this IServiceProvider serviceProvider, string? baseDirectory = "logs")
    {
        if (!IsUseEntityFrameworkCoreLogger)
        {
            return serviceProvider;
        }
            
        using var scope = serviceProvider.CreateScope();
        var loggerDbContext = scope.ServiceProvider.GetService<LoggerDbContext>();
        if (loggerDbContext != null)
        {
            if (!string.IsNullOrEmpty(baseDirectory))
            {
                if (!Directory.Exists(baseDirectory))
                {
                    Directory.CreateDirectory(baseDirectory);
                }
            }
            try
            {
                loggerDbContext.Database.Migrate();
            }
            catch (Exception e)
            {
                SelfLog.WriteLine($"Migrate LoggerDb 数据库出错：{e}");
            }
        }
        return serviceProvider;
    }

    public static LoggerConfiguration UseEntityFrameworkSink(
        this LoggerConfiguration loggerConfiguration,
        Func<LoggerDbContext> dbContextProvider,
        IFormatProvider? formatProvider = null)
    {
        return IsUseEntityFrameworkCoreLogger 
            ? loggerConfiguration.WriteTo.Sink(new EntityFrameworkCoreSink(dbContextProvider, formatProvider)) 
            : loggerConfiguration;
    }
}