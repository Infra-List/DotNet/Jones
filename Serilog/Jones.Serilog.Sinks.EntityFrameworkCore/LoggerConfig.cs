using System;

namespace Jones.Serilog.Sinks.EntityFrameworkCore;

public record LoggerConfig
{
    public int? RetentionPeriodDays { get; set; }
    public string? RetentionCheckCron { get; set; }
        
    private int _defaultLogsPageSize;
    public int DefaultLogsPageSize
    {
        get => _defaultLogsPageSize < 1 ? 50 : _defaultLogsPageSize;
        set => _defaultLogsPageSize = value;
    }
        
    private int _maxLogsPageSize;
    public int MaxLogsPageSize
    {
        get => _maxLogsPageSize < 1 ? 200 : _maxLogsPageSize;
        set => _maxLogsPageSize = value;
    }

    public int GetPageSize(int? pageSize) => Math.Min(pageSize ?? DefaultLogsPageSize, MaxLogsPageSize);
}