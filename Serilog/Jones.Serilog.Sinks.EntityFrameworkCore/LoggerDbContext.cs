﻿using System;
using Jones.Serilog.Sinks.EntityFrameworkCore.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Jones.Serilog.Sinks.EntityFrameworkCore;

public class LoggerDbContext : DbContext
{
    public DbSet<Log> Log { get; set; }

    public LoggerDbContext(DbContextOptions<LoggerDbContext> options) : base(options)
    {
            
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var loggerFactory = new LoggerFactory ();
        loggerFactory.AddProvider(new LoggerDbContextLoggerProvider());
        optionsBuilder.UseLoggerFactory(loggerFactory);
    }
}

internal class LoggerDbContextLoggerProvider : ILoggerProvider {
    public ILogger CreateLogger (string categoryName) => new LoggerDbContextLogger (categoryName);
    public void Dispose () { }
}

internal class LoggerDbContextLogger : ILogger {
    private readonly string _categoryName;

    public LoggerDbContextLogger (string categoryName) => _categoryName = categoryName;

    public bool IsEnabled (LogLevel logLevel) => logLevel == LogLevel.Error;

    public void Log<TState> (LogLevel logLevel, 
        EventId eventId,
        TState state, 
        Exception exception, 
        Func<TState, Exception, string> formatter) {
        Console.WriteLine ();
        Console.WriteLine ($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}] - {_categoryName} - {formatter(state, exception)}");
    }
        
    public IDisposable BeginScope<TState> (TState state) => new Empty();
}

internal class Empty : IDisposable
{
    public void Dispose()
    {
    }
}