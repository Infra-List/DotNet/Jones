using Serilog.Events;

namespace Jones.Serilog.Sinks.EntityFrameworkCore.Dto;

public record LogDto
{
    public int Id { get; }
        
    public LogEventLevel Level { get; }

    public string Message { get; }

    public string? Exception { get; }

    public string LogEvent { get; }

    public long TimeStamp { get; }

    public LogDto(int id, LogEventLevel level, string message, string? exception, string logEvent, long timeStamp)
    {
        Id = id;
        Level = level;
        Message = message;
        Exception = exception;
        LogEvent = logEvent;
        TimeStamp = timeStamp;
    }
}