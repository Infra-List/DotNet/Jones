using Serilog.Events;

namespace Jones.Serilog.Sinks.EntityFrameworkCore.Dto;

public record LogListItemDto
{
    public int Id { get; }
        
    public LogEventLevel Level { get; }

    public string Message { get; }

    public string? Exception { get; }
        
    public long TimeStamp { get; }

    public LogListItemDto(int id, LogEventLevel level, string message, string? exception, long timeStamp)
    {
        Id = id;
        Level = level;
        Message = message;
        Exception = exception;
        TimeStamp = timeStamp;
    }
}