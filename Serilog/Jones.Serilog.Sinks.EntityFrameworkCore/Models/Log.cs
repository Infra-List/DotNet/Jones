﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Serilog.Events;

namespace Jones.Serilog.Sinks.EntityFrameworkCore.Models;

[Index(nameof(TimeStamp))]
public class Log
{
    [Key]
    public int Id { get; set; }
        
    public LogEventLevel Level { get; set; }

    public string Message { get; set; }

    public string? Exception { get; set; }

    public string LogEvent { get; set; }

    public long TimeStamp { get; set; }
}