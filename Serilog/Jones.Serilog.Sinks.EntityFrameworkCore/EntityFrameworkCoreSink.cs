using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Jones.Serilog.Sinks.EntityFrameworkCore.Models;
using Microsoft.EntityFrameworkCore;
using Serilog.Core;
using Serilog.Debugging;
using Serilog.Events;
using Serilog.Formatting.Json;

namespace Jones.Serilog.Sinks.EntityFrameworkCore;

public class EntityFrameworkCoreSink : BatchProvider, ILogEventSink
{
    private readonly IFormatProvider? _formatProvider;
    private readonly Func<DbContext> _dbContextProvider;
    private readonly JsonFormatter _jsonFormatter;
    private static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

    public EntityFrameworkCoreSink(Func<DbContext> dbContextProvider, IFormatProvider? formatProvider)
    {
        _formatProvider = formatProvider;
        _dbContextProvider = dbContextProvider ?? throw new ArgumentNullException(nameof(dbContextProvider));
        _jsonFormatter = new JsonFormatter(formatProvider: formatProvider);
    }

    public void Emit(LogEvent logEvent)
    {
        PushEvent(logEvent);
    }

    protected override async Task<bool> WriteLogEventAsync(ICollection<LogEvent>? logEventsBatch)
    {
        if (logEventsBatch == null || logEventsBatch.Count == 0)
            return true;
        await semaphoreSlim.WaitAsync().ConfigureAwait(false);
        try
        {
            var context = _dbContextProvider.Invoke();
            foreach (var logEvent in logEventsBatch)
            {
                var log = ConvertLogEventToLogRecord(logEvent);
                if (log != null)
                {
                    await context.Set<Log>().AddAsync(log);
                }
            }
            await context.SaveChangesAsync();
            return true;
        }
        catch (Exception e)
        {
            SelfLog.WriteLine(e.Message);
            return false;
        }
        finally
        {
            semaphoreSlim.Release();
        }
    }

    private Log? ConvertLogEventToLogRecord(LogEvent? logEvent)
    {
        if (logEvent == null)
        {
            return null;
        }

        return new Log
        {
            Level = logEvent.Level,
            Message = logEvent.RenderMessage(_formatProvider),
            Exception = logEvent.Exception?.ToString(),
            LogEvent = ConvertLogEventToJson(logEvent),
            TimeStamp = logEvent.Timestamp.ToUnixTimeMilliseconds()
        };
    }

    private string ConvertLogEventToJson(LogEvent logEvent)
    {
        var sb = new StringBuilder();
        using var writer = new StringWriter(sb);
        _jsonFormatter.Format(logEvent, writer);

        return sb.ToString();
    }
}