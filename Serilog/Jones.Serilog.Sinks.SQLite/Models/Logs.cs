﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Serilog.Events;

namespace Jones.Serilog.Sinks.SQLite.Models;

[Index(nameof(Timestamp))]
public class Logs
{
    [Key]
    public int Id { get; set; }

    public DateTime Timestamp { get; set; }
        
    public LogEventLevel Level { get; set; }

    public string? Exception { get; set; }

    public string RenderedMessage { get; set; }

    public string Properties { get; set; }
}