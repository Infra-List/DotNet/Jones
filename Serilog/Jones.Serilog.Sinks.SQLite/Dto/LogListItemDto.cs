using Serilog.Events;

namespace Jones.Serilog.Sinks.SQLite.Dto;

public record LogListItemDto
{
    public int Id { get; set; }

    public DateTime Timestamp { get; set; }
        
    public LogEventLevel Level { get; set; }

    public string? Exception { get; set; }

    public string RenderedMessage { get; set; }

    public LogListItemDto(int id, DateTime timestamp, LogEventLevel level, string? exception, string renderedMessage)
    {
        Id = id;
        Timestamp = timestamp;
        Level = level;
        Exception = exception;
        RenderedMessage = renderedMessage;
    }
}