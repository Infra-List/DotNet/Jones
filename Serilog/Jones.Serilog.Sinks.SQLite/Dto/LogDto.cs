using Serilog.Events;

namespace Jones.Serilog.Sinks.SQLite.Dto;

public record LogDto
{
    public int Id { get; set; }

    public DateTime Timestamp { get; set; }
        
    public LogEventLevel Level { get; set; }

    public string? Exception { get; set; }

    public string RenderedMessage { get; set; }

    public string Properties { get; set; }

    public LogDto(int id, DateTime timestamp, LogEventLevel level, string? exception, string renderedMessage, string properties)
    {
        Id = id;
        Timestamp = timestamp;
        Level = level;
        Exception = exception;
        RenderedMessage = renderedMessage;
        Properties = properties;
    }
}