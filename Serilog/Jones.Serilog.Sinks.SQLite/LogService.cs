using System.ComponentModel.DataAnnotations;
using Jones.EntityFrameworkCore.Extensions;
using Jones.Serilog.Sinks.SQLite.Dto;
using Jones.Serilog.Sinks.SQLite.Models;
using Jones.Specification;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Serilog.Events;

namespace Jones.Serilog.Sinks.SQLite;

public interface ILogService
{
    Task<Paging<LogListItemDto>> Page(LogQueryViewModel filter, PagingParams pager);
        
    Task<LogDto?> Get(int id);

    Task DataMaintenance(int? retentionPeriodDays = null);
    Task<int> Vacuum();
}
    
public class LogService : ILogService
{
    private readonly LoggerConfig _loggerConfig;

    private readonly LoggerDbContext _loggerDbContext;

    public LogService(IOptions<LoggerConfig> loggerConfig, LoggerDbContext loggerDbContext)
    {
        _loggerConfig = loggerConfig.Value;
        _loggerDbContext = loggerDbContext;
    }

    public async Task<Paging<LogListItemDto>> Page(LogQueryViewModel filter, PagingParams pager)
    {
        Specification<Logs> specification = new TrueSpecification<Logs>();
        if (filter.Id != null)
        {
            specification &= new DirectSpecification<Logs>(p => p.Id == filter.Id);
        }
        else
        {
            if (filter.Level != null)
            {
                specification &= new DirectSpecification<Logs>(p => p.Level == filter.Level);
            }

            if (!string.IsNullOrEmpty(filter.Content))
            {
                specification &= new DirectSpecification<Logs>(p => 
                    EF.Functions.Like(p.RenderedMessage, $"%{filter.Content}%") || EF.Functions.Like(p.Exception, $"%{filter.Content}%"));
                    
                // specification &= new DirectSpecification<Log>(p => p.Message.Contains(filter.Content) || (p.Exception != null && p.Exception.Contains(filter.Content)));
            }

            if (filter.FromTimestamp != null)
            {
                specification &= new DirectSpecification<Logs>(p => p.Timestamp >= filter.FromTimestamp);
            }

            if (filter.ToTimestamp != null)
            {
                specification &= new DirectSpecification<Logs>(p => p.Timestamp <= filter.ToTimestamp);
            }
        }
            
        var paging = await _loggerDbContext.Logs
            .Where(specification.SatisfiedBy())
            .OrderByDescending(p => p.Timestamp)
            .ThenByDescending(p => p.Id)
            .ToPagingAsync(pager.Page ?? 1, _loggerConfig.GetPageSize(pager.PageSize));

        return new Paging<LogListItemDto>(
            paging.Items.Select(log => new LogListItemDto(log.Id, log.Timestamp, log.Level, log.Exception, log.RenderedMessage)).ToArray(), 
            paging.Page, paging.PageSize, paging.TotalCount);
    }

    public async Task<LogDto?> Get(int id)
    {
        var log = await _loggerDbContext.Logs.FirstOrDefaultAsync(p => p.Id == id);
        return log == null ? null : new LogDto(log.Id, log.Timestamp, log.Level, log.Exception, log.RenderedMessage, log.Properties);
    }

    public async Task DataMaintenance(int? retentionPeriodDays = null)
    {
        retentionPeriodDays ??= _loggerConfig.RetentionPeriodDays;
        if (retentionPeriodDays == null)
        {
            return;
        }
            
        // var epoch = DateTimeOffset.Now.AddDays(-retentionPeriodDays.Value).ToUnixTimeMilliseconds();
        // await _loggerDbContext.Database.ExecuteSqlInterpolatedAsync($"DELETE FROM Log WHERE TimeStamp < {epoch}");
        
        var epoch = DateTime.UtcNow.AddDays(-retentionPeriodDays.Value);
        await _loggerDbContext.Logs
            .Where(log => log.Timestamp < epoch)
            .ExecuteDeleteAsync();
    }

    public Task<int> Vacuum() => _loggerDbContext.Database.ExecuteSqlRawAsync("VACUUM;");

    // private DateTime GetDateTime(long timeStamp) =>
    //     DateTimeOffset.FromUnixTimeMilliseconds(timeStamp).LocalDateTime;
}

public record LogQueryViewModel
{
    [Display(Name = "Id")]
    public int? Id { get; set; }
        
    [Display(Name = "等级")]
    [EnumDataType(typeof(LogEventLevel))]
    public LogEventLevel? Level { get; set; }

    [Display(Name = "内容")]
    public string? Content { get; set; }

    [Display(Name = "从")]
    public DateTime? FromTimestamp { get; set; }

    [Display(Name = "到")]
    public DateTime? ToTimestamp { get; set; }
}