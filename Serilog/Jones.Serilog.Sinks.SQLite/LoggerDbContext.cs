﻿using Jones.Serilog.Sinks.SQLite.Models;
using Microsoft.EntityFrameworkCore;
using Serilog.Events;

namespace Jones.Serilog.Sinks.SQLite;

public class LoggerDbContext : DbContext
{
    private const string TimestampFormat = "yyyy-MM-ddTHH:mm:ss.fff";
    
    public DbSet<Logs> Logs { get; set; }

    public LoggerDbContext(DbContextOptions<LoggerDbContext> options) : base(options)
    {
            
    }

    // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    // {
    //     var loggerFactory = new LoggerFactory ();
    //     loggerFactory.AddProvider(new LoggerDbContextLoggerProvider());
    //     optionsBuilder.UseLoggerFactory(loggerFactory);
    // }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Logs>().Property(logs => logs.Timestamp).HasConversion<string>(
                time => time.ToString(TimestampFormat), 
                timeString => DateTime.Parse(timeString));

        modelBuilder.Entity<Logs>().Property(logs => logs.Level).HasConversion<string>(
            level => level.ToString(), 
            levelString => Enum.GetValues<LogEventLevel>().FirstOrDefault(level => level.ToString() == levelString));
    }
}

// internal class LoggerDbContextLoggerProvider : ILoggerProvider {
//     public ILogger CreateLogger (string categoryName) => new LoggerDbContextLogger (categoryName);
//     public void Dispose () { }
// }
//
// internal class LoggerDbContextLogger : ILogger {
//     private readonly string _categoryName;
//
//     public LoggerDbContextLogger (string categoryName) => _categoryName = categoryName;
//
//     public bool IsEnabled (LogLevel logLevel) => logLevel == LogLevel.Error;
//
//     public void Log<TState> (LogLevel logLevel, 
//         EventId eventId,
//         TState state, 
//         Exception exception, 
//         Func<TState, Exception, string> formatter) {
//         Console.WriteLine ();
//         Console.WriteLine ($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}] - {_categoryName} - {formatter(state, exception)}");
//     }
//         
//     public IDisposable BeginScope<TState> (TState state) => new Empty();
// }
//
// internal class Empty : IDisposable
// {
//     public void Dispose()
//     {
//     }
// }