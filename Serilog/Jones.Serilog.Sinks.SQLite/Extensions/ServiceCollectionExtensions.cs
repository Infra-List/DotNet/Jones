using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.Serilog.Sinks.SQLite.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddSQLiteEntityFrameworkCoreLogger(this IServiceCollection services, IConfiguration configuration, string loggerDbConnection)
    {
        services.Configure<LoggerConfig>(configuration.GetSection("Logger"));
            
        services.AddDbContextPool<LoggerDbContext>(options => options
            .UseSqlite(loggerDbConnection)
            .EnableSensitiveDataLogging()
            .EnableDetailedErrors());
            
        services.AddTransient<ILogService, LogService>();
        
        return services;
    }
}