using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Jones.Serilog.Sinks.SQLite.Quartz;

[DisallowConcurrentExecution]
public class LoggerDataMaintenanceJob : IJob
{
    public const string Target = "日志文件维护";
    private const string MessagePrefix = $"{Target}Job";
    
    private readonly ILogger<LoggerDataMaintenanceJob> _logger;
    private readonly IServiceProvider _serviceProvider;

    public LoggerDataMaintenanceJob(ILogger<LoggerDataMaintenanceJob> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("[{MessagePrefix}] - 任务启动", MessagePrefix);
            
        using var scope = _serviceProvider.CreateScope();
        var logService = scope.ServiceProvider.GetService<ILogService>();
        if (logService == null)
        {
            _logger.LogWarning("{MessagePrefix} - 没有找到 ILogService", MessagePrefix);
            return;
        }
            
        try
        {
            await logService.DataMaintenance();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "{MessagePrefix} - 日志文件维护出错 - 错误信息[{Message}]", MessagePrefix, e.Message);
        }
    }
}