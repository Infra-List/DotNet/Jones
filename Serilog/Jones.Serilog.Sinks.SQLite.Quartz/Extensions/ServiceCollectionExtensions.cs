using Jones.Quartz;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.Serilog.Sinks.SQLite.Quartz.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddSQLiteEntityFrameworkCoreLoggerQuartz(this IServiceCollection services, string? retentionCheckCron)
    {
        services.AddJob<LoggerDataMaintenanceJob>(LoggerDataMaintenanceJob.Target, retentionCheckCron);
            
        return services;
    }
}