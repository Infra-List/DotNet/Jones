using Jones.Helper;
using Serilog;
using Serilog.Sinks.Grafana.Loki;

namespace Jones.Serilog.Sinks.Grafana.Loki.Extensions;

public static class ServiceCollectionExtensions
{
    // https://grafana.com/grafana/dashboards/17328-app-logs/
    public static LoggerConfiguration UseGrafanaLokiSinks(this LoggerConfiguration loggerConfiguration, SerilogGrafanaLokiConfig config, string app, string? instanceId, string? serviceNamespace)
    {
        if (config.IsLocalDistribution != true)
        {
            return loggerConfiguration;
        }
        
        var labels = new List<LokiLabel>
        {
            new()
            {
                Key = "app",
                Value = app
            }
        };
        
        if (!string.IsNullOrEmpty(instanceId))
        {
            labels.Add(new LokiLabel
            {
                Key = "id",
                Value = instanceId
            });
        }
        
        if (!string.IsNullOrEmpty(serviceNamespace))
        {
            labels.Add(new LokiLabel
            {
                Key = "namespace",
                Value = serviceNamespace
            });
        }

        var assemblyVersion = EntryAssemblyHelper.GetLiteAssemblyVersion();
        if (!string.IsNullOrEmpty(assemblyVersion))
        {
            labels.Add(new LokiLabel
            {
                Key = "version",
                Value = assemblyVersion
            });
        }
        
        return loggerConfiguration.WriteTo.GrafanaLoki(config.Uri, labels,
            credentials: config.Credentials == null 
                ? null 
                : new LokiCredentials
                {
                    Login = config.Credentials.User,
                    Password = config.Credentials.Password
                },
            tenant: config.Tenant, 
            period: config.PeriodSec == null ? null : TimeSpan.FromSeconds(config.PeriodSec.Value));
    }
}