using System.ComponentModel.DataAnnotations;

namespace Jones.Serilog.Sinks.Grafana.Loki;

public record SerilogGrafanaLokiConfig
{
    public bool? IsEnabled { get; set; }
    
    [Display(Name = "是否本地分发")]
    public bool? IsLocalDistribution { get; set; }
    
    public string Uri { get; set; }
    
    [Display(Name = "检查日志时间间隔", Prompt = "检查日志时间间隔，单位为秒")]
    public int? PeriodSec { get; set; }
    
    public string? Tenant { get; set; }
    
    public LokiCredentials? Credentials { get; set; }

    public record LokiCredentials
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}