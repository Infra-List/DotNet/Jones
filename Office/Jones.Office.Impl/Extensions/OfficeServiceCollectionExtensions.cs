using EasyOffice;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.Office.Impl.Extensions;

public static class OfficeServiceCollectionExtensions
{
    public static IServiceCollection AddOfficeService(this IServiceCollection services)
    {
        services.AddEasyOffice(new OfficeOptions());
        services.AddTransient<IExcelExportService, ExcelExportServiceImpl>();

        return services;
    }
}