using EasyOffice.Enums;
using EasyOffice.Models.Excel;
using Jones.Office.Types;

namespace Jones.Office.Impl;

public class ExcelExportServiceImpl : IExcelExportService
{
    private readonly EasyOffice.Interfaces.IExcelExportService _excelExportService;

    public ExcelExportServiceImpl(EasyOffice.Interfaces.IExcelExportService excelExportService)
    {
        _excelExportService = excelExportService;
    }
    
    // https://github.com/donnytian/Npoi.Mapper/blob/master/Npoi.Mapper/src/Npoi.Mapper/Mapper.cs
    // https://www.cnblogs.com/wucy/p/14125392.html
    public Task<byte[]> Export<T>(IEnumerable<T> data, string sheetName, ExcelType excelType) where T : class, new()
    {
        // var mapper = new Mapper();
        // using var stream = new MemoryStream();
        // mapper.Save(stream, data, sheetName, overwrite: false, xlsx: excelType == ExcelType.XLSX);
        // return Task.FromResult(stream.ToArray());

        return _excelExportService.ExportAsync(data.ToList(), new ExportOption<T>
        {
            DataRowStartIndex = 1, //数据行起始索引，默认1
            ExportType = excelType == ExcelType.XLSX ? ExportType.XLSX : ExportType.XLS, // 默认导出Excel类型，默认xlsx
            HeaderRowIndex = 0, //表头行索引，默认0
            SheetName = sheetName //页签名称，默认sheet1
        });
    }
}