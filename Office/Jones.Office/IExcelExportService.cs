using Jones.Office.Types;

namespace Jones.Office;

public interface IExcelExportService
{
    Task<byte[]> Export<T>(IEnumerable<T> data, string sheetName, ExcelType excelType) where T : class, new();
}