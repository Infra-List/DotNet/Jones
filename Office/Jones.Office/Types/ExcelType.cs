namespace Jones.Office.Types;

public enum ExcelType
{
    XLS,
    XLSX,
}