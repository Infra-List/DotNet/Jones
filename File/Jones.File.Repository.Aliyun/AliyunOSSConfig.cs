namespace Jones.File.Repository.Aliyun;

public record AliyunOSSConfig
{
    public string? Host { get; set; }
    public string Bucket { get; set; }
    public string Endpoint { get; set; }
    public string AccessKeyId { get; set; }
    public string AccessKeySecret { get; set; }
}