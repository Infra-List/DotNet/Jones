using System.Threading.Tasks;
using Jones.File.Repository.Aliyun.Entites;
using Microsoft.Extensions.Options;

namespace Jones.File.Repository.Aliyun;

public class AliyunOssServiceImpl : IAliyunOssService
{
    private readonly AliyunOSSConfig _config;
    
    public AliyunOssServiceImpl(IOptions<AliyunOSSConfig> config)
    {
        _config = config.Value;
    }
    
    public async Task<ClientPolicySignature> GetPostObjectSignature(string folder, string? host = null, string? endpoint = null, string? bucketName = null)
    {
        host ??= _config.Host ?? AliyunHelper.GetHost(endpoint ?? _config.Endpoint, bucketName ?? _config.Bucket);
        return AliyunHelper.GetPostObjectSignature(folder, host, endpoint ?? _config.Endpoint, _config.AccessKeyId, _config.AccessKeySecret);
    }
}