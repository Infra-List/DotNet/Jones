using System.Threading.Tasks;
using Jones.File.Repository.Aliyun.Entites;

namespace Jones.File.Repository.Aliyun
{
    public interface IAliyunOssService
    {
        Task<ClientPolicySignature> GetPostObjectSignature(string folder, string? host = null, string? endpoint = null, string? bucketName = null);
    }
}