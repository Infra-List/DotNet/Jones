using System.Collections.Generic;

namespace Jones.File.Repository.Aliyun.Entites
{
    public record AliyunOSSPolicy
    {
        public string Expiration { get; set; }
        
        public List<List<object>>? Conditions { get; set; }
    }
}