namespace Jones.File.Repository.Aliyun.Entites
{
    public class ClientPolicySignature
    {
        public string Host { get; set; }
        public string Folder { get; set; }
        public string OSSAccessKeyId { get; set; }
        public string Policy { get; set; }
        public string Signature { get; set; }
    }
}