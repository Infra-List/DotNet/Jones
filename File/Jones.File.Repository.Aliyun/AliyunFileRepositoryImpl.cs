using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Aliyun.OSS;
using Jones.File.Repository.Entites;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Jones.File.Repository.Aliyun
{
    public class AliyunFileRepositoryImpl : IFileRepository
    {
        private readonly ILogger<AliyunFileRepositoryImpl> _logger;
        private readonly AliyunOSSConfig _config;

        public AliyunFileRepositoryImpl(ILogger<AliyunFileRepositoryImpl> logger, IOptions<AliyunOSSConfig> config)
        {
            _logger = logger;
            _config = config.Value;
        }
        
        private OssClient GetOssClient(string? endpoint) => new (endpoint ?? _config.Endpoint, _config.AccessKeyId, _config.AccessKeySecret);

        public string GetHost(string? endpoint = null, string? bucketName = null)
        {
            return _config.Host ?? AliyunHelper.GetHost(endpoint ?? _config.Endpoint, bucketName ?? _config.Bucket);
        }

        public async Task<RepositoryStatus> GetRepositoryStatus(string? endpoint = null, string? bucketName = null)
        {
            var ossClient = GetOssClient(endpoint);
            var bucketStat = ossClient.GetBucketStat(bucketName ?? _config.Bucket);
            return new RepositoryStatus(Convert.ToInt64(bucketStat.Storage), Convert.ToInt64(bucketStat.ObjectCount));
        }

        public async Task<bool> CreateFolder(string path, string? endpoint = null, string? bucketName = null)
        {
            // https://developer.aliyun.com/ask/150822
            if (path.Last() != '/')
            {
                path += "/";
            }
            await Upload(path, new MemoryStream(), endpoint, bucketName);
            return true;
        }

        public async Task<string> Upload(string fileName, Stream content, string? endpoint = null, string? bucketName = null)
        {
            var ossClient = GetOssClient(endpoint);
            var putObjectResult = ossClient.PutObject(bucketName ?? _config.Bucket, fileName, content);
            if (putObjectResult.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new FileRepositoryException($"http status code : {(int)putObjectResult.HttpStatusCode}");
            }
            return fileName;
        }

        public async Task<string[]> Upload(UploadFileInfo[] fileList, string? endpoint = null, string? bucketName = null)
        {
            
            var uploadedList = new List<string>();
            foreach (var fileInfo in fileList)
            {
                try
                {
                    uploadedList.Add( await Upload(fileInfo.FileName, fileInfo.Content, endpoint, bucketName));
                }
                catch (Exception e)
                {
                    _logger.LogError(e,"[阿里云批量上传文件] - [上传文件出错：{FileName}] - 错误信息[{Message}]", fileInfo.FileName, e.Message);
                }
            }

            return uploadedList.ToArray();
        }

        public async Task<bool> Delete(string[] fileNames, string? endpoint = null, string? bucketName = null)
        {
            var ossClient = GetOssClient(endpoint);
            var deleteObjectsResult = ossClient.DeleteObjects(new DeleteObjectsRequest(bucketName ?? _config.Bucket, fileNames));
            if (deleteObjectsResult.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new FileRepositoryException($"http status code : {(int)deleteObjectsResult.HttpStatusCode}");
            }

            return true;
        }

        public async Task<RepositoryFileInfo[]> List(int? maxCount, string? prefix, string? marker, string? endpoint = null, string? bucketName = null)
        {
            var ossClient = GetOssClient(endpoint);
            var objectListing = ossClient.ListObjects(new ListObjectsRequest(bucketName ?? _config.Bucket)
            {
                Prefix = prefix,
                Marker = marker,
                MaxKeys = maxCount
            });
            
            if (objectListing.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new FileRepositoryException($"http status code : {(int)objectListing.HttpStatusCode}");
            }
            return objectListing.ObjectSummaries.Select(p => new RepositoryFileInfo(p.Key, p.Size, p.LastModified)).ToArray();
        }
    }
}