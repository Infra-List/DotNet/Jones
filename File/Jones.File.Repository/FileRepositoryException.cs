using System;
using System.Runtime.Serialization;

namespace Jones.File.Repository
{
    public class FileRepositoryException : Exception
    {
        public FileRepositoryException()
        {
        }

        protected FileRepositoryException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public FileRepositoryException(string message) : base(message)
        {
        }

        public FileRepositoryException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}