using System;

namespace Jones.File.Repository.Entites
{
    public record RepositoryFileInfo
    {
        public string FileName { get; }
        /// <summary>
        /// 文件大小，单位为：Byte
        /// </summary>
        public long Size { get; set; }
        
        public DateTime LastModifiedTime { get; }

        public RepositoryFileInfo(string fileName, long size, DateTime lastModifiedTime)
        {
            FileName = fileName;
            Size = size;
            LastModifiedTime = lastModifiedTime;
        }
    }
}