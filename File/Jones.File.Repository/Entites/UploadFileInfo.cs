using System.IO;

namespace Jones.File.Repository.Entites
{
    public record UploadFileInfo
    {
        public string FileName { get; }
        public Stream Content { get; }
        public long Size { get; }

        public UploadFileInfo(string fileName, Stream content, long size)
        {
            FileName = fileName;
            Content = content;
            Size = size;
        }
    }
}