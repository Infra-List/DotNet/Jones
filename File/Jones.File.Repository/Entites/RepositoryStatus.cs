namespace Jones.File.Repository.Entites
{
    public record RepositoryStatus
    {
        /// <summary>
        /// 空间大小，单位为：Byte
        /// </summary>
        public long Size { get; set; }
        
        public long FileCount { get; set; }

        public RepositoryStatus(long size, long fileCount)
        {
            Size = size;
            FileCount = fileCount;
        }
    }
}