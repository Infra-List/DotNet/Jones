using System.IO;
using System.Threading.Tasks;
using Jones.File.Repository.Entites;

namespace Jones.File.Repository;

public interface IFileRepository
{
    string GetHost(string? endpoint = null, string? bucketName = null);
        
    Task<RepositoryStatus> GetRepositoryStatus(string? endpoint = null, string? bucketName = null);
        
    Task<bool> CreateFolder(string path, string? endpoint = null, string? bucketName = null);

    /// <summary>
    /// 上传文件
    /// </summary>
    /// <param name="endpoint"></param>
    /// <param name="bucketName"></param>
    /// <param name="fileName">文件名（含文件夹）</param>
    /// <param name="content"></param>
    /// <returns>文件路径</returns>
    Task<string> Upload(string fileName, Stream content, string? endpoint = null, string? bucketName = null);
    Task<string[]> Upload(UploadFileInfo[] fileList, string? endpoint = null, string? bucketName = null);
        
    Task<bool> Delete(string[] fileNames, string? endpoint = null, string? bucketName = null);

    Task<RepositoryFileInfo[]> List(int? maxCount, string? prefix, string? marker, string? endpoint = null, string? bucketName = null);
}