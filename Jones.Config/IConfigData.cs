namespace Jones.Config;

public interface IConfigData
{
    string Version { get; set; }
    string ConfigKey { get; }
}