namespace Jones.Config;

public record ConfigInfo(string ConfigFile, Type ConfigType);