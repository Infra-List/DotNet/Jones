using Microsoft.Extensions.Configuration;

namespace Jones.Config.Extensions;

public static class ServiceCollectionExtensions
{
    public static void AddJsonConfig(this ConfigurationManager configuration, string configFileNameNotIncludeExtension, string? configPath, bool isOptional, string? environmentName = null)
    {
        configuration
            .AddJsonFile(string.IsNullOrEmpty(configPath) 
                ? Path.Combine(AppContext.BaseDirectory, $"{configFileNameNotIncludeExtension}.json") 
                : Path.Combine(AppContext.BaseDirectory, configPath, $"{configFileNameNotIncludeExtension}.json"), 
                optional: isOptional, reloadOnChange: true);

        if (!string.IsNullOrEmpty(environmentName))
        {
            configuration
                .AddJsonFile(string.IsNullOrEmpty(configPath) 
                    ? Path.Combine(AppContext.BaseDirectory, $"{configFileNameNotIncludeExtension}.{environmentName}.json")
                    : Path.Combine(AppContext.BaseDirectory, configPath, $"{configFileNameNotIncludeExtension}.{environmentName}.json"), 
                    optional: true, reloadOnChange: true);
        }
    }
}