using Microsoft.Extensions.Configuration;

namespace Jones.Config.Extensions;

public static class ConfigurationExtensions
{
    public static T GetConfig<T>(this IConfiguration configuration) where T : IConfigData, new()
    {
        var config = new T();
        return configuration.GetConfig(config);
    }
    
    public static T GetConfig<T>(this IConfiguration configuration, T config) where T : IConfigData
    {
        configuration.GetSection(config.ConfigKey).Bind(config);
        return config;
    }
}