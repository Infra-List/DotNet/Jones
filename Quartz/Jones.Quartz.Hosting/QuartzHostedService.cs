using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Jones.Scheduler;
using Jones.Scheduler.Dto;
using Jones.Scheduler.Types;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Spi;

namespace Jones.Quartz.Hosting;

public class QuartzHostedService : IHostedService, ISchedulerService
{
    private readonly ILogger<QuartzHostedService> _logger;
    private readonly ISchedulerFactory _schedulerFactory;
    private readonly IJobFactory _jobFactory;
    private readonly IEnumerable<JobSchedule> _injectJobSchedules;
    private List<JobSchedule>? _allJobSchedules;

    private readonly ConcurrentDictionary<string, SchedulerJobStatistics> _jobStatistics = new();
        
    public IScheduler? Scheduler { get; set; }

    private CancellationToken? _cancellationToken;
    
    public QuartzHostedService(ILogger<QuartzHostedService> logger, ISchedulerFactory schedulerFactory, IJobFactory jobFactory, IEnumerable<JobSchedule> jobSchedules)
    {
        _logger = logger;
        _schedulerFactory = schedulerFactory;
        _jobFactory = jobFactory;
        _injectJobSchedules = jobSchedules;
    }

    public async Task StartAsync()
    {
        if (_cancellationToken != null)
        {
            await StartAsync(_cancellationToken.Value);
        }
    }
    /// <summary>
    /// 啟動排程器
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        if (Scheduler == null || Scheduler.IsShutdown)
        {
            // 存下 cancellation token 
            _cancellationToken = cancellationToken;

            // 先加入在 startup 註冊注入的 Job 工作
            _allJobSchedules = new List<JobSchedule>();
            _allJobSchedules.AddRange(_injectJobSchedules);

            // 初始排程器 Scheduler
            Scheduler = await _schedulerFactory.GetScheduler(cancellationToken);
            Scheduler.JobFactory = _jobFactory;


            // 逐一將工作項目加入排程器中 
            foreach (var jobSchedule in _allJobSchedules)
            {
                var jobDetail = CreateJobDetail(jobSchedule);
                var trigger = CreateTrigger(jobSchedule);
                await Scheduler.ScheduleJob(jobDetail, trigger, cancellationToken);
                jobSchedule.JobStatus = JobStatus.Scheduled;
            }

            // 啟動排程
            await Scheduler.Start(cancellationToken);
        }
    }

    /// <summary>
    /// 停止排程器
    /// </summary>
    /// <returns></returns>
    public async Task StopAsync()
    {
        if (_cancellationToken != null)
        {
            await StopAsync(_cancellationToken.Value);
        }
    }
    public async Task StopAsync(CancellationToken cancellationToken)
    {
        if (Scheduler != null && !Scheduler.IsShutdown)
        {
            _logger.LogInformation("停止 Scheduler");
            await Scheduler.Shutdown(cancellationToken);
        }
    }

    /// <summary>
    /// 取得所有作業的最新狀態
    /// </summary>
    public async Task<IEnumerable<SchedulerJob>> GetSchedulerJobs()
    {
        var jobScheduleList = await GetJobSchedules();
        return jobScheduleList?.Select(p => p.ToDto(_jobStatistics.ContainsKey(p.JobName) ? _jobStatistics[p.JobName] : null)) ?? Enumerable.Empty<SchedulerJob>();
    }

    public async Task<IEnumerable<JobSchedule>?> GetJobSchedules()
    {
        if (Scheduler?.IsShutdown == true)
        {
            // 排程器停止時更新各工作狀態為停止
            if (_allJobSchedules != null)
            {
                foreach (var jobSchedule in _allJobSchedules)
                {
                    jobSchedule.JobStatus = JobStatus.Stopped;
                }
            }
        }
        else
        {
            // 取得目前正在執行的 Job 來更新各 Job 狀態
            if (Scheduler != null)
            {
                var executingJobs = await Scheduler.GetCurrentlyExecutingJobs();
                if (_allJobSchedules != null)
                {
                    foreach (var jobSchedule in _allJobSchedules)
                    {
                        var isRunning = executingJobs.FirstOrDefault(j => j.JobDetail.Key.Name == jobSchedule.JobName) != null;
                        jobSchedule.JobStatus = isRunning ? JobStatus.Running : JobStatus.Scheduled;
                    }
                }
            }
        }

        return _allJobSchedules;
    }

    public async Task RunStatistics(string jobName)
    {
        var schedulerJobStatistics = _jobStatistics.ContainsKey(jobName) ? _jobStatistics[jobName] : null;
        if (schedulerJobStatistics == null)
        {
            schedulerJobStatistics = new SchedulerJobStatistics
            {
                RunCount = 1
            };
            _jobStatistics.TryAdd(jobName, schedulerJobStatistics);
        }
        else
        {
            schedulerJobStatistics.RunCount += 1;
        }

        schedulerJobStatistics.LastRunTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();
    }

    public Task<bool> IsJobExists<T>() => IsJobExists(QuartzExtensions.GetJobName<T>());
    public async Task<bool> IsJobExists(string jobName) => Scheduler != null && await Scheduler.CheckExists(GetJobKey(jobName));

    /// <summary>
    /// 手動觸發作業
    /// </summary>
    public Task TriggerJob<T>(IDictionary<string, object>? data = null) => TriggerJob(QuartzExtensions.GetJobName<T>(), data);
    public async Task TriggerJob(string jobName, IDictionary<string, object>? data = null)
    {
        if (Scheduler != null && !Scheduler.IsShutdown)
        {
            _logger.LogInformation("{JobName} - TriggerJob", jobName);
            await (data == null
                ? Scheduler.TriggerJob(GetJobKey(jobName), _cancellationToken ?? default (CancellationToken))
                : Scheduler.TriggerJob(GetJobKey(jobName), new JobDataMap(data), _cancellationToken ?? default (CancellationToken)));
        }
    }

    /// <summary>
    /// 手動中斷作業
    /// </summary>
    public Task InterruptJob<T>() => InterruptJob(QuartzExtensions.GetJobName<T>());
    public async Task InterruptJob(string jobName)
    {
        if (Scheduler != null && !Scheduler.IsShutdown)
        {
            var targetExecutingJob = await GetExecutingJob(jobName);
            if (targetExecutingJob != null)
            {
                _logger.LogInformation("{JobName} - InterruptJob", jobName);
                await Scheduler.Interrupt(GetJobKey(jobName));
            }
        }
    }

    private static JobKey GetJobKey<T>() => GetJobKey(QuartzExtensions.GetJobName<T>());

    private static JobKey GetJobKey(string jobName) => new (jobName);

    /// <summary>
    /// 取得特定執行中的作業
    /// </summary>
    private async Task<IJobExecutionContext?> GetExecutingJob(string jobName)
    {
        if (Scheduler == null) return null;
        var executingJobs = await Scheduler.GetCurrentlyExecutingJobs();
        return executingJobs.FirstOrDefault(j => j.JobDetail.Key.Name == jobName);

    }

    /// <summary>
    /// 建立作業細節 (後續會透過 JobFactory 依此資訊從 DI 容器取出 Job 實體)
    /// </summary>
    private IJobDetail CreateJobDetail(JobSchedule jobSchedule)
    {
        var jobType = jobSchedule.JobType;
        var jobDetail = JobBuilder
            .Create(jobType)
            .WithIdentity(jobSchedule.JobName)  
            .WithDescription(jobType.Name)
            .Build();

        // 可以在建立 job 時傳入資料給 job 使用
        jobDetail.JobDataMap.Put("Payload", jobSchedule);

        return jobDetail;
    }

    /// <summary>
    /// 產生觸發器
    /// </summary>
    /// <param name="schedule"></param>
    /// <returns></returns>
    private ITrigger CreateTrigger(JobSchedule schedule)
    {
        var triggerBuilder = TriggerBuilder
            .Create()
            .WithIdentity($"{schedule.JobName}.trigger")
            .WithCronSchedule(schedule.CronExpression ?? "59 59 23 31 12 ? 2099")
            .WithDescription(schedule.Description);
        // if (schedule.CronExpression != null)
        // {
        //     triggerBuilder = triggerBuilder.WithCronSchedule(schedule.CronExpression);
        // }
        return schedule.SetTriggerBuilder(triggerBuilder).Build();
    }
}