using Jones.Scheduler;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.Quartz.Hosting;

public static class QuartzHostingExtensions
{
    public static IServiceCollection AddQuartzHostedService(this IServiceCollection services)
    {
        services.AddSingleton<QuartzHostedService>();
        services.AddHostedService(provider => provider.GetService<QuartzHostedService>());
        return services;
    }
        
    public static IServiceCollection AddQuartzHostedServiceWithSchedulerService(this IServiceCollection services)
    {
        return services
            .AddQuartzHostedService()
            .AddSingleton<ISchedulerService>(provider => provider.GetRequiredService<QuartzHostedService>());
    }
}