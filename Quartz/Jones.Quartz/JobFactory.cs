using System;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Spi;

namespace Jones.Quartz;

public class JobFactory : IJobFactory
{
    private readonly IServiceProvider _serviceProvider;


    public JobFactory(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
    }


    public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
    {
        var jobType = bundle.JobDetail.JobType;

        // 从 DI 容器取出指定 Job Type 实例
        return _serviceProvider.GetRequiredService(jobType) as IJob  ?? throw new ArgumentNullException(nameof(jobType));
    }

    public void ReturnJob(IJob job)
    {
        var disposable = job as IDisposable;
        disposable?.Dispose();
    }
}