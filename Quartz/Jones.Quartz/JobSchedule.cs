using System;
using Jones.Scheduler.Dto;
using Jones.Scheduler.Types;
using Quartz;

namespace Jones.Quartz;

/// <summary>
/// 调度器
/// </summary>
public class JobSchedule
{
    public JobSchedule(string jobName, Type jobType, string? description, string? cronExpression, Func<TriggerBuilder, TriggerBuilder>? onSetTriggerBuilder)
    {
        JobName = jobName;
        JobType = jobType;
        Description = description;
        CronExpression = cronExpression;
        OnSetTriggerBuilder = onSetTriggerBuilder;
    }

    /// <summary>
    /// Job名称
    /// </summary>
    public string JobName { get; private set; }

    /// <summary>
    /// Job类型
    /// </summary>
    public Type JobType { get; private set; }
        
    public string? Description { get; private set; }

    /// <summary>
    /// Cron表示式
    /// </summary>
    public string? CronExpression { get; private set; }
        
    public Func<TriggerBuilder, TriggerBuilder>? OnSetTriggerBuilder { get; private set; }

    /// <summary>
    /// Job状态
    /// </summary>
    public JobStatus JobStatus { get; set; } = JobStatus.Init;

    public TriggerBuilder SetTriggerBuilder(TriggerBuilder triggerBuilder) =>
        OnSetTriggerBuilder != null ? OnSetTriggerBuilder(triggerBuilder) : triggerBuilder;

    public SchedulerJob ToDto(SchedulerJobStatistics? statistics)
    {
        var next = CronExpression == null ? null : new CronExpression(CronExpression).GetNextValidTimeAfter(DateTimeOffset.UtcNow);
        return new SchedulerJob(JobName, JobType.FullName ?? JobType.Name, Description, CronExpression, next?.UtcDateTime, JobStatus, statistics);
    }
}