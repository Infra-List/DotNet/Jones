using System;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace Jones.Quartz;

public static class QuartzExtensions
{
    public static IServiceCollection AddQuartz(this IServiceCollection services, bool isIgnoreSchedulerFactory = false)
    {
        services.AddSingleton<IJobFactory, JobFactory>();
        if (!isIgnoreSchedulerFactory)
        {
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
        }
        return services;
    }

    public static IServiceCollection AddJob<T>(this IServiceCollection services, string? description, string? cronExpression, 
        Func<TriggerBuilder, TriggerBuilder>? onSetTriggerBuilder = null, string? jobName = null) where T : class
    {
        services.AddSingleton<T>();
        services.AddSingleton(new JobSchedule(jobName ?? GetJobName<T>(), typeof(T), description, cronExpression, onSetTriggerBuilder));
        return services;
    }

    public static string GetJobName<T>() => typeof(T).Name;
}