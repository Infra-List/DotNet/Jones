using Microsoft.EntityFrameworkCore;

namespace Jones.EntityFrameworkCore.Extensions;

public static class QueryableExtensions
{
    public static async Task<Paging<T>> ToPagingAsync<T>(this IQueryable<T> source,
        int page, int pageSize, CancellationToken cancellationToken = default)
    {
        var totalCount = await source.CountAsync(cancellationToken).ConfigureAwait(false);
        var items = totalCount == 0 ? [] : await source.Skip((page - 1) * pageSize).Take(pageSize).ToArrayAsync(cancellationToken).ConfigureAwait(false);
        return new Paging<T>(items, page, pageSize, totalCount);
    }
    
    public static Paging<T> ToPaging<T>(this IQueryable<T> source, int page, int pageSize)
    {
        var totalCount = source.Count();
        var items = totalCount == 0 ? [] : source.Skip((page - 1) * pageSize).Take(pageSize).ToArray();
        return new Paging<T>(items, page, pageSize, totalCount);
    }
}