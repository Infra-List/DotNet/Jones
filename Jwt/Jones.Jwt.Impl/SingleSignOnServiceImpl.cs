using Microsoft.AspNetCore.Http;

namespace Jones.Jwt.Impl;

public class SingleSignOnServiceImpl : ISingleSignOnService
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IJwtBearerProvider _jwtBearerProvider;

    public SingleSignOnServiceImpl(IHttpContextAccessor httpContextAccessor, IJwtBearerProvider jwtBearerProvider)
    {
        _httpContextAccessor = httpContextAccessor;
        _jwtBearerProvider = jwtBearerProvider;
    }
    
    private string? GetAccessToken() => _httpContextAccessor.HttpContext?.Request.Headers["Authorization"].FirstOrDefault()?.Remove(0, "Bearer ".Length) 
                                        ?? _httpContextAccessor.HttpContext?.Request.Query.FirstOrDefault(p => p.Key == _jwtBearerProvider.AccessTokenQueryName).Value;
    
    public int? GetCurrentId()
    {
        var accessToken = GetAccessToken();
        return accessToken == null ? null : _jwtBearerProvider.GetCurrentId(accessToken);
    }
    
    public TR[]? GetRoles<TR>()
    {
        var accessToken = GetAccessToken();
        return accessToken == null ? null : _jwtBearerProvider.GetRoles<TR>(accessToken);
    }
}