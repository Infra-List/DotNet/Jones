using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Jones.Jwt.Dto;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Jones.Jwt.Impl;

public class JwtBearerProviderImpl : IJwtBearerProvider
{
    private readonly AuthenticationBearerConfig _config;

    public JwtBearerProviderImpl(IOptions<AuthenticationBearerConfig> config)
    {
        _config = config.Value;
    }

    public JwtIdentityToken Generate(int id, Claim[]? claims)
    {
        var claimList = claims == null ? new List<Claim>() : claims.ToList();
        claimList.Add(new Claim(JwtRegisteredClaimNames.Sub, id.ToString()));
        if (_config.ValidAudiences?.Any() == true)
        {
            claimList.AddRange(_config.ValidAudiences.Select(p => new Claim(JwtRegisteredClaimNames.Aud, p)));
        }
        
        var token = new JwtSecurityToken
        (
            issuer: _config.ValidIssuer,
            claims: claimList,
            expires: DateTime.UtcNow.AddSeconds(_config.Expires),
            notBefore: DateTime.UtcNow,
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.SigningKey)), SecurityAlgorithms.HmacSha256)
        );
        
        var accessToken = new JwtSecurityTokenHandler().WriteToken(token);
        return new JwtIdentityToken("Bearer", accessToken, _config.Expires, null);
    }

    private JwtSecurityToken? GetJwtSecurityToken(string accessToken)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        return tokenHandler.ReadToken(accessToken) as JwtSecurityToken;
    }

    public int? GetCurrentId(string accessToken)
    {
        if (string.IsNullOrEmpty(accessToken))
        {
            return null;
        }
        
        var jwtToken = GetJwtSecurityToken(accessToken);
        // var claims = jwtToken?.Claims.ToArray();
        // var claim = claims?.FirstOrDefault(p => p.Type == ClaimTypes.SerialNumber);
        if (int.TryParse(jwtToken?.Subject, out var id))
        {
            return id;
        }
        return null;
    }

    public string AccessTokenQueryName => _config.AccessTokenQueryName;
    
    public TR[]? GetRoles<TR>(string accessToken)
    {
        if (string.IsNullOrEmpty(accessToken))
        {
            return null;
        }
        
        var jwtToken = GetJwtSecurityToken(accessToken);
        var roleStrings = jwtToken?.Claims.Where(p => p.Type == ClaimTypes.Role).Select(p => p.Value).ToArray();
        return roleStrings == null 
            ? null 
            : Enum.GetValues(typeof(TR)).Cast<TR>().Where(p => roleStrings.Contains(p?.ToString())).ToArray();
    }
}