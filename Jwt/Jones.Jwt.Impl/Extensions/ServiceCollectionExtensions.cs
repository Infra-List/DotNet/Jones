using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Jones.Jwt.Impl.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddJwtService(this IServiceCollection services)
    {
        services.AddTransient<IJwtBearerProvider, JwtBearerProviderImpl>();
        services.AddScoped<ISingleSignOnService, SingleSignOnServiceImpl>();
        return services;
    }

    public static AuthenticationBuilder AddJwtBearer(this AuthenticationBuilder builder, AuthenticationBearerConfig config)
    {
        return builder.AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = config.ValidAudiences?.Any() == true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = config.ValidIssuer,
                ValidAudiences = config.ValidAudiences,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.SigningKey))
            };
            options.Events = new JwtBearerEvents
            {
                OnMessageReceived = context =>
                {
                    if (!string.IsNullOrEmpty(config.AccessTokenQueryName) && context.Request.Query.TryGetValue(config.AccessTokenQueryName, out var token))
                    {
                        context.Token = token;
                    }
                    return Task.CompletedTask;
                }
            };
        });
    }
}