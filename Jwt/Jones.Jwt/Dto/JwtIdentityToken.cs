namespace Jones.Jwt.Dto;

public record JwtIdentityToken(string TokenType, string AccessToken, int ExpiresIn, string? Scope);