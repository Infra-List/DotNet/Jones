namespace Jones.Jwt;

public record AuthenticationBearerConfig
{
    public string[]? ValidAudiences { get; set; }
    public string? ValidIssuer { get; set; }
    public string SigningKey { get; set; }
    public int Expires { get; set; }
    public string AccessTokenQueryName { get; set; }
}