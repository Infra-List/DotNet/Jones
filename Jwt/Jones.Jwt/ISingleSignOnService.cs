namespace Jones.Jwt;

public interface ISingleSignOnService
{
    int? GetCurrentId();
    TR[]? GetRoles<TR>();
}