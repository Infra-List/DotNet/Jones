using System.Security.Claims;
using Jones.Jwt.Dto;

namespace Jones.Jwt;

public interface IJwtBearerProvider
{
    JwtIdentityToken Generate(int id, Claim[]? claims);
    int? GetCurrentId(string accessToken);
    string AccessTokenQueryName { get; }
    TR[]? GetRoles<TR>(string accessToken);
}