using Microsoft.Extensions.DependencyInjection;

namespace Jones.Quill.Impl.Extensions;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddQuillService(this IServiceCollection services)
    {
        services.AddTransient<IQuillService, QuillServiceImpl>();
        return services;
    }
}