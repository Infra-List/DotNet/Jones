using Newtonsoft.Json.Linq;
using Quill.Delta;

namespace Jones.Quill.Impl;

public class QuillServiceImpl : IQuillService
{
    public string ToHtml(string json)
    {
        var htmlConverter = new HtmlConverter(JArray.Parse(json));
        return htmlConverter.Convert();
    }
}