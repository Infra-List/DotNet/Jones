namespace Jones.Quill;

public interface IQuillService
{
    string ToHtml(string json);
}