﻿using Microsoft.AspNetCore.Identity;

namespace Jones.Cryptography;

// https://github.com/dotnet/AspNetCore/blob/main/src/Identity/Extensions.Core/src/PasswordHasher.cs
public static class PasswordHasher
{
    private static PasswordHasher<object?> _passwordHasher = new();

    public static void SetPasswordHasher(PasswordHasher<object?> passwordHasher)
    {
        _passwordHasher = passwordHasher;
    }

    public static string HashPassword(string password) => _passwordHasher.HashPassword(default, password);

    public static PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword) =>
        _passwordHasher.VerifyHashedPassword(default, hashedPassword, providedPassword);
}