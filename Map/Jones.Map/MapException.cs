using System;

namespace Jones.Map;

public class MapException : Exception
{
    public MapException(string? message) : base(message)
    {
            
    }
}