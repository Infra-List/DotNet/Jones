using System.ComponentModel.DataAnnotations;

namespace Jones.Map.Dto;

public record ReGeoResultDto
{
    [Display(Name = "省份")]
    public string Province { get; set; }
        
    [Display(Name = "城市")]
    public string City { get; set; }
        
    [Display(Name = "区")]
    public string Dstrict { get; set; }
        
    [Display(Name = "乡镇/街道")]
    public string Township { get; set; }
        
    [Display(Name = "区域代码")]
    public string DivisionCode { get; set; }
}