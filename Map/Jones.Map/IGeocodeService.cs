using System.Threading.Tasks;
using Jones.Map.Dto;

namespace Jones.Map;

public interface IGeocodeService
{
    Task<ReGeoResultDto> ReGeo(double longitude, double latitude);
}