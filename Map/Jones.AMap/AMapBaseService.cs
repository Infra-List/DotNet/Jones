using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace Jones.AMap;

public abstract class AMapBaseService
{
    public const string HttpClient = "AMapServiceHttpClient";
    private const string _baseUrl = "https://restapi.amap.com/";
        
    private readonly AMapConfig _aMapConfig;
    private readonly IHttpClientFactory _httpClientFactory;
        
    private readonly Dictionary<string, object> _defaultQueries;

    protected AMapBaseService(IOptions<AMapConfig> aMapConfig, IHttpClientFactory httpClientFactory)
    {
        _aMapConfig = aMapConfig.Value;
        _httpClientFactory = httpClientFactory;
            
        _defaultQueries = new Dictionary<string, object>
        {
            { "key", _aMapConfig.Key },
            { "output", "JSON" }
        };
    }

    protected HttpClient GetClient() => _httpClientFactory.CreateClient(HttpClient);
        
    protected Task<T?> Get<T>(string path, IDictionary<string , object>? query) =>
        GetClient().GetFromJsonAsync<T>(CreateGetMethodUrl(path, query));

    private string CreateGetMethodUrl(string path, IDictionary<string, object>? query)
    {
        var data = query?.Concat(_defaultQueries)
            .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
        var parameters = data == null ? null : string.Join("&", data.Select(p => p.Key + "=" + (p.Value == null ? "" : Uri.EscapeDataString(p.Value.ToString()))));
        parameters = parameters == string.Empty ? null : $"?{parameters}";
        return $"{_baseUrl}{path}{parameters}";
    }
}