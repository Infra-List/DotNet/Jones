namespace Jones.AMap;

public record AMapConfig
{
    public string Key { get; set; }
}