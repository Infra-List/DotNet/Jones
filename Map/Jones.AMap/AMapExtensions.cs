using Jones.Map;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.AMap;

public static class AMapExtensions
{
    public static IServiceCollection AddAMapService(this IServiceCollection services, IConfiguration configuration)
    {
            
        services.Configure<AMapConfig>(configuration.GetSection("AMap"));
        services.AddHttpClient(AMapBaseService.HttpClient);
        services.AddTransient<IGeocodeService, GeocodeServiceImpl>();
        return services;
    }
}