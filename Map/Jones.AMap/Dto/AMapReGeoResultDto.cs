namespace Jones.AMap.Dto;

public record AMapReGeoResultDto
{
    public string Status { get; set; }
    public Regeocode Regeocode { get; set; }
    public string Info { get; set; }
    public string Infocode { get; set; }
}
    
public class StreetNumber
{
    public string Number { get; set; }
    public string Location { get; set; }
    public string Direction { get; set; }
    public string Distance { get; set; }
    public string Street { get; set; }
}

public class AddressComponent
{
    public string City { get; set; }
    public string Province { get; set; }
    public string Adcode { get; set; }
    public string District { get; set; }
    public string Towncode { get; set; }
    public StreetNumber StreetNumber { get; set; }
    public string Country { get; set; }
    public string Township { get; set; }
    public string Citycode { get; set; }
}

public class Regeocode
{
    public AddressComponent AddressComponent { get; set; }
    public string FormattedAddress { get; set; }
}