using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Jones.AMap.Dto;
using Jones.Map;
using Jones.Map.Dto;
using Microsoft.Extensions.Options;

namespace Jones.AMap;

public class GeocodeServiceImpl : AMapBaseService, IGeocodeService
{
        
    public GeocodeServiceImpl(IOptions<AMapConfig> aMapConfig, IHttpClientFactory httpClientFactory) : base(aMapConfig, httpClientFactory)
    {
    }
        
    public async Task<ReGeoResultDto> ReGeo(double longitude, double latitude)
    {
        var result = await Get<AMapReGeoResultDto>("v3/geocode/regeo", new Dictionary<string, object>
        {
            { "location", $"{longitude},{latitude}" },
            { "extensions", "base" },
            { "batch", false }
        });
        if (result is not { Status: "1" })
        {
            throw new MapException("逆地理编码失败");
        }

        var data = result.Regeocode;
        return new ReGeoResultDto
        {
            Province = data.AddressComponent.Province,
            City = data.AddressComponent.City,
            Dstrict = data.AddressComponent.District,
            Township = data.AddressComponent.Township,
            DivisionCode = data.AddressComponent.Adcode
        };
    }
}