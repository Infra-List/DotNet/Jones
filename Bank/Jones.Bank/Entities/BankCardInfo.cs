namespace Jones.Bank.Entities;

public record BankCardInfo(string Code, string Name, BankCardNumberPatterns[]? Patterns);

public record BankCardNumberPatterns(string BankCardType, string Reg);