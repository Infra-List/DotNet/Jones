namespace Jones.Bank.Dto;

public record BankInfoDto(string Code, string Name, string Logo);