namespace Jones.Bank.Dto;

public record BankCardInfoDto(string? Type, BankInfoDto Bank);