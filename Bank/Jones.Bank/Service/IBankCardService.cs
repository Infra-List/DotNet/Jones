using Jones.Bank.Dto;

namespace Jones.Bank.Service;

public interface IBankCardService
{
    BankCardInfoDto? GetBankCardInfoByCardNumber(string cardNumber);
    Task<BankCardInfoDto?> GetBankCardInfoByCardNumberAsync(string cardNumber);
    BankInfoDto? GetBankInfoByBankCode(string code);
}