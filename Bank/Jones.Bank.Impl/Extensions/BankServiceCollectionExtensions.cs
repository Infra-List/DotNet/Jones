using System.Text;
using Jones.Bank.Service;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.Bank.Impl.Extensions;

public static class BankServiceCollectionExtensions
{
    public static IServiceCollection AddBankService(this IServiceCollection services)
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        
        services.AddTransient<IBankCardService, BankCardServiceImpl>();

        return services;
    }
}