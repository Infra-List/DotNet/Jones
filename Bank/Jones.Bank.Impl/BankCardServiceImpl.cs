using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Jones.Bank.Dto;
using Jones.Bank.Service;
using Microsoft.AspNetCore.WebUtilities;

namespace Jones.Bank.Impl;

public class BankCardServiceImpl : IBankCardService
{
    private static readonly JsonSerializerOptions JsonOptions = new JsonSerializerOptions
    {
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,    // 忽略null值的属性
        PropertyNameCaseInsensitive = true,    //忽略大小写
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,    // 驼峰式
        Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping    // 序列化中文时的编码问题
    };
    
    private readonly IHttpClientFactory _httpClientFactory;

    public BankCardServiceImpl(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }

    public BankCardInfoDto? GetBankCardInfoByCardNumber(string cardNumber)
    {
        return (from bankCard in BankCardConstant.BankCards where bankCard.Patterns != null 
            from pattern in bankCard.Patterns where new Regex(pattern.Reg).IsMatch(cardNumber) 
            select new BankCardInfoDto(pattern.BankCardType, new BankInfoDto(bankCard.Code, bankCard.Name, GetLogo(bankCard.Code))))
            .FirstOrDefault();
    }

    public async Task<BankCardInfoDto?> GetBankCardInfoByCardNumberAsync(string cardNumber)
    {
        var url = QueryHelpers.AddQueryString("https://ccdcapi.alipay.com/validateAndCacheCardInfo.json", new Dictionary<string, string>
        {
            { "_input_charset", "utf-8"},
            { "cardBinCheck", "true"},
            { "cardNo", cardNumber}
        });
        var httpClient = _httpClientFactory.CreateClient();
        var bankCardByteArray = await httpClient.GetByteArrayAsync(url);
        
        var bankCardJson = Encoding.GetEncoding("GBK").GetString(bankCardByteArray);
        var bankCard = JsonSerializer.Deserialize<BankCardJson>(bankCardJson, JsonOptions);
        if (bankCard?.Validated == true)
        {
            return new BankCardInfoDto(bankCard.CardType, 
                new BankInfoDto(bankCard.Bank, BankCardConstant.BankCards.First(p => p.Code == bankCard.Bank).Name, GetLogo(bankCard.Bank)));
        }

        return null;
    }

    public BankInfoDto? GetBankInfoByBankCode(string code)
    {
        var bank = BankCardConstant.BankCards.SingleOrDefault(p => p.Code == code);
        return bank == null 
            ? null 
            : new BankInfoDto(code, bank.Name, GetLogo(code));
    }

    private static string GetLogo(string cardCode) => $"https://apimg.alipay.com/combo.png?d=cashier&t={cardCode}";

    public record BankCardJson(
        string CardType,
        string Bank,
        string Key,
        object[] Messages,
        bool Validated,
        string Stat
    );
}