using SKIT.FlurlHttpClient.Wechat.TenpayV3.Settings;

namespace Jones.WeChat.Pay.Impl.Service;

public interface IWeChatPayCertificateManagerFactory
{
    ICertificateManager Create(string merchantId);
}