using System.Text;
using Jones.WeChat.Pay.Entities;
using Jones.WeChat.Pay.Impl.Service;
using SKIT.FlurlHttpClient.Wechat.TenpayV3;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Events;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Models;

namespace Jones.WeChat.Pay.Impl;

// https://github.com/fudiwei/DotNetCore.SKIT.FlurlHttpClient.Wechat/blob/main/docs/WechatTenpayV3/README.md
public class WeChatPayServiceImpl : IWeChatPayService
{
    private readonly IWeChatPayCertificateManagerFactory _weChatPayCertificateManagerFactory;

    public WeChatPayServiceImpl(IWeChatPayCertificateManagerFactory weChatPayCertificateManagerFactory)
    {
        _weChatPayCertificateManagerFactory = weChatPayCertificateManagerFactory;
    }
    
    // https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_sl_api.php?chapter=9_1
    // https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_7&index=5
    #region 微信小程序支付

    public async Task<MiniProgramPayOrderResult> MiniProgramPayOrder(MerchantInfo merchant, string openId, string tradeNo, int totalFee, string? description, string notifyUrl)
    {
        var client = CreateWeChatPayClient(merchant);
        var request = new CreatePayTransactionJsapiRequest
        {
            OutTradeNumber = tradeNo,
            AppId = merchant.AppId,
            Description = description ?? string.Empty,
            ExpireTime = DateTimeOffset.Now.AddMinutes(15),
            NotifyUrl = notifyUrl,
            Amount = new CreatePayTransactionJsapiRequest.Types.Amount
            {
                Total = totalFee
            },
            Payer = new CreatePayTransactionJsapiRequest.Types.Payer
            {
                OpenId = openId
            }
        };
        var response = await client.ExecuteCreatePayTransactionJsapiAsync(request);
        return response.IsSuccessful() 
            ? MiniProgramPayOrderResult.Success(response.PrepayId, CreateMiniProgramRequestPaymentParameter(client, request.AppId, response.PrepayId)) 
            : MiniProgramPayOrderResult.Error(response.ErrorCode, response.ErrorMessage, response.ErrorDetail);
    }

    public async Task<MiniProgramPayOrderResult> MiniProgramPayOrder(MerchantInfo merchant, SubMerchantInfo subMerchant, string openId, string tradeNo,
        int totalFee, string? description, string notifyUrl)
    {
        var client = CreateWeChatPayClient(merchant);
        var request = new CreatePayPartnerTransactionJsapiRequest
        {
            OutTradeNumber = tradeNo,
            SubMerchantId = subMerchant.Id,
            AppId = merchant.AppId,
            SubAppId = subMerchant.AppId,
            Description = description ?? string.Empty,
            ExpireTime = DateTimeOffset.Now.AddMinutes(15),
            NotifyUrl = notifyUrl,
            Amount = new CreatePayPartnerTransactionJsapiRequest.Types.Amount
            {
                Total = totalFee,
            },
            Payer = new CreatePayPartnerTransactionJsapiRequest.Types.Payer
            {
                OpenId = subMerchant.AppId == null ? openId : null,
                SubOpenId = subMerchant.AppId == null ? null : openId
            }
        };
        var response = await client.ExecuteCreatePayPartnerTransactionJsapiAsync(request);
        return response.IsSuccessful() 
            ? MiniProgramPayOrderResult.Success(response.PrepayId, CreateMiniProgramRequestPaymentParameter(client, subMerchant.AppId ?? merchant.AppId, response.PrepayId)) 
            : MiniProgramPayOrderResult.Error(response.ErrorCode, response.ErrorMessage, response.ErrorDetail);
    }

    #endregion

    // https://pay.weixin.qq.com/wiki/doc/apiv3_partner/apis/chapter4_5_2.shtml
    #region 订单查询
    
    public async Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, string id, bool isTransactionId)
    {
        var client = CreateWeChatPayClient(merchant);
        var response = isTransactionId
            ? await client.ExecuteGetPayTransactionByIdAsync(new GetPayTransactionByIdRequest()
            {
                TransactionId = id
            })
            : await client.ExecuteGetPayTransactionByOutTradeNumberAsync(new GetPayTransactionByOutTradeNumberRequest()
            {
                OutTradeNumber = id
            });
        return new WeChatPayOrderQueryResult(response.IsSuccessful(), Encoding.UTF8.GetString(response.GetRawBytes()), 
            response.ErrorCode, response.ErrorMessage, response.TradeState, response.TradeStateDescription,
            response.Payer?.OpenId, null, response.OutTradeNumber, response.TransactionId, response.Amount?.Total, response.SuccessTime?.UtcDateTime);
    }

    public async Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, SubMerchantInfo subMerchant, string id, bool isTransactionId)
    {
        var client = CreateWeChatPayClient(merchant);
        var response = isTransactionId
            ? await client.ExecuteGetPayPartnerTransactionByIdAsync(new GetPayPartnerTransactionByIdRequest
            {
                SubMerchantId = subMerchant.Id,
                TransactionId = id
            })
            : await client.ExecuteGetPayPartnerTransactionByOutTradeNumberAsync(new GetPayPartnerTransactionByOutTradeNumberRequest()
            {
                SubMerchantId = subMerchant.Id,
                OutTradeNumber = id
            });
        return new WeChatPayOrderQueryResult(response.IsSuccessful(), Encoding.UTF8.GetString(response.GetRawBytes()), 
            response.ErrorCode, response.ErrorMessage, response.TradeState, response.TradeStateDescription,
            response.Payer?.OpenId, response.Payer?.SubOpenId, response.OutTradeNumber, response.TransactionId, response.Amount.Total, response.SuccessTime?.UtcDateTime);
    }

    #endregion

    // https://pay.weixin.qq.com/wiki/doc/apiv3_partner/apis/chapter4_5_3.shtml
    #region 关闭订单

    public async Task<WeChatPayOrderCloseResult> Close(MerchantInfo merchant, string tradeNo)
    {
        var client = CreateWeChatPayClient(merchant);
        var response = await client.ExecuteGetPayTransactionByOutTradeNumberAsync(new GetPayTransactionByOutTradeNumberRequest()
        {
            OutTradeNumber = tradeNo
        });
        return new WeChatPayOrderCloseResult(response.IsSuccessful(), Encoding.UTF8.GetString(response.GetRawBytes()), 
            response.TradeState, response.TradeStateDescription);
    }

    public async Task<WeChatPayOrderCloseResult> Close(MerchantInfo merchant, SubMerchantInfo subMerchant, string tradeNo)
    {
        var client = CreateWeChatPayClient(merchant);
        var response = await client.ExecuteGetPayPartnerTransactionByOutTradeNumberAsync(new GetPayPartnerTransactionByOutTradeNumberRequest()
        {
            SubMerchantId = subMerchant.Id,
            OutTradeNumber = tradeNo
        });
        return new WeChatPayOrderCloseResult(response.IsSuccessful(), Encoding.UTF8.GetString(response.GetRawBytes()), 
            response.TradeState, response.TradeStateDescription);
    }

    #endregion
    
    // https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_5_5.shtml
    #region 支付完成回调

    public async Task<CheckData<WeChatPayTransactionNotifyResult>> Notify(MerchantInfo merchant, string timestamp, string nonce, string signature, string serialNumber, string content)
    {
        var client = CreateWeChatPayClient(merchant);
        var isValid = client.VerifyEventSignature(timestamp, nonce, content, signature, serialNumber);

        if (!isValid)
        {
            return new CheckData<WeChatPayTransactionNotifyResult>(false, "验签失败", null);
        }
        
        var wechatPayEvent = client.DeserializeEvent(content);
        if ("TRANSACTION.SUCCESS".Equals(wechatPayEvent.EventType))
        {
            /* 根据事件类型，解密得到支付通知敏感数据 */
            var transaction = client.DecryptEventResource<PartnerTransactionResource>(wechatPayEvent);
            return new CheckData<WeChatPayTransactionNotifyResult>(true, null, 
                new WeChatPayTransactionNotifyResult(transaction.TradeState, transaction.TradeStateDescription, 
                    transaction.Payer?.OpenId, transaction.Payer?.SubOpenId, transaction.OutTradeNumber, transaction.TransactionId, transaction.Amount.Total, transaction.SuccessTime.UtcDateTime));
        }
        
        return new CheckData<WeChatPayTransactionNotifyResult>(false, $"交易事件类型异常：{wechatPayEvent.EventType}", null); 
    }

    #endregion
    
    // https://pay.weixin.qq.com/wiki/doc/api/micropay_sl.php?chapter=9_4
    #region 退款

    public bool IsCanRefund(DateTime paymentTime, int refundedCount) =>
        (DateTime.UtcNow - paymentTime).TotalDays < 365 && refundedCount < 50;

    public Task<WeChatRefundResult> Refund(MerchantInfo merchant, string transactionId, string refundNo, int totalFee, int refundFee, string? description, string notifyUrl) =>
        DoRefund(merchant, null, transactionId, refundNo, totalFee, refundFee, description, notifyUrl);

    public Task<WeChatRefundResult> Refund(MerchantInfo merchant, SubMerchantInfo subMerchant, string transactionId, string refundNo, int totalFee, int refundFee, string? description, string notifyUrl) =>
        DoRefund(merchant, subMerchant, transactionId, refundNo, totalFee, refundFee, description, notifyUrl);

    public async Task<WeChatRefundResult> DoRefund(MerchantInfo merchant, SubMerchantInfo? subMerchant, string transactionId, string refundNo, int totalFee,
        int refundFee, string? description, string notifyUrl)
    {
        var client = CreateWeChatPayClient(merchant);
        
        var request = new CreateRefundDomesticRefundRequest()
        {
            SubMerchantId = subMerchant?.Id,
            TransactionId = transactionId,
            OutRefundNumber = refundNo,
            Amount = new CreateRefundDomesticRefundRequest.Types.Amount
            {
                Total = totalFee,
                Refund = refundFee
            },
            Reason = description,
            NotifyUrl = notifyUrl
        };
        var response = await client.ExecuteCreateRefundDomesticRefundAsync(request);
        return new WeChatRefundResult(response.IsSuccessful(), Encoding.UTF8.GetString(response.GetRawBytes()), 
            response.ErrorCode, response.ErrorMessage, response.Status, response.RefundId,
            response.Amount.SettlementTotal, response.Amount.SettlementRefund, response.Channel, response.FundsAccount, response.UserReceivedAccount,
            response.Amount.PayerTotal, response.Amount.PayerRefund, response.CreateTime.UtcDateTime, response.SuccessTime?.UtcDateTime);
    }

    #endregion

    // https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_5_10.shtml
    #region 退款查询

    public Task<WeChatRefundResult> RefundQuery(MerchantInfo merchant, string refundNo) =>
        DoRefundQuery(merchant, null, refundNo);

    public Task<WeChatRefundResult> RefundQuery(MerchantInfo merchant, SubMerchantInfo subMerchant, string refundNo) =>
        DoRefundQuery(merchant, subMerchant, refundNo);

    public async Task<WeChatRefundResult> DoRefundQuery(MerchantInfo merchant, SubMerchantInfo? subMerchant, string refundNo)
    {
        var client = CreateWeChatPayClient(merchant);
        
        var request = new GetRefundDomesticRefundByOutRefundNumberRequest
        {
            SubMerchantId = subMerchant?.Id,
            OutRefundNumber = refundNo
        };
        var response = await client.ExecuteGetRefundDomesticRefundByOutRefundNumberAsync(request);
        return new WeChatRefundResult(response.IsSuccessful(), Encoding.UTF8.GetString(response.GetRawBytes()), 
            response.ErrorCode, response.ErrorMessage, response.Status, response.RefundId,
            response.Amount.SettlementTotal, response.Amount.SettlementRefund, response.Channel, response.FundsAccount, response.UserReceivedAccount,
            response.Amount.PayerTotal, response.Amount.PayerRefund, response.CreateTime.UtcDateTime, response.SuccessTime?.UtcDateTime);
    }

    #endregion

    // https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_5_11.shtml
    #region 退款完成回调
    
    public async Task<CheckData<WeChatPayRefundNotifyResult>> RefundNotify(MerchantInfo merchant, string timestamp, string nonce, string signature, string serialNumber, string content)
    {
        var client = CreateWeChatPayClient(merchant);
        var isValid = client.VerifyEventSignature(timestamp, nonce, content, signature, serialNumber);

        if (!isValid)
        {
            return new CheckData<WeChatPayRefundNotifyResult>(false, "验签失败", null);
        }
        
        var wechatPayEvent = client.DeserializeEvent(content);
        if (wechatPayEvent.EventType is "REFUND.SUCCESS" or "REFUND.ABNORMAL" or "REFUND.CLOSED")
        {
            /* 根据事件类型，解密得到支付通知敏感数据 */
            var transaction = client.DecryptEventResource<RefundResource>(wechatPayEvent);
            return new CheckData<WeChatPayRefundNotifyResult>(true, null, new WeChatPayRefundNotifyResult(transaction.RefundStatus, transaction.RefundId));
        }
        
        return new CheckData<WeChatPayRefundNotifyResult>(false, $"交易事件类型异常：{wechatPayEvent.EventType}", null); 
    }

    #endregion

    private WechatTenpayClient CreateWeChatPayClient(MerchantInfo merchant) => new (new WechatTenpayClientOptions
        {
            MerchantId = merchant.Id,
            MerchantV3Secret = merchant.Secret,
            MerchantCertificateSerialNumber = merchant.CertificateSerialNumber,
            MerchantCertificatePrivateKey = merchant.CertificatePrivateKey,
            PlatformCertificateManager = _weChatPayCertificateManagerFactory.Create(merchant.Id),
        });

    private static MiniProgramRequestPaymentParameter CreateMiniProgramRequestPaymentParameter(WechatTenpayClient client, string appId, string prepayId)
    {
        var paramMap = client.GenerateParametersForJsapiPayRequest(appId, prepayId);
        return new MiniProgramRequestPaymentParameter(paramMap["timeStamp"], paramMap["nonceStr"], paramMap["package"], paramMap["signType"], paramMap["paySign"]);
    }
}