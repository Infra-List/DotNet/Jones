using System.Text;
using Flurl.Http;
using Jones.WeChat.Pay.Entities;
using Jones.WeChat.Pay.Exceptions;
using Jones.WeChat.Pay.Impl.Sandbox;
using Microsoft.Extensions.Options;
using SKIT.FlurlHttpClient.Wechat.TenpayV2;
using SKIT.FlurlHttpClient.Wechat.TenpayV2.Models;

namespace Jones.WeChat.Pay.Impl;

// https://github.com/fudiwei/DotNetCore.SKIT.FlurlHttpClient.Wechat/blob/main/docs/WechatTenpayV2/README.md
public class WeChatPayV2ServiceImpl : IWeChatPayV2Service
{
    private readonly WeChatPayConfig _config;
    
    public WeChatPayV2ServiceImpl(IOptions<WeChatPayConfig> config)
    {
        _config = config.Value;
    }
    
    // https://pay.weixin.qq.com/wiki/doc/api/micropay_sl.php?chapter=9_10&index=1
    #region 付款码支付

    public Task<PaymentCodePayOrderV2Result> PaymentCodePayOrder(MerchantInfo merchant, string paymentCode, 
        string clientIp, string tradeNo, int totalFee, string body) =>
        DoPaymentCodePayOrder(merchant, null, paymentCode, clientIp, tradeNo, totalFee, body);

    public Task<PaymentCodePayOrderV2Result> PaymentCodePayOrder(MerchantInfo merchant, SubMerchantInfo subMerchant,
        string paymentCode, string clientIp, string tradeNo, int totalFee, string body) =>
        DoPaymentCodePayOrder(merchant, subMerchant, paymentCode, clientIp, tradeNo, totalFee, body);

    private async Task<PaymentCodePayOrderV2Result> DoPaymentCodePayOrder(MerchantInfo merchant, SubMerchantInfo? subMerchant, string paymentCode, string clientIp, string tradeNo, int totalFee, string body)
    {
        var client = CreateWeChatPayClientV2(merchant);
        var request = new CreatePayMicroPayRequest
        {
            SubMerchantId = subMerchant?.Id,
            SubAppId = subMerchant?.AppId,
            OutTradeNumber = tradeNo,
            TotalFee = totalFee,
            ClientIp = clientIp,
            AuthCode = paymentCode,
            Body = body,
        };

        if (_config.IsSandbox && totalFee >= 100)
        {
            client.Interceptors.Add(new SandboxMicropayErrorHttpCallInterceptor(totalFee switch
            {
                999 => MicropayErrorType.UserPaying,
                300 => MicropayErrorType.PayError,
                400 => MicropayErrorType.PayQueryTimeout,
                _ => MicropayErrorType.Timeout
            }));
        }

        try
        {
            var response = await client.ExecuteCreatePayMicroPayAsync(request);
            return new PaymentCodePayOrderV2Result(IsSuccess(response), Encoding.UTF8.GetString(response.GetRawBytes()), response.ReturnCode, response.ReturnMessage,
                response.ResultCode, response.ErrorCode, response.ErrorCodeDescription, response.OpenId, response.SubOpenId, response.TransactionId, response.TotalFee, response.EndTime?.UtcDateTime);
        }
        catch (FlurlHttpTimeoutException e)
        {
            throw new WeChatPayRequestTimeoutException(e.Message, e.InnerException);
        }
    }

    #endregion
    
    // https://pay.weixin.qq.com/wiki/doc/api/micropay_sl.php?chapter=9_02
    #region 订单查询
    
    public Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, string? tradeNo, string? transactionId) =>
        DoQuery(merchant, null, tradeNo, transactionId);

    public Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, SubMerchantInfo subMerchant, string? tradeNo, string? transactionId) =>
        DoQuery(merchant, subMerchant, tradeNo, transactionId);
    
    private async Task<WeChatPayOrderQueryResult> DoQuery(MerchantInfo merchant, SubMerchantInfo? subMerchant, string? tradeNo, string? transactionId)
    {
        var client = CreateWeChatPayClientV2(merchant);
        var request = new GetPayOrderRequest
        {
            SubMerchantId = subMerchant?.Id,
            SubAppId = subMerchant?.AppId,
            OutTradeNumber = transactionId == null ? tradeNo : null,
            TransactionId = transactionId
        };
        
        var response = await client.ExecuteGetPayOrderAsync(request);
        return new WeChatPayOrderQueryResult(IsSuccess(response), Encoding.UTF8.GetString(response.GetRawBytes()), 
            response.ErrorCode, response.ErrorCodeDescription, response.TradeState, response.TradeStateDescription,
            response.OpenId, response.SubOpenId, response.OutTradeNumber, response.TransactionId, response.TotalFee, response.EndTime?.UtcDateTime);
    }

    #endregion
    
    // https://pay.weixin.qq.com/wiki/doc/api/micropay_sl.php?chapter=9_11&index=3
    #region 撤销订单

    public Task<WeChatPayOrderReverseV2Result> Reverse(MerchantInfo merchant, string tradeNo) =>
        DoReverse(merchant, null, tradeNo);

    public Task<WeChatPayOrderReverseV2Result> Reverse(MerchantInfo merchant, SubMerchantInfo subMerchant, string tradeNo) =>
        DoReverse(merchant, subMerchant, tradeNo);
    
    private async Task<WeChatPayOrderReverseV2Result> DoReverse(MerchantInfo merchant, SubMerchantInfo? subMerchant, string tradeNo)
    {
        var client = CreateWeChatPayClientV2(merchant);
        var request = new ReversePayOrderRequest
        {
            SubMerchantId = subMerchant?.Id,
            SubAppId = subMerchant?.AppId,
            OutTradeNumber = tradeNo
        };
        
        var response = await client.ExecuteReversePayOrderAsync(request);
        return new WeChatPayOrderReverseV2Result(IsSuccess(response), Encoding.UTF8.GetString(response.GetRawBytes()), response.ReturnCode, response.ReturnMessage,
            response.ResultCode, response.ErrorCode, response.ErrorCodeDescription, response.RequireRecall);
    }

    #endregion
    
    // https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_sl_api.php?chapter=9_3
    #region 关闭订单

    public Task<WeChatPayOrderCloseV2Result> Close(MerchantInfo merchant, string tradeNo) =>
        DoClose(merchant, null, tradeNo);

    public Task<WeChatPayOrderCloseV2Result> Close(MerchantInfo merchant, SubMerchantInfo subMerchant, string tradeNo) =>
        DoClose(merchant, subMerchant, tradeNo);

    private async Task<WeChatPayOrderCloseV2Result> DoClose(MerchantInfo merchant, SubMerchantInfo? subMerchant, string tradeNo)
    {
        var client = CreateWeChatPayClientV2(merchant);
        var request = new ClosePayOrderRequest
        {
            SubMerchantId = subMerchant?.Id,
            SubAppId = subMerchant?.AppId,
            OutTradeNumber = tradeNo
        };
        
        var response = await client.ExecuteClosePayOrderAsync(request);
        return new WeChatPayOrderCloseV2Result(IsSuccess(response), Encoding.UTF8.GetString(response.GetRawBytes()), response.ReturnCode, response.ReturnMessage,
            response.ResultCode, response.ErrorCode, response.ErrorCodeDescription);
    }

    #endregion

    // https://pay.weixin.qq.com/wiki/doc/api/micropay_sl.php?chapter=9_4
    #region 退款

    public bool IsCanRefund(DateTime paymentTime, int refundedCount) =>
        (DateTime.UtcNow - paymentTime).TotalDays < 365 && refundedCount < 50;

    public Task<WeChatRefundV2Result> Refund(MerchantInfo merchant, string transactionId, string refundNo, int totalFee, int refundFee, string? description, string notifyUrl) =>
        DoRefund(merchant, null, transactionId, refundNo, totalFee, refundFee, description, notifyUrl);

    public Task<WeChatRefundV2Result> Refund(MerchantInfo merchant, SubMerchantInfo subMerchant, string transactionId, string refundNo, int totalFee, int refundFee, string? description, string notifyUrl) =>
        DoRefund(merchant, subMerchant, transactionId, refundNo, totalFee, refundFee, description, notifyUrl);

    public async Task<WeChatRefundV2Result> DoRefund(MerchantInfo merchant, SubMerchantInfo? subMerchant, string transactionId, string refundNo, int totalFee,
        int refundFee, string? description, string notifyUrl)
    {
        var client = CreateWeChatPayClientV2(merchant);
        
        var request = new CreatePayRefundRequest
        {
            SubMerchantId = subMerchant?.Id,
            SubAppId = subMerchant?.AppId,
            TransactionId = transactionId,
            OutRefundNumber = refundNo,
            TotalFee = totalFee,
            RefundFee = refundFee,
            Description = description,
            NotifyUrl = notifyUrl
        };
        
        var response = await client.ExecuteCreatePayRefundAsync(request);
        return new WeChatRefundV2Result(IsSuccess(response), Encoding.UTF8.GetString(response.GetRawBytes()), response.ReturnCode, response.ReturnMessage,
            response.ResultCode, response.ErrorCode, response.ErrorCodeDescription, response.RefundId, response.SettlementTotalFee, response.SettlementRefundFee);
    }

    #endregion
    

    private WechatTenpayClient CreateWeChatPayClientV2(MerchantInfo merchant)
    {
        var options = new WechatTenpayClientOptions
        {
            MerchantId = merchant.Id,
            MerchantSecret = merchant.Secret,
            MerchantCertificateBytes = Convert.FromBase64String(merchant.Certificate
                .Replace("-----BEGIN CERTIFICATE-----", "")
                .Replace("-----END CERTIFICATE-----", "")
                .Trim()),
            AppId = merchant.AppId,
        };
        if (_config.IsSandbox)
        {
            options.Endpoint = $"{options.Endpoint}/xdc/apiv2sandbox";
            options.MerchantSecret = merchant.SecretSandbox ?? string.Empty;
        }
        return new WechatTenpayClient(options);
    }
    
    private bool IsSuccess(WechatTenpayResponse response) => 
        response.IsSuccessful() || (_config.IsSandbox && "SUCCESS".Equals(response.ErrorCode));
}