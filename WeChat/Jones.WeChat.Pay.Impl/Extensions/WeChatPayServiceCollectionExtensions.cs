using Jones.WeChat.Pay.Impl.Service;
using Microsoft.Extensions.DependencyInjection;

namespace Jones.WeChat.Pay.Impl.Extensions;

public static class WeChatPayServiceCollectionExtensions
{
    public static IServiceCollection AddWeChatPayService(this IServiceCollection services)
    {
        services.AddSingleton<IWeChatPayCertificateManagerFactory, WeChatPayCertificateManagerFactoryImpl>();
        
        services.AddTransient<IWeChatPayV2Service, WeChatPayV2ServiceImpl>();
        services.AddTransient<IWeChatPayService, WeChatPayServiceImpl>();

        return services;
    }
}