using System.Collections.Concurrent;
using Jones.WeChat.Pay.Impl.Service;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Settings;

namespace Jones.WeChat.Pay.Impl;

public class WeChatPayCertificateManagerFactoryImpl : IWeChatPayCertificateManagerFactory
{
    private readonly ConcurrentDictionary<string, ICertificateManager> _dict = new();
    
    public ICertificateManager Create(string merchantId) => _dict.GetOrAdd(merchantId, new InMemoryCertificateManager());
}