using System.ComponentModel.DataAnnotations;
using Flurl.Http;
using Jones.Extensions;
using SKIT.FlurlHttpClient;

namespace Jones.WeChat.Pay.Impl.Sandbox;

public enum MicropayErrorType
{
    [Display(Name = "用户未支付完成", Description = "MICROPAY_USERPAYING", Prompt = "用户未支付完成，微信支付返回FAIL。微信支付返回超时或失败时，都先调查单api确认结果，再做下一步判断。")]
    UserPaying = 1,
    [Display(Name = "用户支付成功，微信支付返回超时", Description = "MICROPAY_TIMEOUT", Prompt = "用户支付成功，微信支付返回超时（客户端超时时间请设置在 500ms 以内）。微信支付返回超时或失败时，都先调查单api确认结果，再做下一步判断。")]
    Timeout = 2,
    [Display(Name = "用户支付失败", Description = "MICROPAY_PAYERROR", Prompt = "用户支付失败，微信支付返回超时（客户端超时时间请设置在 500ms 以内）。微信支付返回超时或失败时，都先调查单api确认结果，再做下一步判断。")]
    PayError = 3,
    [Display(Name = "微信支付返回超时", Description = "MICROPAY_PAY_QUERY_TIMEOUT", Prompt = "微信支付返回超时，且查单失败。在无法确认用户支付状态情况下，先撤销上一笔单，再换单重试。")]
    PayQueryTimeout = 4,
}

public class SandboxMicropayErrorHttpCallInterceptor : HttpInterceptor
{
    private readonly MicropayErrorType _errorType;

    public SandboxMicropayErrorHttpCallInterceptor(MicropayErrorType errorType)
    {
        _errorType = errorType;
    }

    public override Task BeforeCallAsync(HttpInterceptorContext context, CancellationToken cancellationToken = default)
    {
        context.FlurlCall.Request.WithHeader("Wechatpay-Negative-Test", _errorType.GetDisplay()?.Description);
        if (_errorType is MicropayErrorType.Timeout or MicropayErrorType.PayError)
        {
            context.FlurlCall.Request.WithTimeout(TimeSpan.FromMilliseconds(500));
        }
        return Task.CompletedTask;
    }
}