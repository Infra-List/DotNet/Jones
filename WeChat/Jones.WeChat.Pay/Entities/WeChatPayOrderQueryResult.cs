namespace Jones.WeChat.Pay.Entities;

public record WeChatPayOrderQueryResult(bool IsSuccess, string? Content, string? ErrorCode, string? ErrorMessage,
    string? State, string? StateDescription, string? OpenId, string? SubOpenId, string? TradeNo, string? TransactionId, int? TotalFee, DateTime? PaymentTime);