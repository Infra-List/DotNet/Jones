namespace Jones.WeChat.Pay.Entities;

public record WeChatRefundV2Result(bool IsSuccess, string? Content, string? ReturnCode, string? ReturnMessage, string? ResultCode, 
    string? ErrorCode, string? ErrorCodeDescription, string RefundId, int? SettlementTotalFee, int? SettlementRefundFee);