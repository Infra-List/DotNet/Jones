namespace Jones.WeChat.Pay.Entities;

public record ServiceProviderMerchantInfo(MerchantInfo Merchant, SubMerchantInfo SubMerchant);