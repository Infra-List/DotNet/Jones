namespace Jones.WeChat.Pay.Entities;

public record MiniProgramRequestPaymentParameter(string TimeStamp, string NonceStr, string Package, string SignType, string PaySign);