namespace Jones.WeChat.Pay.Entities;

public record PaymentCodePayOrderV2Result(bool IsSuccess, string? Content, string? ReturnCode, string? ReturnMessage,
    string? ResultCode, string? ErrorCode, string? ErrorCodeDescription, string? OpenId, string? SubOpenId, string? TransactionId, int? TotalFee, DateTime? PaymentTime);