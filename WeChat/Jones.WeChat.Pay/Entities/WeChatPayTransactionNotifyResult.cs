namespace Jones.WeChat.Pay.Entities;

public record WeChatPayTransactionNotifyResult(string State, string StateDescription, 
    string? OpenId, string? SubOpenId, string TradeNo, string TransactionId, int TotalFee, DateTime PaymentTime);