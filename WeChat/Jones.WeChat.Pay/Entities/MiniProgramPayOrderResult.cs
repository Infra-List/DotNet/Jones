namespace Jones.WeChat.Pay.Entities;

public record MiniProgramPayOrderResult(bool IsSuccess, string? PrepayId, MiniProgramRequestPaymentParameter? RequestPaymentParameter, 
    string? ErrorCode, string? ErrorMessage, Dictionary<string, object>? ErrorDetail)
{
    public static MiniProgramPayOrderResult Success(string prepayId, MiniProgramRequestPaymentParameter requestPaymentParameter) => 
        new (true, prepayId, requestPaymentParameter, null, null, null);
    public static MiniProgramPayOrderResult Error(string? errorCode, string? errorMessage, Dictionary<string, object>? errorDetail) => 
        new (false, null, null, errorCode, errorMessage, errorDetail);
}