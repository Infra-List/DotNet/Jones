namespace Jones.WeChat.Pay.Entities;

public record WeChatPayOrderReverseV2Result(bool IsSuccess, string? Content, string? ReturnCode, string? ReturnMessage, string? ResultCode, 
    string? ErrorCode, string? ErrorCodeDescription, bool IsRequireRecall);