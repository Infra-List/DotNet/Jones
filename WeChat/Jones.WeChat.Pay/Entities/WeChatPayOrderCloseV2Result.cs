namespace Jones.WeChat.Pay.Entities;

public record WeChatPayOrderCloseV2Result(bool IsSuccess, string? Content, string? ReturnCode, string? ReturnMessage, string? ResultCode, 
    string? ErrorCode, string? ErrorCodeDescription);