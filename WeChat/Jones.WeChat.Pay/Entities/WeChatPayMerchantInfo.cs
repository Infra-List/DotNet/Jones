using OneOf;

namespace Jones.WeChat.Pay.Entities;

public record WeChatPayMerchantInfo(MerchantInfo? Merchant, ServiceProviderMerchantInfo? ServiceProviderMerchant)
{
    public OneOf<MerchantInfo, ServiceProviderMerchantInfo>? ToOneOf()
    {
        if (ServiceProviderMerchant != null)
        {
            return ServiceProviderMerchant;
        }

        if (Merchant != null)
        {
            return Merchant;
        }

        return null;
    }

    public MerchantInfo? MerchantInfo => ServiceProviderMerchant != null ? ServiceProviderMerchant.Merchant : Merchant;
}