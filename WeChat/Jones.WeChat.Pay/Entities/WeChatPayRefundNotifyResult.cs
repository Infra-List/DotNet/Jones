namespace Jones.WeChat.Pay.Entities;

public record WeChatPayRefundNotifyResult(string State, string RefundId);