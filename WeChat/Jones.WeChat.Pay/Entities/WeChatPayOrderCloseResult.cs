namespace Jones.WeChat.Pay.Entities;

public record WeChatPayOrderCloseResult(bool IsSuccess, string? Content, string State, string StateDescription);