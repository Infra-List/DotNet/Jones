namespace Jones.WeChat.Pay.Entities;

public record WeChatRefundResult(bool IsSuccess, string? Content, string? ErrorCode, string? ErrorMessage, string State, string? RefundId, int? SettlementTotal, int? SettlementRefund,
    string? Channel, string? RefundAccount, string? UserReceivedAccount, int? UserPaymentTotal, int? UserReceivedMoney, 
    DateTime? AcceptanceTime, DateTime? SuccessTime);