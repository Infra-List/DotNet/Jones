namespace Jones.WeChat.Pay.Entities;

public record MerchantInfo(string Id, string Secret, string? SecretSandbox, 
    string Certificate, string CertificateSerialNumber, string CertificatePrivateKey, string AppId);