using System.Diagnostics.CodeAnalysis;

namespace Jones.WeChat.Pay.Entities;

public record CheckData<T>(bool IsSuccess, string? Message, T? Data)
{
    public bool CheckIsSuccess([NotNullWhen(true)] out T? data)
    {
        data = Data;
        return IsSuccess;
    }
}