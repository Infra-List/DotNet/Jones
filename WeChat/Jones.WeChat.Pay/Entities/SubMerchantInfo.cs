namespace Jones.WeChat.Pay.Entities;

public record SubMerchantInfo(string Id, string? AppId);