using System.ComponentModel.DataAnnotations;

namespace Jones.WeChat.Pay.Types;

public static class WeChatErrorCode
{
    [Display(Description = "用户支付中，需要输入密码", GroupName ="202", Prompt = "等待5秒，然后调用被扫订单结果查询API，查询当前订单的不同状态，决定下一步的操作。")]
    public const string UserPaying = "USERPAYING";
}