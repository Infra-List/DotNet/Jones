using System.ComponentModel.DataAnnotations;

namespace Jones.WeChat.Pay.Types;

public static class WeChatReturnCode
{
    [Display(Name = "成功")]
    public const string Success = "SUCCESS";
    
    [Display(Name = "失败")]
    public const string Fail = "FAIL";
}