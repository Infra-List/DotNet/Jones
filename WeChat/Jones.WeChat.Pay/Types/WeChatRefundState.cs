using System.ComponentModel.DataAnnotations;

namespace Jones.WeChat.Pay.Types;

public static class WeChatRefundState
{
    [Display(Description = "退款成功")]
    public const string Success = "SUCCESS";
    
    [Display(Description = "退款关闭")]
    public const string Closed = "CLOSED";
    
    [Display(Description = "退款处理中")]
    public const string Processing = "PROCESSING";
    
    [Display(Description = "退款异常")]
    public const string Abnormal = "ABNORMAL";
    
    

    public static bool IsProcessing(string? state) => state is null or Processing;
}