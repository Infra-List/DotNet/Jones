using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jones.WeChat.Pay.Types;

[Description("交易状态")]
public static class WeChatTradeState
{
    [Display(Description = "支付成功")]
    public const string Success = "SUCCESS";

    [Display(Description = "转入退款")]
    public const string Refund = "REFUND";
    
    [Display(Description = "未支付")]
    public const string NotPay = "NOTPAY";
    
    [Display(Description = "已关闭")]
    public const string Closed = "CLOSED";
    
    [Display(Description = "已撤销", Prompt = "仅付款码支付会返回")]
    public const string Revoked = "REVOKED";
    
    [Display(Description = "用户支付中", Prompt = "仅付款码支付会返回")]
    public const string UserPaying = "USERPAYING";
    
    [Display(Description = "支付失败", Prompt = "仅付款码支付会返回")]
    public const string PayError = "PAYERROR";

    public static bool IsCompleted(string? state) => 
        state is Success or Refund or NotPay or Closed or Revoked or PayError;

    public static bool IsSuccess(string? state) => IsCompleted(state) && state == Success;

    public static bool IsUserPaying(string? state) => state is null or UserPaying or WeChatPayServerTimeout;
    
    [Display(Description = "微信支付返回超时", Prompt = "微信支付服务器超时无响应")]
    public const string WeChatPayServerTimeout = "WeChatPayServerTimeout";
}