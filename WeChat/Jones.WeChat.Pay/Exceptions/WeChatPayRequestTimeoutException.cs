using System.Runtime.Serialization;

namespace Jones.WeChat.Pay.Exceptions;

public class WeChatPayRequestTimeoutException : Exception
{
    public WeChatPayRequestTimeoutException()
    {
    }

    protected WeChatPayRequestTimeoutException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public WeChatPayRequestTimeoutException(string? message) : base(message)
    {
    }

    public WeChatPayRequestTimeoutException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}