namespace Jones.WeChat.Pay;

public record WeChatPayConfig
{
    public bool IsSandbox { get; set; }
}