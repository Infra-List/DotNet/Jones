using Jones.WeChat.Pay.Entities;

namespace Jones.WeChat.Pay;

public interface IWeChatPayV2Service
{
    // 付款码支付
    Task<PaymentCodePayOrderV2Result> PaymentCodePayOrder(MerchantInfo merchant, 
        string paymentCode, string clientIp, string tradeNo, int totalFee, string body);
    Task<PaymentCodePayOrderV2Result> PaymentCodePayOrder(MerchantInfo merchant, SubMerchantInfo subMerchant, 
        string paymentCode, string clientIp, string tradeNo, int totalFee, string body);
    
    // 订单查询
    Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, string? tradeNo, string? transactionId);
    Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, SubMerchantInfo subMerchant, string? tradeNo, string? transactionId);
    
    // 撤销订单
    Task<WeChatPayOrderReverseV2Result> Reverse(MerchantInfo merchant, string tradeNo);
    Task<WeChatPayOrderReverseV2Result> Reverse(MerchantInfo merchant, SubMerchantInfo subMerchant, string tradeNo);
    
    // 关闭订单
    Task<WeChatPayOrderCloseV2Result> Close(MerchantInfo merchant, string tradeNo);
    Task<WeChatPayOrderCloseV2Result> Close(MerchantInfo merchant, SubMerchantInfo subMerchant, string tradeNo);
    
    // 退款
    bool IsCanRefund(DateTime paymentTime, int refundedCount);
    Task<WeChatRefundV2Result> Refund(MerchantInfo merchant, string transactionId, string refundNo, 
        int totalFee, int refundFee, string? description, string notifyUrl);
    Task<WeChatRefundV2Result> Refund(MerchantInfo merchant, SubMerchantInfo subMerchant, string transactionId, string refundNo, 
        int totalFee, int refundFee, string? description, string notifyUrl);
}