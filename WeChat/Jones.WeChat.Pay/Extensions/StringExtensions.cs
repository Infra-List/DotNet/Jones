using System.Text.RegularExpressions;

namespace Jones.WeChat.Pay.Extensions;

public static class StringExtensions
{
    // http://www.qilin668.com/a/5e4be4e8a1b78z4.html
    // https://pay.weixin.qq.com/wiki/doc/api/micropay.php?chapter=5_1
    public static bool IsWeChatPaymentCode(this string source) => new Regex(@"^1[0-5]\d{16}$").IsMatch(source);
}