using Jones.WeChat.Pay.Entities;
using Jones.WeChat.Pay.Types;

namespace Jones.WeChat.Pay.Extensions;

public static class PaymentCodePayOrderV2ResultExtensions
{
    public static string? ErrorMessage(this PaymentCodePayOrderV2Result source) => 
        source.ReturnCode != WeChatReturnCode.Success 
            ? source.ReturnMessage 
            : source.ErrorCodeDescription;
}