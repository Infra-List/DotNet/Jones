using Jones.WeChat.Pay.Entities;

namespace Jones.WeChat.Pay;

public interface IWeChatPayService
{
    // 微信小程序支付
    Task<MiniProgramPayOrderResult> MiniProgramPayOrder(MerchantInfo merchant, string openId, string tradeNo, int totalFee, string? description, string notifyUrl);
    Task<MiniProgramPayOrderResult> MiniProgramPayOrder(MerchantInfo merchant, SubMerchantInfo subMerchant, string openId, string tradeNo, int totalFee, string? description, string notifyUrl);

    // 订单查询
    Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, string id, bool isTransactionId);
    Task<WeChatPayOrderQueryResult> Query(MerchantInfo merchant, SubMerchantInfo subMerchant, string id, bool isTransactionId);

    // 关闭订单
    Task<WeChatPayOrderCloseResult> Close(MerchantInfo merchant, string tradeNo);
    Task<WeChatPayOrderCloseResult> Close(MerchantInfo merchant, SubMerchantInfo subMerchant, string tradeNo);

    // 支付通知
    Task<CheckData<WeChatPayTransactionNotifyResult>> Notify(MerchantInfo merchant, string timestamp, string nonce, string signature, string serialNumber, string content);
    
    
    // 退款
    bool IsCanRefund(DateTime paymentTime, int refundedCount);
    
    Task<WeChatRefundResult> Refund(MerchantInfo merchant, string transactionId, string refundNo, 
        int totalFee, int refundFee, string? description, string notifyUrl);
    Task<WeChatRefundResult> Refund(MerchantInfo merchant, SubMerchantInfo subMerchant, string transactionId, string refundNo, 
        int totalFee, int refundFee, string? description, string notifyUrl);
    
    // 退款查询
    Task<WeChatRefundResult> RefundQuery(MerchantInfo merchant, string refundNo);
    Task<WeChatRefundResult> RefundQuery(MerchantInfo merchant, SubMerchantInfo subMerchant, string refundNo);

    // 退款完成回调
    Task<CheckData<WeChatPayRefundNotifyResult>> RefundNotify(MerchantInfo merchant, string timestamp, string nonce, string signature, string serialNumber, string content);
}