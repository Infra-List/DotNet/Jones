using Microsoft.Extensions.DependencyInjection;

namespace Jones.WeChat.MiniProgram.Impl.Extensions;

public static class WeChatMiniProgramServiceCollectionExtensions
{
    public static IServiceCollection AddWeChatMiniProgramService(this IServiceCollection services)
    {
        services.AddSingleton<IWeChatMiniProgramService, WeChatMiniProgramServiceImpl>();

        return services;
    }
}