using System.Text.Json;
using Jones.WeChat.MiniProgram.Entities;
using Jones.WeChat.MiniProgram.Impl.Helper;
using SKIT.FlurlHttpClient.Wechat.Api;
using SKIT.FlurlHttpClient.Wechat.Api.Models;

namespace Jones.WeChat.MiniProgram.Impl;

public class WeChatMiniProgramServiceImpl : IWeChatMiniProgramService
{
    public async Task<Code2SessionResult> GetSessionAndId(string appId, string appSecret, string code)
    {
        var client = new WechatApiClient(new WechatApiClientOptions
        {
            AppId = appId,
            AppSecret = appSecret
        });
        var response = await client.ExecuteSnsJsCode2SessionAsync(new SnsJsCode2SessionRequest
        {
            JsCode = code
        });
        return new Code2SessionResult
        {
            OpenId = response.OpenId,
            SessionKey = response.SessionKey,
            UnionId = response.UnionId
        };
    }

    public Task<WeChatPhoneNumber?> GetPhoneNumber(string sessionKey, string encryptedData, string iv)
    {
        var decryptPhoneNumberJson = EncryptHelper.DecodeEncryptedData(sessionKey, encryptedData, iv);
        var decryptPhoneNumber = JsonSerializer.Deserialize<WxaBusinessGetUserPhoneNumberResponse.Types.PhoneInfo>(decryptPhoneNumberJson);

        return Task.FromResult(
            decryptPhoneNumber == null 
                ? null 
                : new WeChatPhoneNumber 
                {
                    PhoneNumber = decryptPhoneNumber.PhoneNumber,
                    PurePhoneNumber = decryptPhoneNumber.PurePhoneNumber,
                    CountryCode = decryptPhoneNumber.CountryCode,
                    Watermark = new WeChatWatermark
                    {
                        AppId = decryptPhoneNumber.Watermark.AppId,
                        Timestamp = decryptPhoneNumber.Watermark.Timestamp
                    }
                });
    }
    
    // https://developers.weixin.qq.com/miniprogram/dev/framework/open-ability/signature.html#%E5%8A%A0%E5%AF%86%E6%95%B0%E6%8D%AE%E8%A7%A3%E5%AF%86%E7%AE%97%E6%B3%95
    public Task<WeChatUserInfo?> GetUserInfo(string sessionKey, string encryptedData, string iv)
    {
        var decodedUserInfoJson = EncryptHelper.DecodeEncryptedData(sessionKey, encryptedData, iv);
        var decodedUserInfo = JsonSerializer.Deserialize<DecodedUserInfo>(decodedUserInfoJson);
        
        return Task.FromResult(
            decodedUserInfo == null 
                ? null 
                : new WeChatUserInfo
                {
                    UnionId = decodedUserInfo.UnionId,
                    OpenId = decodedUserInfo.OpenId,
                    NickName = decodedUserInfo.NickName,
                    Gender = decodedUserInfo.Gender,
                    City = decodedUserInfo.City,
                    Province = decodedUserInfo.Province,
                    Country = decodedUserInfo.Country,
                    AvatarUrl = decodedUserInfo.AvatarUrl,
                    Watermark = new WeChatWatermark
                    {
                        AppId = decodedUserInfo.Watermark.AppId,
                        Timestamp = decodedUserInfo.Watermark.Timestamp
                    },
                });
    }
}