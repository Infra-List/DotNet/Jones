using System.Security.Cryptography;
using System.Text;

namespace Jones.WeChat.MiniProgram.Impl.Helper;

public static class EncryptHelper
{
    /// <summary>
    /// 解密所有消息的基础方法
    /// </summary>
    /// <param name="sessionKey">储存在 SessionBag 中的当前用户 会话 SessionKey</param>
    /// <param name="encryptedData">接口返回数据中的 encryptedData 参数</param>
    /// <param name="iv">接口返回数据中的 iv 参数，对称解密算法初始向量</param>
    /// <returns></returns>
    public static string DecodeEncryptedData(string sessionKey, string encryptedData, string iv)
    {
        //var aesCipher = Convert.FromBase64String(encryptedData);
        var aesKey = Convert.FromBase64String(sessionKey);
        var aesIv = Convert.FromBase64String(iv);

        var result = AES_Decrypt(encryptedData, aesIv, aesKey);
        var resultStr = Encoding.UTF8.GetString(result);
        return resultStr;
    }
    
    private static byte[] AES_Decrypt(string input, byte[] iv, byte[] key)
    {
        var aes = Aes.Create();
        aes.KeySize = 128;//原始：256
        aes.BlockSize = 128;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;
        aes.Key = key;
        aes.IV = iv;
        var decrypt = aes.CreateDecryptor(aes.Key, aes.IV);
        byte[] xBuff;
        try
        {
            using var ms = new MemoryStream();
            using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
            {
                //cs.Read(decryptBytes, 0, decryptBytes.Length);
                //cs.Close();
                //ms.Close();

                //cs.FlushFinalBlock();//用于解决第二次获取小程序Session解密出错的情况


                var xXml = Convert.FromBase64String(input);
                var msg = new byte[xXml.Length + 32 - xXml.Length % 32];
                Array.Copy(xXml, msg, xXml.Length);
                cs.Write(xXml, 0, xXml.Length);
            }
            //cs.Dispose();
            xBuff = Decode2(ms.ToArray());
        }
        catch (CryptographicException)
        {
            using var ms = new MemoryStream();
            //cs 不自动释放，用于避免“Padding is invalid and cannot be removed”的错误    —— 2019.07.27 Jeffrey
            var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write);
            {
                var xXml = Convert.FromBase64String(input);
                var msg = new byte[xXml.Length + 32 - xXml.Length % 32];
                Array.Copy(xXml, msg, xXml.Length);
                cs.Write(xXml, 0, xXml.Length);
            }
            xBuff = Decode2(ms.ToArray());
        }
        return xBuff;
    }
    
    private static byte[] Decode2(byte[] decrypted)
    {
        int pad = decrypted[^1];
        if (pad is < 1 or > 32)
        {
            pad = 0;
        }
        var res = new byte[decrypted.Length - pad];
        Array.Copy(decrypted, 0, res, 0, decrypted.Length - pad);
        return res;
    }
}