namespace Jones.WeChat.MiniProgram.Entities;

public record WeChatWatermark
{
    public string AppId { get; set; }
    public long Timestamp { get; set; }
}