namespace Jones.WeChat.MiniProgram.Entities;

public record WeChatDevelopmentInfo(int WeChatId,string AppId, string AppSecret);