using System.Text.Json.Serialization;

namespace Jones.WeChat.MiniProgram.Entities;

public record DecodedUserInfo : DecodedEntityBase
{
    [JsonPropertyName("unionId")]
    public string UnionId { get; set; }
    
    [JsonPropertyName("openId")]
    public string OpenId { get; set; }
    
    [JsonPropertyName("nickName")]
    public string NickName { get; set; }
    
    [JsonPropertyName("gender")]
    public int Gender { get; set; }
    
    [JsonPropertyName("city")]
    public string City { get; set; }
    
    [JsonPropertyName("province")]
    public string Province { get; set; }
    
    [JsonPropertyName("country")]
    public string Country { get; set; }
    
    [JsonPropertyName("avatarUrl")]
    public string AvatarUrl { get; set; }
}