namespace Jones.WeChat.MiniProgram.Entities
{
    public record WeChatUserInfo
    {
        public string UnionId { get; set; }
        public string OpenId { get; set; }
        public string NickName { get; set; }
        public int Gender { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string AvatarUrl { get; set; }
    
        public WeChatWatermark Watermark { get; set; }
    }
}