namespace Jones.WeChat.MiniProgram.Entities;

public record WeChatPhoneNumber
{
    /// <summary>
    /// 用户绑定的手机号（国外手机号会有区号）
    /// </summary>
    public string PhoneNumber { get; set; }
    /// <summary>
    /// 没有区号的手机号
    /// </summary>
    public string PurePhoneNumber { get; set; }
    /// <summary>
    /// 区号（Senparc注：国别号）
    /// </summary>
    public string CountryCode { get; set; }
    
    public WeChatWatermark Watermark { get; set; }
}