using System.Text.Json.Serialization;

namespace Jones.WeChat.MiniProgram.Entities;

public record DecodedEntityBase
{
    [JsonPropertyName("watermark")]
    public Types.Watermark Watermark { get; set; }

    public static class Types
    {
        public record Watermark
        {
            /// <summary>获取或设置小程序 AppId</summary>
            [JsonPropertyName("appid")]
            public string AppId { get; set; }

            /// <summary>获取或设置用户获取手机号操作的时间戳。</summary>
            [JsonPropertyName("timestamp")]
            public long Timestamp { get; set; }
        }
    }
}
    
