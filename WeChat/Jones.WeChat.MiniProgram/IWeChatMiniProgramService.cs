using Jones.WeChat.MiniProgram.Entities;

namespace Jones.WeChat.MiniProgram
{
    public interface IWeChatMiniProgramService
    {
        Task<Code2SessionResult> GetSessionAndId(string appId, string appSecret, string code);

        Task<WeChatPhoneNumber?> GetPhoneNumber(string sessionKey, string encryptedData, string iv);
        
        Task<WeChatUserInfo?> GetUserInfo(string sessionKey, string encryptedData, string iv);
    }
}